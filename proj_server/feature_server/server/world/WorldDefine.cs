﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.World
{
    /// <summary>
    /// Id [5000,6000)
    /// </summary>
    public enum EWorldProto
    {
        G2W_Req_GetRoomList = 5000,
        W2G_Res_GetRoomList = 5001,
        G2W_Req_CreateUnit = 5002,
        W2G_Res_CreateUnit = 5003,
    }

    public enum EWorldErrorCode
    {

    }

    public static class WorldDefine
    {
    }
}
