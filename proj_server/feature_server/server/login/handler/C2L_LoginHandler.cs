﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.Login
{
    [MessageHandler(EPeerType.Login)]
    public class C2L_LoginHandler : AbstractRpcHandler<C2L_Req_Login, L2C_Res_Login>
    {
        protected override async ETTask Run(Session session, C2L_Req_Login request, L2C_Res_Login response, Action reply)
        {
            var server = Context.Game.GetComponent<LoginServer>();
            var inner_comp = server.GetComponent<NetworkInnerComponent>();
            var route_comp = Context.Game.GetComponent<RouteTableComponent>();

            var db_addr = await route_comp.GetAddrAsync(EPeerType.Database);
            var db_session = inner_comp.Get(db_addr);
            var query_reponse = (D2L_Res_Login)await db_session.Call(new L2D_Req_Login() { name = request.name, pwd = request.pwd });

            if (query_reponse.AccountId == 0)
            {
                response.Error = 3;
                response.Message = "账号或者密码不存在";
                goto ReplyLabel;
            }
            Log.Debug($"AccountId = {query_reponse.AccountId}");
            var gate_addr = await route_comp.GetAddrAsync(EPeerType.Gate);
            var gate_session = inner_comp.Get(gate_addr);

            // 向gate请求一个key,客户端可以拿着这个key连接gate
            G2L_Res_Login g2l_res = (G2L_Res_Login)await gate_session.Call(new L2G_Req_Login() { AccountId = query_reponse.AccountId });

            response.key = g2l_res.key;
            response.addr = g2l_res.gate_addr;

        ReplyLabel:
            reply();
        }
    }
}
