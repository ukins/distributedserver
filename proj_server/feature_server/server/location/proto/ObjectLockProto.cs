﻿using ETT.Model;
using ETT.Model.proto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Server.Location
{
    [Message((int)ELocationProto.Req_ObjectLock)]
    public class ObjectLockRequest : AbstractRequest
    {
        public long Key { get; set; }
        public long InstanceId { get; set; }
        public int Time { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(Key);
            writer.Write(InstanceId);
            writer.Write(Time);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            Key = reader.ReadInt64();
            InstanceId = reader.ReadInt64();
            Time = reader.ReadInt32();
        }
    }

    [Message((int)ELocationProto.Res_ObjectLock)]
    public class ObjectLockResponse : AbstractResponse
    {

    }
}
