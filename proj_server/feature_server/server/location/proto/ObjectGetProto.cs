﻿using ETT.Model;
using ETT.Model.proto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Server.Location
{
    [Message((int)ELocationProto.Req_ObjectGet)]
    public class ObjectGetRequest:AbstractRequest
    {
        public long Key { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(Key);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            Key = reader.ReadInt64();
        }
    }


    [Message((int)ELocationProto.Res_ObjectGet)]
    public class ObjectGetResponse : AbstractResponse
    {
        public long InstanceId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(InstanceId);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            InstanceId = reader.ReadInt64();
        }
    }
}
