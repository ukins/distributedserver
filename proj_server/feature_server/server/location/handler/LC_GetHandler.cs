﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.Table;
using System;

namespace ETT.Server.Location
{
    [MessageHandler(EPeerType.Location)]
    public class LC_GetHandler : AbstractRpcHandler<ObjectGetRequest, ObjectGetResponse>
    {
        protected override async ETTask Run(Session session, ObjectGetRequest request, ObjectGetResponse response, Action reply)
        {
            long instanceid = await session.Root.GetComponent<LocationComponent>().GetAsync(request.Key);

            if (instanceid == 0)
            {
                response.Error = (int)ECommonErrCodeTable.ActorLocationNotFound;
            }
            response.InstanceId = instanceid;

            reply();
        }
    }
}
