﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    [MessageHandler(EPeerType.Location)]
    public class LC_AddHandler : AbstractRpcHandler<ObjectAddRequest, ObjectAddResponse>
    {
        protected override async ETTask Run(Session session, ObjectAddRequest request, ObjectAddResponse response, Action reply)
        {
            session.Root.GetComponent<LocationComponent>().Add(request.Key, request.InstanceId);

            reply();

            await ETTask.CompletedTask;
        }
    }
}
