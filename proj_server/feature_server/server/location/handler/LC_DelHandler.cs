﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    [MessageHandler(EPeerType.Location)]
    public class LC_DelHandler : AbstractRpcHandler<ObjectDelRequest, ObjectDelResponse>
    {
        protected override async ETTask Run(Session session, ObjectDelRequest request, ObjectDelResponse response, Action reply)
        {
            session.Root.GetComponent<LocationComponent>().Remove(request.Key);

            reply();

            await ETTask.CompletedTask;
        }
    }
}
