﻿using ETT.Model;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ETT.Server.Location
{
    public class ActorMessageSenderComponent : Component
    {
        public ActorMessageSender Get(long actorid)
        {
            if (actorid == 0)
            {
                throw new Exception("actor instanceid is 0");
            }
            var route_table = Context.Game.GetComponent<RouteTableComponent>();
            IPEndPoint ipendpoint = route_table.GetAddr(SerialUtils.GetPeerId(actorid));
            ActorMessageSender sender = new ActorMessageSender(actorid, ipendpoint, Root);
            return sender;
        }
    }
}
