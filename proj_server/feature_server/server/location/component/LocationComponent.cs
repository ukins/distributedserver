﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    public class LocationComponent : Component
    {
        /// <summary>
        /// Id:InstanceId
        /// </summary>
        private readonly Dictionary<long, long> m_location_dict = new Dictionary<long, long>();

        private readonly Dictionary<long, long> m_lock_dict = new Dictionary<long, long>();

        private readonly Dictionary<long, Queue<LocationTask>> m_task_dict = new Dictionary<long, Queue<LocationTask>>();

        public void Add(long key, long instanceid)
        {
            m_location_dict[key] = instanceid;
            Log.Info($"location add key:{key} instanceid:{instanceid}");

            // 更新db
            //await Game.Scene.GetComponent<DBProxyComponent>().Save(new Location(key, address));
        }

        public void Remove(long key)
        {
            Log.Info($"locatoin remove key:{key}");
            m_location_dict.Remove(key);
        }

        public long Get(long key)
        {
            Log.Info($"locatoin get key:{key}");
            m_location_dict.TryGetValue(key, out long instanceid);
            return instanceid;
        }

        public async ETVoid Lock(long key, long instanceId, int time = 0)
        {
            if (m_lock_dict.ContainsKey(key) == true)
            {
                Log.Error($"不可能同时存在两次lock,key:{key} InstanceId:{instanceId}");
                return;
            }

            Log.Info($"location lock key:{key} InstanceId:{instanceId}");
            if (m_location_dict.TryGetValue(key, out long register_id) == false)
            {
                Log.Error($"actor 没有注册， key:{key} instanceid:{instanceId}");
                return;
            }

            if (register_id != instanceId)
            {
                Log.Error($"actor注册的instanceId与Lock的不一致,key:{key} instanceid:{instanceId} register_id:{register_id}");
                return;
            }

            m_lock_dict.Add(key, instanceId);

            if (time > 0)
            {
                await Context.Game.GetComponent<TimeComponent>().WaitAsync(time);

                if (m_lock_dict.ContainsKey(key) == false)
                {
                    return;
                }
                Log.Info($"location timeout unluck key:{key} time:{time}");
                UnLock(key);
            }
        }

        public void UnLockAndUpdate(long key, long oldinstanceid, long instanceid)
        {
            m_lock_dict.TryGetValue(key, out long lockinstanceid);
            if (lockinstanceid != oldinstanceid)
            {
                Log.Error($"unlock appid is different{lockinstanceid} {oldinstanceid}");
            }
            Log.Info($"location unlock key:{key} oldinstanceid:{oldinstanceid} new:{instanceid}");
            m_location_dict[key] = instanceid;
            UnLock(key);
        }

        private void UnLock(long key)
        {
            m_lock_dict.Remove(key);
            if (m_task_dict.TryGetValue(key, out var task_queue) == false)
            {
                return;
            }

            while (true)
            {
                if (task_queue.Count <= 0)
                {
                    m_task_dict.Remove(key);
                    return;
                }
                if (m_lock_dict.ContainsKey(key) == true)
                {
                    return;
                }

                LocationTask task = task_queue.Dequeue();
                try
                {
                    task.Run();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
                task.Dispose();
            }
        }

        public ETTask<long> GetAsync(long key)
        {
            if (m_lock_dict.ContainsKey(key) == false)
            {
                m_location_dict.TryGetValue(key, out long instanceid);
                Log.Info($"location get key:{key} instanceid:{instanceid}");
                return ETTask.FromResult(instanceid);
            }
            LocationQueryTask task = Root.Factory.CreateWithParent<LocationQueryTask, long>(this, key);
            AddTask(key, task);
            return task.Task;
        }

        private void AddTask(long key, LocationTask task)
        {
            if (m_task_dict.TryGetValue(key, out var task_queue) == false)
            {
                task_queue = new Queue<LocationTask>();
                m_task_dict[key] = task_queue;
            }
            task_queue.Enqueue(task);
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            m_location_dict.Clear();
            m_lock_dict.Clear();
            m_task_dict.Clear();
        }
    }
}
