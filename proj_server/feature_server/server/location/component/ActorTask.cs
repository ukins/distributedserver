﻿using ETT.Model;

namespace ETT.Server.Location
{
    public struct ActorTask
    {
        public IActorRequest ActorProto;

        public ETTaskCompletionSource<IActorLocationResponse> Tcs;

        public ActorTask(IActorLocationMessage actorMessage)
        {
            this.ActorProto = actorMessage;
            this.Tcs = null;
        }

        public ActorTask(IActorLocationRequest request, ETTaskCompletionSource<IActorLocationResponse> tcs)
        {
            ActorProto = request;
            Tcs = tcs;
        }
    }
}
