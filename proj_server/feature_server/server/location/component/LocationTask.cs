﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    public abstract class LocationTask : Component
    {
        public abstract void Run();
    }

    public sealed class LocationQueryTask : LocationTask
    {
        public long Key;

        public ETTaskCompletionSource<long> Tcs;

        public ETTask<long> Task => Tcs.Task;

        public override void Run()
        {
            try
            {
                LocationComponent location_comp = GetParent<LocationComponent>();
                long location = location_comp.Get(Key);
                Tcs.SetResult(location);
            }
            catch (Exception e)
            {
                Tcs.SetException(e);
            }
        }
    }
}
