﻿using ETT.Model;
using ETT.Model.network;
using ETT.Server.Common;
using ETT.Server.Location;
using ETT.Server.Table;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.Location
{
    public class ActorLocationSender : ComponentWithId, IAwake, IDestroy
    {
        public long ActorId;

        public long LastRecvTime;

        public int FailTimes;

        public const int MaxFailTimes = 5;

        public void Awake()
        {
            LastRecvTime = TimeUtils.Now();
            FailTimes = 0;
            ActorId = 0;

            StartAsync().Coroutine();
        }

        private async ETVoid StartAsync()
        {
            using (await Context.Game.GetComponent<CoroutineLockComponent>().Wait(Id))
            {
                ActorId = await Root.GetComponent<LocationProxyComponent>().Get(Id);
            }
        }

        public void Destroy()
        {
            Id = 0;
            LastRecvTime = 0;
            ActorId = 0;
            FailTimes = 0;
        }

        private async ETTask<IActorResponse> Run(IActorRequest iActorRequest)
        {
            long instanceId = InstanceId;

            using (await Context.Game.GetComponent<CoroutineLockComponent>().Wait(Id))
            {
                if (InstanceId != instanceId)
                {
                    throw new RpcException((int)ECommonErrCodeTable.ActorRemove, "");
                }

                ActorMessageSender sender = Root.GetComponent<ActorMessageSenderComponent>().Get(ActorId);
                try
                {
                    // ERR_NotFoundActor是需要抛异常的，但是这里不能抛
                    IActorResponse response = await sender.CallWithoutException(iActorRequest);

                    switch (response.Error)
                    {
                        case (int)ECommonErrCodeTable.NotFoundActor:
                            // 如果没找到Actor,重试
                            ++FailTimes;

                            // 失败MaxFailTimes次则清空actor发送队列，返回失败
                            if (FailTimes > MaxFailTimes)
                            {
                                // 失败直接删除actorproxy
                                Log.Info($"actor send message fail, actorid: {Id}");
                                GetParent<ActorLocationSenderComponent>().Remove(Id);
                                throw new RpcException(response.Error, "");
                            }

                            // 等待0.5s再发送
                            await Context.Game.GetComponent<TimeComponent>().WaitAsync(500);

                            if (InstanceId != instanceId)
                            {
                                throw new RpcException((int)ECommonErrCodeTable.ActorRemove, "");
                            }
                            ActorId = await Context.Game.GetComponent<LocationProxyComponent>().Get(Id);
                            IActorResponse iActorResponse = await Run(iActorRequest);
                            if (InstanceId != instanceId)
                            {
                                throw new RpcException((int)ECommonErrCodeTable.ActorRemove, "");
                            }
                            return iActorResponse;

                        case (int)ECommonErrCodeTable.ActorNoMailBoxComponent:
                            GetParent<ActorLocationSenderComponent>().Remove(Id);
                            throw new RpcException(response.Error, "");

                        default:
                            LastRecvTime = TimeUtils.Now();
                            FailTimes = 0;
                            break;
                    }

                    return response;
                }
                catch (Exception)
                {
                    GetParent<ActorLocationSenderComponent>().Remove(Id);
                    throw;
                }
            }
        }



        public async ETVoid Send(IActorLocationMessage message)
        {
            if (message == null)
            {
                throw new Exception("actor location send message is null");
            }
            await Run(message);
        }

        public async ETTask<IActorLocationResponse> Call(IActorLocationRequest request)
        {
            if (request == null)
            {
                throw new Exception("actor location call request is null");
            }
            return await Run(request) as IActorLocationResponse;
        }
    }
}
