﻿using ETT.Model;
using System.Collections.Generic;

namespace ETT.Server.Gate
{
    public class GateSessionKeyComponent : Component
    {
        private readonly Dictionary<long, long> m_dict = new Dictionary<long, long>();

        public void Add(long key, long account_id)
        {
            m_dict.Add(key, account_id);
            TimeoutRemoveKey(key);
        }

        public long Get(long key)
        {
            m_dict.TryGetValue(key, out long account_id);
            return account_id;
        }

        private async void TimeoutRemoveKey(long key)
        {
            await Context.Game.GetComponent<TimeComponent>().WaitAsync(20000);
            m_dict.Remove(key);
        }
    }
}
