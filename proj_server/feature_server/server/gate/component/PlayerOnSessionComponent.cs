﻿using ETT.Model;
using ETT.Model.network;
using ETT.Server.Common;
using ETT.Server.Location;

namespace ETT.Server.Gate
{
    public class PlayerOnSessionComponent : Component, IDestroy
    {
        public PlayerOnGate Player { get; set; }

        public void Destroy()
        {
            var session = Entitas as Session;
            var server = session.Root;

            // 发送断线消息
            ActorLocationSender sender = server.GetComponent<ActorLocationSenderComponent>().Get(Player.UnitId);
            sender.Send(new G2W_Ntf_SessionDisconnect()).Coroutine();

            server.GetComponent<PlayerGroupOnGateComponent>()?.Remove(Player.Id);
        }
    }
}
