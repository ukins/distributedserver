﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using System;

namespace ETT.Server.Gate
{
    [MessageHandler(EPeerType.Gate)]
    public class C2G_EnterMapHandler : AbstractRpcHandler<C2G_Req_EnterMap, G2C_Res_EnterMap>
    {
        protected override async ETTask Run(Session session, C2G_Req_EnterMap request, G2C_Res_EnterMap response, Action reply)
        {
            var server = session.Root;
            PlayerOnGate player = session.GetComponent<PlayerOnSessionComponent>().Player;
            var world_addr = await Context.Game.GetComponent<RouteTableComponent>().GetAddrAsync(EPeerType.World);
            Session world_session = server.GetComponent<NetworkInnerComponent>().Get(world_addr);
            W2G_Res_CreateUnit res_create_unit = (W2G_Res_CreateUnit)await world_session.Call(new G2W_Req_CreateUnit()
            {
                PlayerId = player.Id,
                PlayerName = player.Name,
                GateSessionId = session.InstanceId
            });
            player.UnitId = res_create_unit.UnitId;
            response.UnitId = res_create_unit.UnitId;
            reply();
        }
    }
}
