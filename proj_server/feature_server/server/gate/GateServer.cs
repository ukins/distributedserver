﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.Gate;
using ETT.Server.Location;

namespace ETT.Server
{
    public class GateServer : AbstractPeer, IAwake<string, string>
    {
        public override EPeerType PeerType => EPeerType.Gate;

        public void Awake(string addr_inner, string addr_outer)
        {
            Root = this;
            InnerAddr = addr_inner;

            var comp1 = AddComponent<NetworkInnerComponent, string>(addr_inner);
            comp1.SetServiceName("Gate_Inner");

            var comp2 = AddComponent<NetworkOuterExComponent, string>(addr_outer);
            comp2.SetServiceName("Gate_Outer");

            AddComponent<MessageDispatchComponent>();
            AddComponent<RouteComponent>();
            AddComponent<GateSessionKeyComponent>();
            AddComponent<PlayerGroupOnGateComponent>();
            AddComponent<LocationProxyComponent>();

            AddComponent<ActorLocationSenderComponent>();
            AddComponent<ActorMessageSenderComponent>();
        }
    }
}
