﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Server.proto;
using System.Collections.Generic;
using System.Linq;

namespace ETT.Server.Master
{
    using PeerDict = Dictionary<EPeerType, Dictionary<string, PeerVO>>;
    public class MasterRouteComponent : Component, IAwake, IUpdate
    {
        private int m_peer_id_generator = 1000;

        private PeerDict m_actor_dict = new PeerDict();
        private Dictionary<int, PeerVO> m_actor_id_dict = new Dictionary<int, PeerVO>();

        private List<PeerVO> m_add_list = new List<PeerVO>();
        private List<PeerVO> m_del_list = new List<PeerVO>();

        public void Awake()
        {
            m_actor_dict.Clear();
            m_actor_id_dict.Clear();
            m_add_list.Clear();
            m_del_list.Clear();

            var Peer = Root as IAbstractPeer;
            Peer.RootId = GetActorId();

            var yaml = Context.Game.GetComponent<Yaml.YamlComponent>();

            PeerVO peer = new PeerVO()
            {
                Id = Peer.RootId,
                PeerType = Peer.PeerType,
                Addr = yaml.ServerConf.Master_Addr,
                FrameRate = 0,
            };
            AddActor(peer);
        }

        public void Update()
        {
            if (m_add_list.Count > 0 || m_del_list.Count > 0)
            {
                RouterMessage message = Context.Game.GetComponent<ProtoComponent>().GetInstance((int)EServerMasterProto.Ntf_Route) as RouterMessage;
                message.AddList.Clear();
                message.AddList.AddRange(m_add_list);
                message.DelList.Clear();
                message.DelList.AddRange(m_del_list);

                var network_comp = Entitas.GetComponent<NetworkInnerComponent>();
                network_comp.SendAll(message);

                m_add_list.Clear();
                m_del_list.Clear();
            }
        }

        public List<PeerVO> GetAllActor()
        {
            return m_actor_id_dict.Values.ToList();
        }

        public PeerVO Find(int actor_id)
        {
            m_actor_id_dict.TryGetValue(actor_id, out PeerVO peer);
            return peer;
        }

        public PeerVO Find(EPeerType actor_type, string addr)
        {
            if (m_actor_dict.ContainsKey(actor_type) == false)
            {
                return null;
            }
            var dict = m_actor_dict[actor_type];
            if (dict.ContainsKey(addr) == false)
            {
                return null;
            }
            return dict[addr];
        }

        public void AddActor(PeerVO actor)
        {
            if (m_actor_dict.ContainsKey(actor.PeerType) == false)
            {
                m_actor_dict.Add(actor.PeerType, new Dictionary<string, PeerVO>());
            }

            var dict = m_actor_dict[actor.PeerType];
            var addr = actor.Addr.ToString();

            dict[addr] = actor;
            m_actor_id_dict[actor.Id] = actor;

            m_add_list.Add(actor);
        }

        public void RemoveActor(PeerVO actor)
        {
            m_actor_id_dict.Remove(actor.Id);

            if (m_actor_dict.ContainsKey(actor.PeerType) == false)
            {
                return;
            }
            var dict = m_actor_dict[actor.PeerType];
            dict.Remove(actor.Addr.ToString());

            m_del_list.Add(actor);
        }

        public int GetActorId()
        {
            return m_peer_id_generator++;
        }

    }
}
