﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.proto;
using System;

namespace ETT.Server.Master
{
    [MessageHandler(EPeerType.Master)]
    public class A2M_RouteHandler : AbstractRpcHandler<RouterRequest, RouterResponse>
    {
        protected override async ETTask Run(Session session, RouterRequest request, RouterResponse response, Action reply)
        {
            var server_master = Context.Game.GetComponent<ServerMaster>();
            if (server_master == null)
            {
                response.Error = 1;
                response.Message = "找不到ServerMaster服务器";
                goto ReplyLabel;
            }

            var route_comp = server_master.GetComponent<MasterRouteComponent>();
            if (route_comp == null)
            {
                response.Error = 2;
                response.Message = "找不到MasterRouteComponent组件";
                goto ReplyLabel;
            }
            var peer = route_comp.Find(request.ActorType, request.AccessAddr);
            if (peer != null)
            {
                response.Error = 3;
                response.Message = "ServerMaster路由组件中已存在当前Actor";
                goto ReplyLabel;
            }

            peer = new PeerVO()
            {
                PeerType = request.ActorType,
                Addr = request.AccessAddr,
                FrameRate = 0,
                Id = route_comp.GetActorId(),
            };

            route_comp.AddActor(peer);
            response.PeerId = peer.Id;
            response.PeerList = route_comp.GetAllActor();
            await ETTask.CompletedTask;

        ReplyLabel:
            reply();
        }
    }
}
