﻿using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.proto;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Server.Master
{
    [Message((int)EServerMasterProto.Req_Route)]
    public class RouterRequest : AbstractRequest
    {
        public EPeerType ActorType;
        public string AccessAddr;

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write((int)ActorType);
            writer.Write(AccessAddr);
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            ActorType = (EPeerType)reader.ReadInt32();
            AccessAddr = reader.ReadString();
        }
    }

    [Message((int)EServerMasterProto.Res_Route)]
    public class RouterResponse : AbstractResponse
    {
        public int PeerId;

        public List<PeerVO> PeerList = new List<PeerVO>();

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(PeerId);
            writer.Write(PeerList.Count);
            for (int i = 0; i < PeerList.Count; ++i)
            {
                PeerList[i].Pack(writer);
            }
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);
            PeerId = reader.ReadInt32();
            int cnt = reader.ReadInt32();
            PeerList.Clear();
            for (int i = 0; i < cnt; ++i)
            {
                PeerVO peer = new PeerVO();
                peer.UnPack(reader);
                PeerList.Add(peer);
            }
        }
    }

    [Message((int)EServerMasterProto.Ntf_Route)]
    public class RouterMessage : AbstractMessage
    {
        public List<PeerVO> AddList = new List<PeerVO>();
        public List<PeerVO> DelList = new List<PeerVO>();

        public override void Pack(BinaryWriter writer)
        {
            base.Pack(writer);
            writer.Write(AddList.Count);
            for (int i = 0; i < AddList.Count; ++i)
            {
                AddList[i].Pack(writer);
            }

            writer.Write(DelList.Count);
            for (int i = 0; i < DelList.Count; ++i)
            {
                DelList[i].Pack(writer);
            }
        }

        public override void UnPack(BinaryReader reader)
        {
            base.UnPack(reader);

            int cnt = reader.ReadInt32();
            AddList.Clear();
            for (int i = 0; i < cnt; ++i)
            {
                PeerVO peer = new PeerVO();
                peer.UnPack(reader);
                AddList.Add(peer);
            }

            cnt = reader.ReadInt32();
            DelList.Clear();
            for (int i = 0; i < cnt; ++i)
            {
                PeerVO peer = new PeerVO();
                peer.UnPack(reader);
                DelList.Add(peer);
            }
        }
    }
}
