﻿using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;

namespace ETT.Server.proto
{
    public class PeerVO : AbstractProtoPacker
    {
        public int Id;
        public EPeerType PeerType;
        public string Addr;
        public int FrameRate;//帧率，用于负载均衡

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(Id);
            writer.Write((int)PeerType);
            writer.Write(Addr.ToString());
            writer.Write(FrameRate);
        }

        public override void UnPack(BinaryReader reader)
        {
            Id = reader.ReadInt32();
            PeerType = (EPeerType)reader.ReadInt32();
            Addr = reader.ReadString();
            FrameRate = reader.ReadInt32();
        }
    }
}
