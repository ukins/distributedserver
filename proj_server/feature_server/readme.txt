﻿1.client向gate发送请求房间列表
	C2G_GetRoomListHandler
	C2G_Req_GetRoomList
	G2C_Res_GetRoomList
2.gate向world发送请求房间列表
	G2W_GetRoomListHandler
	G2W_Req_GetRoomList
	W2G_Res_GetRoomList
3.client向world发送创建房间
	C2W_CreateRoomHandler
	C2W_Req_CreateRoom
	W2C_Res_CraeteRoom
4.client向world发送申请加入房间
	C2W_ApplyJoinRoomHandler
	C2W_Req_ApplyJoinRoom
	W2C_Res_ApplyJoinRoom
	W2C_Ntf_ApplyJoinRoom
5.client向world发送审批加入房间
	C2W_DealJoinRoomHandler
	C2W_Req_DealJoinRoom
	W2C_Res_DealJoinRoom
	W2C_Ntf_DealJoinRoom
6.world向client发送比赛开始
	W2C_StartMatch（Message）
7.client向world发送move消息
	C2W_MoveHandler
	C2W_Req_Move
	W2C_Res_Move
	W2C_Ntf_Move
8.client向world发送kill消息
	C2W_KillHandler
	C2W_Req_Kill
	W2C_Res_Kill
	W2C_Ntf_Kill
9.world向client发送finish消息
	W2C_Finish(Message)





client向world发送匹配（自动加入房间）

服务器