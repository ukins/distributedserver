using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.Table
{
	public class SceneTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Desc { get; private set; }

		public string ResPath { get; private set; }

		public string[] Preload { get; private set; }

		public bool ManualCloseLoadingWnd { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Desc = br.ReadString();

				ResPath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				Preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					Preload[i] = br.ReadString();
				}

				ManualCloseLoadingWnd = br.ReadBoolean();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class SceneTable : SingleKeyTable<SceneTableConf,uint>
	{
		public override string Name { get { return "SceneTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"zhongguoxiangqi/generic/table/scene",
			@"wuziqi/generic/table/scene",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='SceneTable' >
						<field name='Id' type='uint' primarykey='true' desc='场景Id' />
						<field name='Name' type='string' desc='场景名称' />
						<field name='Desc' type='string' desc='描述' />
						<field name='ResPath' type='string' desc='资源路径' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
						<field name='ManualCloseLoadingWnd' type='bool' desc='场景加载完成手动关闭loading界面' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
