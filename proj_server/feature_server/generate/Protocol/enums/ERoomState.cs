using ETT.Model;

namespace ETT.Server
{
	public enum ERoomState
	{
		Invalid = 0,
		Wait = 1,
		Ready = 2,
		Battle = 3,
		MAX = 4,
	}
}
