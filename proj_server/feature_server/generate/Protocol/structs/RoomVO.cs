using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Server
{
	public partial class RoomVO : AbstractProtoPacker
	{
		/// <summary>
		/// 房间id
		/// <summary>
		public uint Roomid;

		/// <summary>
		/// 房间状态
		/// <summary>
		public ERoomState Roomstate;

		/// <summary>
		/// 房间状态
		/// <summary>
		public List<PlayerVO> Players;

		public override void Pack(BinaryWriter writer)
		{
			writer.Write(Roomid);
			writer.Write((int)Roomstate);
			writer.Write(Players.Count);
			for (int i = 0; i < Players.Count; i++)
			{
				Players[i].Pack(writer);
			}
		}

		public override void UnPack(BinaryReader reader)
		{
			Roomid = reader.ReadUInt32();
			Roomstate = (ERoomState)reader.ReadInt32();
			int len = reader.ReadInt32();
			Players = new List<PlayerVO>(len); 
			for (int i = 0; i < len; i++)
			{
				Players.Add(new PlayerVO());
				Players[i].UnPack(reader);
			}
		}
	}
}
