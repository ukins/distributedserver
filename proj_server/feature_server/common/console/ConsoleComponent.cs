﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ETT.Server
{
    public class ConsoleComponent : Component, IAwake, IStart
    {
        public CancellationTokenSource CancellationTokenSource;

        public void Awake()
        {
            CancellationTokenSource = new CancellationTokenSource();
        }

        public void Start()
        {
            StartAsync().Coroutine();
        }

        private async ETVoid StartAsync()
        {
            while (true)
            {
                try
                {
                    string line = await Task.Factory.StartNew(() =>
                    {
                        Console.Write("> ");
                        return Console.In.ReadLine();
                    }, CancellationTokenSource.Token);

                    line = line.Trim();
                    string[] param_lst = Regex.Split(line, @"\s+");
                    foreach(var s in param_lst)
                    {
                        Log.Debug(s);
                    }
                    Context.Game.GetComponent<CommandComponent>().Parse(param_lst);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }
        }
    }
}
