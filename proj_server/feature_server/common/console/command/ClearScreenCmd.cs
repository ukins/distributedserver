﻿using ETT.Model.Command;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server
{
    [CommandOption("clear")]
    public class ClearScreenCmd : AbstractCmd
    {
        public override bool Check(string[] args)
        {
            return true;
        }

        public override void Execute()
        {
            Console.Clear();
        }
    }
}
