﻿using ETT.Model;
using ETT.Server.Table;
using ETT.Table;
using System.Collections.Generic;
using System.Linq;

namespace ETT.Server
{
    public partial class TableComponent : Component, IAwake<string>
    {
        //private readonly Dictionary<string, IAbstractTable> m_tabledict = new Dictionary<string, IAbstractTable>();
        private readonly List<IAbstractTable> m_list = new List<IAbstractTable>();

        public List<IAbstractTable> FindAll()
        {
            return m_list;
        }
        //public IAbstractTable Find(string name)
        //{
        //    if (m_tabledict.ContainsKey(name) == true)
        //    {
        //        return m_tabledict[name];
        //    }
        //    return null;
        //}

        private AbstractTableParser m_parser;

        private ShieldWordTree m_shield;

        public string ResPath { get; private set; }

        public void Awake(string res_path)
        {
            ResPath = res_path;

            InnerInit();

            m_parser = new TextParser(this);

            m_parser.Parse();

            m_shield = new ShieldWordTree(Context.Table.GetComponent<ShieldwordTable>());

            Log.Debug("TableComponent Awake");
        }

        public bool Match(char[] word)
        {
            return m_shield.Match(word);
        }
    }
}
