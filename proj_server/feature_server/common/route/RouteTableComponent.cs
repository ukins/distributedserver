﻿using ETT.Model;
using ETT.Model.server;
using ETT.Server.proto;
using System.Collections.Generic;
using System.Net;

namespace ETT.Server
{
    using ActorDict = Dictionary<EPeerType, Dictionary<string, PeerVO>>;

    public class RouteTableComponent : Component
    {
        protected ActorDict m_actor_peer_dict = new ActorDict();

        public async ETTask<IPEndPoint> GetAddrAsync(EPeerType eActorType)
        {
            var time_comp = Context.Game.GetComponent<TimeComponent>();
            IPEndPoint addr = null;
            for (int i = 0; i < 3; ++i)
            {
                m_actor_peer_dict.TryGetValue(eActorType, out var dict);
                if (dict == null)
                {
                    await time_comp.WaitAsync(500);
                }
                else
                {
                    addr = ServerDefine.ToIPEndPoint(dict.Any().Addr);
                    break;
                }
            }
            return addr;
        }

        public IPEndPoint GetAddr(int peerid)
        {
            var time_comp = Context.Game.GetComponent<TimeComponent>();
            IPEndPoint addr = null;
            PeerVO p = null;
            foreach (var dict in m_actor_peer_dict.Values)
            {
                foreach (var vo in dict.Values)
                {
                    if (vo.Id == peerid)
                    {
                        p = vo;
                        break;
                    }
                }
                if (p != null)
                {
                    break;
                }
            }
            if (p != null)
            {
                addr = ServerDefine.ToIPEndPoint(p.Addr);
            }
            return addr;
        }

        public void AddActor(PeerVO actor)
        {
            if (m_actor_peer_dict.ContainsKey(actor.PeerType) == false)
            {
                m_actor_peer_dict.Add(actor.PeerType, new Dictionary<string, PeerVO>());
            }

            var dict = m_actor_peer_dict[actor.PeerType];
            var addr = actor.Addr.ToString();

            dict[addr] = actor;
        }

        public void RemoveActor(PeerVO actor)
        {
            if (m_actor_peer_dict.ContainsKey(actor.PeerType) == false)
            {
                return;
            }
            var dict = m_actor_peer_dict[actor.PeerType];
            dict.Remove(actor.Addr.ToString());
        }
    }
}
