﻿using ETT.Model;
using ETT.Model.mailbox;
using ETT.Model.network;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server
{
    [MailBoxHandler(EMailBoxType.Dispatcher)]
    public class MailBoxActorDispatcherHandler : IMailBoxHandler
    {
        public async ETTask Handle(Session session, Entitas entitas, object actorMessage)
        {
            try
            {
                await session.Root.GetComponent<ActorMessageDispatchComponent>().Handle(session, entitas, actorMessage as IProtoPacker);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }
    }
}
