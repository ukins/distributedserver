﻿using ETT.Model;
using ETT.Model.network;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server
{
    public static class ProtoSystemEx
    {
        // 客户端为了0GC需要消息池，服务端消息需要跨协程不需要消息池
        public static IProtoPacker GetInstance(this ProtoComponent system, uint proto_id)
        {
            Type type = system.GetType(proto_id);
            if (type == null)
            {
                // 服务端因为有人探测端口，有可能会走到这一步，如果找不到proto_id，抛异常
                throw new Exception($"not found proto: {proto_id}");
            }
            return Activator.CreateInstance(type) as IProtoPacker;
        }
    }
}
