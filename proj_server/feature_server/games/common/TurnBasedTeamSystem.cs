﻿using ETT.Model;

namespace ETT.Server.World
{
    /// <summary>
    /// 回合制组队系统
    /// </summary>
    public class TurnBasedTeamSystem : Component, IAwake<int>
    {
        private ITurnBasedTeamMember[] m_unit_arr;
        private ITurnBasedTeamMember[] m_wait_arr;
        private int m_cnt = 0;

        public ITurnBasedTeamMember[] GetAll()
        {
            return m_unit_arr;
        }

        public ITurnBasedTeamMember Get(int index)
        {
            if (index >= 0 && index < m_cnt)
            {
                return m_unit_arr[index];
            }
            return null;
        }

        public ITurnBasedTeamMember Get(long id)
        {
            foreach (ITurnBasedTeamMember unit in m_unit_arr)
            {
                if (unit.Id == id)
                {
                    return unit;
                }
            }
            return null;
        }

        private int GetIndex(ITurnBasedTeamMember unit)
        {
            for (int i = 0; i < m_unit_arr.Length; ++i)
            {
                if (m_unit_arr[i].Id == unit.Id)
                {
                    return i;
                }
            }
            return -1;
        }

        public bool IsEnought => UnitCnt() == m_cnt;

        private int UnitCnt()
        {
            int cnt = 0;
            for (int i = 0; i < m_unit_arr.Length; ++i)
            {
                if (m_unit_arr[i] != null)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        private int WaitCnt()
        {
            int cnt = 0;
            for (int i = 0; i < m_wait_arr.Length; ++i)
            {
                if (m_wait_arr[i] != null)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public void Awake(int cnt)
        {
            m_cnt = cnt;
            m_unit_arr = new ITurnBasedTeamMember[cnt];
            m_wait_arr = new ITurnBasedTeamMember[cnt - 1];
        }

        public void Add(ITurnBasedTeamMember unit)
        {
            for (int i = 0; i < m_unit_arr.Length; ++i)
            {
                if (m_unit_arr[i] == null)
                {
                    m_unit_arr[i] = unit;
                    unit.TurnIndex = i;
                    break;
                }
            }
        }

        public void AddFromWait(long id)
        {
            ITurnBasedTeamMember player = null;
            for (int i = 0; i < m_wait_arr.Length; ++i)
            {
                if (m_wait_arr[i] != null && m_wait_arr[i].Id == id)
                {
                    player = m_wait_arr[i];
                    break;
                }
            }
            if (player != null)
            {
                Add(player);
            }
        }

        public bool AddWait(ITurnBasedTeamMember unit)
        {
            if (UnitCnt() + WaitCnt() >= m_cnt)
            {
                return false;
            }

            for (int i = 0; i < m_wait_arr.Length; ++i)
            {
                if (m_wait_arr[i] == null)
                {
                    m_wait_arr[i] = unit;
                    return true;
                }
            }

            return false;
        }

        public void DelWait(ITurnBasedTeamMember unit)
        {
            for (int i = 0; i < m_wait_arr.Length; ++i)
            {
                if (m_wait_arr[i] == unit)
                {
                    m_wait_arr[i] = null;
                    break;
                }
            }
        }

        public void DelWait(long id)
        {
            for (int i = 0; i < m_wait_arr.Length; ++i)
            {
                if (m_wait_arr[i] != null && m_wait_arr[i].Id == id)
                {
                    m_wait_arr[i] = null;
                    break;
                }
            }
        }

        public ITurnBasedTeamMember CurrUnit { get; private set; }

        public async void StartMatch()
        {
            await Context.Game.GetComponent<TimeComponent>().WaitAsync(1000 * 5);
            NextTurn();
        }

        public void FinishMatch()
        {

        }

        public void NextTurn()
        {
            int curr_index = -1;
            if (CurrUnit != null)
            {
                CurrUnit.TurnOff();
                curr_index = GetIndex(CurrUnit);
            }
            curr_index = (curr_index + 1) % m_cnt;
            CurrUnit = m_unit_arr[curr_index];
            CurrUnit.TurnOn();
        }

        public void Quit()
        {
            for (int i = 0; i < m_unit_arr.Length; ++i)
            {
                var unit = m_unit_arr[i];
                if (unit == null)
                {
                    continue;
                }
                unit.Quit();
                m_unit_arr[i] = null;
            }
        }

        public void Broadcast(IActorLocationMessage message,
           long except_unit_id = 0)
        {
            foreach (var unit in m_unit_arr)
            {
                if (unit.Id == except_unit_id)
                {
                    continue;
                }
                unit.Unicast(message);
            }
        }
    }
}
