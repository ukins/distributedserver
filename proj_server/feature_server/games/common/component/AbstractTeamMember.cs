﻿using ETT.Model;
using ETT.Server.Location;

namespace ETT.Server.World
{
    /// <summary>
    /// 棋手类
    /// </summary>
    public abstract class AbstractTeamMember : Component, ITurnBasedTeamMember
    {
        public int TurnIndex { get; set; }

        public long Id => unit.Id;

        public Unit unit => GetParent<Unit>();

        public abstract void TurnOn();

        public abstract void TurnOff();

        public void Quit()
        {
            TurnIndex = 0;
        }

        public void Unicast(IActorLocationMessage message)
        {
            UnitAgentComponent unitComp = unit.GetComponent<UnitAgentComponent>();
            if (unitComp.IsDisconnect == true)
            {
                return;
            }
            var comp = unit.Root.GetComponent<ActorMessageSenderComponent>();
            ActorMessageSender sender = comp.Get(unitComp.GateSessionActorId);
            sender.Send(message);
        }
    }
}
