namespace ETT.Server.Common
{
	/// <summary>
	/// Range [0,110000)
	/// <summary>
	public enum ECommonErrCodeTable
	{
		//成功
		Success = 0,
		//找不到Actor
		NotFoundActor = 100002,
		//Actor没有Mailbox组件
		ActorNoMailBoxComponent = 100003,
		//Location服务器上找不到对应的Actor
		ActorLocationNotFound = 100004,
		//玩家不存在
		NoPlayer = 100005,
		//房间数量已达上限
		RoomCntMaxLimit = 100006,
		//房间不存在
		NoRoom = 100007,
		//房间重复
		RoomDuplicate = 100008,
		//该房间已开始对局
		RoomWithMatching = 100009,
		//房间信息无效
		InvalidRoomInfo = 100010,
		//房间申请者信息无效
		InvalidApplier = 100011,
		//不是房主，不能解散房间
		CannotDisband = 100012,
		//房间已经匹配完成，无法退出
		CannotExit = 100013,
		//已申请加入，请耐心等待
		AlreadyApply = 100014,
		//房主正在处理其他玩家的加入请求，请耐心等待
		OnProcessing = 100015,
		//申请人数已满
		ApplierIsFull = 100016,
		//当前棋子不可用
		ChessmanIsNotAvailable = 100017,
		//当前棋子非己方
		NotYourChessman = 100018,
		//当前不是你的回合
		NotYourTurn = 100019,
		//无法移动
		CannotMove = 100020,
		//无法攻击
		CannotKill = 100021,
		//账号不存在
		NoAccount = 100022,
		//创建账号失败
		CreateFail = 100023,
		//账号已存在
		AlreadyExist = 100024,
		//用户名包含非法字符
		InvalidChar = 100025,
		//Actor无效
		ActorRemove = 100026,
	}

}
