namespace ETT.Server.Common
{
	/// <summary>
	/// Range [0,1000)
	/// <summary>
	public enum ECommonWndTable
	{
		//通用加载窗口
		SceneLoading = 1,
		//通用对话框
		Dialog = 2,
		//通用提示框
		Tips = 3,
		//指令窗口
		Command = 4,
		//选择游戏界面
		SelectGame = 10,
	}

}
