namespace ETT.Server.Common
{
	/// <summary>
	/// Common协议枚举 Range[10000,20000)
	/// <summary>
	public enum ECommonProto
	{
		C2L_Req_Login = 10000,
		L2C_Res_Login = 10001,
		C2G_Req_Login = 10002,
		G2C_Res_Login = 10003,
		L2D_Req_Login = 10004,
		D2L_Res_Login = 10005,
		L2G_Req_Login = 10006,
		G2L_Res_Login = 10007,
		C2G_Req_EnterMap = 10100,
		G2C_Res_EnterMap = 10101,
		G2W_Req_CreateUnit = 10102,
		W2G_Res_CreateUnit = 10103,
		G2W_Ntf_SessionDisconnect = 10200,
	}

}
