﻿using ETT.AI.Common;
using ETT.AI.ZhongGuoXiangQi;
using ETT.Model;
using System;
using System.Collections.Generic;

namespace ETT.Server.ZhongGuoXiangQi
{
    public class ChessPieceComponent : Component, IAwake<ChessmanTableConf>, IAbstractChessPieceEx
    {
        public ChessmanTableConf Conf { get; private set; }

        #region Rule

        public AbstractChessPiece ChessPiece { get; private set; }

        public EChessPieceType Chessmantype => ChessPiece.Chessmantype;

        public List<UVector2Int> FindPoints()
        {
            return ChessPiece.FindPoints();
        }

        public bool CanKill(UVector2Int point)
        {
            return ChessPiece.CanKill(point);
        }

        public void Kill(UVector2Int point)
        {
            ChessPiece.Kill(point);
        }

        public bool CanMove(int x, int y)
        {
            return ChessPiece.CanMove(x, y);
        }

        public bool CanMove(UVector2Int point)
        {
            return ChessPiece.CanMove(point);
        }

        public void Move(int x, int y)
        {
            ChessPiece.Move(x, y);
        }

        public void Move(UVector2Int vec)
        {
            ChessPiece.Move(vec);
        }

        #endregion

        public void Awake(ChessmanTableConf conf)
        {
            Conf = conf;

            ChessPiece = Create(conf);

            ChessPiece.Move(conf.BirthPoint);
        }


        private AbstractChessPiece Create(ChessmanTableConf conf)
        {
            EChessPieceFaction faction = conf.Id <= 16 ? EChessPieceFaction.RED : EChessPieceFaction.BLACK;
            AbstractChessPiece rule = null;
            var kind = (EChessPieceType)Enum.Parse(typeof(EChessPieceType), conf.Type);
            var parent = GetParent<ChessPieceEntity>();
            switch (kind)
            {
                case EChessPieceType.Soldier:
                    rule = Root.Factory.CreateWithId<Soldier>();
                    break;
                case EChessPieceType.Cannon:
                    rule = Root.Factory.CreateWithId<Cannon>();
                    break;
                case EChessPieceType.Chariot:
                    rule = Root.Factory.CreateWithId<Chariot>();
                    break;
                case EChessPieceType.Knight:
                    rule = Root.Factory.CreateWithId<Knight>();
                    break;
                case EChessPieceType.Minister:
                    rule = Root.Factory.CreateWithId<Minister>();
                    break;
                case EChessPieceType.Scholar:
                    rule = Root.Factory.CreateWithId<Scholar>();
                    break;
                case EChessPieceType.General:
                    rule = Root.Factory.CreateWithId<General>();
                    break;
            }
            if (rule != null)
            {
                rule.ChessPiece = GetParent<ChessPieceEntity>();
                rule.ChessPiece.Faction = (int)faction;
            }
            return rule;
        }

    }
}
