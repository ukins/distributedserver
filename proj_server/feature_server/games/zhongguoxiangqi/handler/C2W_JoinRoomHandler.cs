﻿using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.Table;
using ETT.Server.World;
using System;

namespace ETT.Server.ZhongGuoXiangQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_JoinRoomHandler : AbstractActorRpcHandler<Unit, C2W_Req_JoinRoom, W2C_Res_JoinRoom>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_JoinRoom request, W2C_Res_JoinRoom response, Action reply)
        {
            Log.Debug("C2W_JoinRoomHandler");

            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var room = roomMgr.Get(request.RoomId);
            if (room == null)
            {
                response.Error = (int)ECommonErrCodeTable.NoRoom;
                reply();
                return;
            }

            unit.GetComponent<UnitAgentComponent>().Room = room;

            var mem = unit.AddComponent<TeamMember>();

            var sys_team = room.GetComponent<TurnBasedTeamSystem>();
            var result = sys_team.AddWait(mem);

            if (result == false)
            {
                response.Error = (int)ECommonErrCodeTable.ApplierIsFull;
                reply();
                return;
            }

            //成功加入申请队列，并等待房主通过
            reply();

            //通知房主
            var wagent = unit.GetComponent<UnitAgentComponent>();

            W2C_Ntf_JoinRoom ntf = new W2C_Ntf_JoinRoom();
            ntf.Applier = new PlayerVO
            {
                //使用unitid，而非wagent.playerid
                PlayerId = unit.Id,
                PlayerName = wagent.Name
            };

            sys_team.Get(0).Unicast(ntf);

            await ETTask.CompletedTask;
        }
    }
}
