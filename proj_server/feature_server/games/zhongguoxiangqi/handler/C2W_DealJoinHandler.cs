﻿using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.Location;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.ZhongGuoXiangQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_DealJoinHandler : AbstractActorRpcHandler<Unit, C2W_Req_DealJoin, W2C_Res_DealJoin>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_DealJoin request, W2C_Res_DealJoin response, Action reply)
        {
            Log.Debug("C2W_DealJoinHandler");

            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var agent = unit.GetComponent<UnitAgentComponent>();
            var room = agent.Room;
            var sys_team = room.GetComponent<TurnBasedTeamSystem>();

            reply();

            if (request.IsAccept == false)
            {
                sys_team.DelWait(request.PlayerId);
                agent.Room = null;
            }
            else
            {
                sys_team.AddFromWait(request.PlayerId);
            }

            var members = sys_team.GetAll();

            //通知申请者
            var wagent = unit.GetComponent<UnitAgentComponent>();

            W2C_Ntf_DealJoin ntf = new W2C_Ntf_DealJoin();
            ntf.IsAccept = request.IsAccept;
            ntf.Accepter = new PlayerVO()
            {
                PlayerId = wagent.PlayerId,
                PlayerName = wagent.Name
            };

            sys_team.Get(request.PlayerId).Unicast(ntf);

            if (sys_team.IsEnought == false)
            {
                return;
            }
            sys_team.StartMatch();

            //通知比赛开始
            W2C_Ntf_StartMatch ntf_startmatch = new W2C_Ntf_StartMatch();
            //5s之后开始比赛
            ntf_startmatch.StartTime = TimeUtils.ClientNow() + 5000;
            ntf_startmatch.RoomVO = new RoomVO
            {
                Roomid = agent.Room.SerialId,
                Roomstate = ERoomState.Battle,
                Players = new List<PlayerVO>()
            };
            var players = ntf_startmatch.RoomVO.Players;
            players.Clear();
            for (int i = 0; i < members.Length; ++i)
            {
                var uagent = members[i].unit.GetComponent<UnitAgentComponent>();
                players.Add(new PlayerVO() { PlayerId = uagent.PlayerId, PlayerName = uagent.Name });
            }

            sys_team.Broadcast(ntf_startmatch);

            await ETTask.CompletedTask;
        }
    }
}
