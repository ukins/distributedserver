﻿using ETT.AI.Common;
using ETT.AI.ZhongGuoXiangQi;
using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.Location;
using ETT.Server.Table;
using ETT.Server.World;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Server.ZhongGuoXiangQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_MoveHandler : AbstractActorRpcHandler<Unit, C2W_Req_Move, W2C_Res_Move>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_Move request, W2C_Res_Move response, Action reply)
        {
            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var agent = unit.GetComponent<UnitAgentComponent>();

            TurnBasedTeamSystem sys_team = null;
            int err_code = 0;
            if (agent.Room == null)
            {
                err_code = (int)ECommonErrCodeTable.NoRoom;
                goto ReplyLabel;
            }

            sys_team = agent.Room.GetComponent<TurnBasedTeamSystem>();
            if (sys_team.CurrUnit.Id != unit.Id)
            {
                err_code = (int)ECommonErrCodeTable.NotYourTurn;
                goto ReplyLabel;
            }
            var chess_board = agent.Room.GetComponent<ChessBoardEntity>();
            var chess_piece = chess_board[request.chess.id];
            if (chess_piece == null || chess_piece.IsAvailable == false)
            {
                err_code = (int)ECommonErrCodeTable.ChessmanIsNotAvailable;
                goto ReplyLabel;
            }
            var chess_piece_agent = chess_piece.GetComponent<ChessPieceComponent>();
            var turnbased_comp = unit.GetComponent<TeamMember>();
            EChessPieceFaction faction = (EChessPieceFaction)(turnbased_comp.TurnIndex + 1);
            if ((int)faction != chess_piece.Faction)
            {
                err_code = (int)ECommonErrCodeTable.NotYourChessman;
                goto ReplyLabel;
            }

            bool result = chess_piece_agent.CanMove(request.chess.x, request.chess.y);
            if (result == false)
            {
                err_code = (int)ECommonErrCodeTable.CannotMove;
                goto ReplyLabel;
            }

            chess_piece_agent.Move(request.chess.x, request.chess.y);
            ReplyLabel:

            response.Error = err_code;
            response.chess = request.chess;
            reply();

            if (err_code == 0)
            {
                W2C_Ntf_Move ntf = new W2C_Ntf_Move()
                {
                    chess = request.chess
                };

                sys_team.Broadcast(ntf, unit.Id);
                sys_team.NextTurn();
            }

            await ETTask.CompletedTask;
        }
    }
}
