﻿using ETT.AI.Common;
using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.World;
using System;

namespace ETT.Server.WuZiQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_CreateRoomHandler : AbstractActorRpcHandler<Unit, C2W_Req_CreateRoom, W2C_Res_CreateRoom>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_CreateRoom request, W2C_Res_CreateRoom response, Action reply)
        {
            Log.Debug("C2W_CreateRoomHandler");

            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();
            var agent = unit.GetComponent<UnitAgentComponent>();

            int err_code = 0;
            if (agent.Room != null)
            {
                err_code = (int)ECommonErrCodeTable.RoomDuplicate;
                goto ReplyLabel;
            }

            var room = server.Factory.CreateWithId<Room>();
            room.Parent = roomMgr;
            roomMgr.Add(room);

            agent.Room = room;

            var mem = unit.AddComponent<TeamMember>();

            var turnbased_comp = room.AddComponent<TurnBasedTeamSystem, int>(Define.GamerCnt);
            turnbased_comp.Add(mem);

            var board_entity = room.AddComponent<ChessBoardEntity>();
            board_entity.AddComponent<ChessBoardComponent>();

            response.RoomId = room.SerialId;

            ReplyLabel:

            response.Error = err_code;
            reply();

            await ETTask.CompletedTask;
        }
    }
}
