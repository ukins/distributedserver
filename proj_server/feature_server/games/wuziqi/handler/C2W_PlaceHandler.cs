﻿using ETT.Model;
using ETT.Model.proto;
using ETT.Model.server;
using ETT.Server.Common;
using ETT.Server.World;
using System;

namespace ETT.Server.WuZiQi.Battle
{
    [ActorMessageHandler(EPeerType.World)]
    public class C2W_PlaceHandler : AbstractActorRpcHandler<Unit, C2W_Req_Place, W2C_Res_Place>
    {
        protected override async ETTask Run(Unit unit, C2W_Req_Place request, W2C_Res_Place response, Action reply)
        {
            var server = unit.Root;
            var chess_ent = server.GetComponent<ChessEntitas>();
            var roomMgr = chess_ent.GetComponent<RoomManager>();

            var agent = unit.GetComponent<UnitAgentComponent>();
            if (agent.Room == null)
            {
                response.Error = (int)ECommonErrCodeTable.NoRoom;
                reply();
                return;
            }
            //var chess_board = agent.Room.GetComponent<ChessBoardEntity>();
            //var chessman = chess_board[request.chessid];
            //if (chessman == null || chessman.IsAvailable == false)
            //{
            //    response.Error = (int)ECommonErrCodeTable.ChessmanIsNotAvailable;
            //    reply();
            //    return;
            //}

            reply();

            var sys_team = agent.Room.GetComponent<TurnBasedTeamSystem>();

            W2C_Ntf_Place ntf_select = new W2C_Ntf_Place
            {
                chessid = request.chessid
            };

            sys_team.Broadcast(ntf_select, unit.Id);

            await ETTask.CompletedTask;
        }
    }
}
