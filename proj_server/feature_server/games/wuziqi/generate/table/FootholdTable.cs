using ETT.Server.Table;
using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Server.WuZiQi
{
	public class FootholdTableConf : SingleKeyItem<UVector2Int>
	{
		public override UVector2Int PrimaryKey => Pos;

		public UVector2Int Pos { get; private set; }

		public Coordinate Axis { get; private set; } = new Coordinate();

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Pos = UVector2Int.Parse(br.ReadString());

				Axis.Deserialize(br);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class FootholdTable : SingleKeyTable<FootholdTableConf,UVector2Int>
	{
		public override string Name { get { return "FootholdTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"wuziqi/battle/table/foothold",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='FootholdTable' isseparate='true' >
						<field name='Pos' type='vector2int' primarykey='true' desc='坐标点位' />
						<field name='Axis' type='Coordinate' isvertical='false' >
							<field name='X' type='float' desc='世界坐标X轴' />
							<field name='Y' type='float' desc='世界坐标Y轴' />
						</field>
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				UVector2Int primarykey = UVector2Int.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				UVector2Int range1 = UVector2Int.Parse(min);
				UVector2Int range2 = UVector2Int.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
