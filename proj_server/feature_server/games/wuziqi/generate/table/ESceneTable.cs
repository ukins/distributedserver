namespace ETT.Server.WuZiQi
{
	/// <summary>
	/// Range [2000,3000)
	/// <summary>
	public enum EGenericSceneTable
	{
		//登陆场景
		scene_login = 2000,
		//大厅场景
		scene_lobby = 2001,
		//比赛场景
		scene_battle = 2002,
	}

}
