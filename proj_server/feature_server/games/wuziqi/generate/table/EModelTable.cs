namespace ETT.Server.WuZiQi
{
	/// <summary>
	/// Range [2000,3000)
	/// <summary>
	public enum EGenericModelTable
	{
		//精灵
		Elven = 2000,
		//哥布林
		Goblin = 2001,
		//人类
		Human = 2002,
		//亡灵
		Undead = 2003,
	}

	/// <summary>
	/// Range [3000,4000)
	/// <summary>
	public enum EBattleModelTable
	{
		//红子
		Red = 3000,
		//黑子
		Black = 3001,
		//路径点
		PathPoint = 3002,
	}

}
