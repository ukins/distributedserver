namespace ETT.Server.WuZiQi
{
	/// <summary>
	/// Range [2000,3000)
	/// <summary>
	public enum EGenericWndTable
	{
		//登陆窗口
		Login = 2001,
		//大厅窗口
		Lobby = 2010,
		//匹配界面
		Match = 2011,
		//加入房间界面
		JoinRoom = 2012,
		//战斗窗口
		Battle = 2020,
		//操作窗口
		Operate = 2021,
	}

}
