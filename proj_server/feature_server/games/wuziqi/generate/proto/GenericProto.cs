using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Server.WuZiQi
{

	/// <summary>
	/// 请求创建房间 【Client, World】
	/// </summary>
	[Message((uint)EGenericProto.C2W_Req_CreateRoom)]
	public class C2W_Req_CreateRoom : AbstractLocationRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应创建房间 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Res_CreateRoom)]
	public class W2C_Res_CreateRoom : AbstractLocationResponse
	{
		public uint RoomId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(RoomId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomId = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 请求退出房间 【Client, World】
	/// </summary>
	[Message((uint)EGenericProto.C2W_Req_QuitRoom)]
	public class C2W_Req_QuitRoom : AbstractLocationRequest
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应退出房间 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Res_QuitRoom)]
	public class W2C_Res_QuitRoom : AbstractLocationResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求加入房间 【Client, World】
	/// </summary>
	[Message((uint)EGenericProto.C2W_Req_JoinRoom)]
	public class C2W_Req_JoinRoom : AbstractLocationRequest
	{
		public uint RoomId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(RoomId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			RoomId = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 响应加入房间 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Res_JoinRoom)]
	public class W2C_Res_JoinRoom : AbstractLocationResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 加入房间 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Ntf_JoinRoom)]
	public class W2C_Ntf_JoinRoom : AbstractLocationMessage
	{
		public PlayerVO Applier {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			Applier.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			Applier = new PlayerVO();
			Applier.UnPack(reader);
		}
	}

	/// <summary>
	/// 处理加入请求 【Client, World】
	/// </summary>
	[Message((uint)EGenericProto.C2W_Req_DealJoin)]
	public class C2W_Req_DealJoin : AbstractLocationRequest
	{
		public bool IsAccept {get; set;}
		public long PlayerId {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(IsAccept);
			writer.Write(PlayerId);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			IsAccept = reader.ReadBoolean();
			PlayerId = reader.ReadInt64();
		}
	}

	/// <summary>
	/// 处理加入请求 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Res_DealJoin)]
	public class W2C_Res_DealJoin : AbstractLocationResponse
	{

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
		}
	}

	/// <summary>
	/// 响应加入房间 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Ntf_DealJoin)]
	public class W2C_Ntf_DealJoin : AbstractLocationMessage
	{
		public bool IsAccept {get; set;}
		public PlayerVO Accepter {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(IsAccept);
			Accepter.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			IsAccept = reader.ReadBoolean();
			Accepter = new PlayerVO();
			Accepter.UnPack(reader);
		}
	}

	/// <summary>
	/// 开始比赛通知 【World, Client】
	/// </summary>
	[Message((uint)EGenericProto.W2C_Ntf_StartMatch)]
	public class W2C_Ntf_StartMatch : AbstractLocationMessage
	{
		public long StartTime {get; set;}
		public RoomVO RoomVO {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(StartTime);
			RoomVO.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			StartTime = reader.ReadInt64();
			RoomVO = new RoomVO();
			RoomVO.UnPack(reader);
		}
	}
}
