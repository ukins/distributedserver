namespace ETT.Server.WuZiQi
{
	/// <summary>
	/// Generic协议枚举 Range[30000,35000)
	/// <summary>
	public enum EGenericProto
	{
		C2W_Req_CreateRoom = 30200,
		W2C_Res_CreateRoom = 30201,
		C2W_Req_QuitRoom = 30205,
		W2C_Res_QuitRoom = 30206,
		C2W_Req_JoinRoom = 30210,
		W2C_Res_JoinRoom = 30211,
		W2C_Ntf_JoinRoom = 30212,
		C2W_Req_DealJoin = 30215,
		W2C_Res_DealJoin = 30216,
		W2C_Ntf_DealJoin = 30217,
		W2C_Ntf_StartMatch = 30220,
	}

}
