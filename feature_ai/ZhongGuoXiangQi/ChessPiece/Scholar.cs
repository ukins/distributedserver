﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 仕 士 类
    /// </summary>
    public class Scholar : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.Scholar;

        private static readonly UVector2Int[] m_vecArr = new UVector2Int[] {
            new UVector2Int(1, 1),
            new UVector2Int(-1, 1),
            new UVector2Int(1, -1),
            new UVector2Int(-1, -1)
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 3 || point.x > 5)
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.RED && (point.y < 0 || point.y > 2))
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.BLACK && (point.y < 7 || point.y > 9))
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            ChessPieceEntity chessman = Board[point];
            if (chessman != null && chessman.Faction == (int)Faction)
            {
                return false;
            }
            return true;
        }

        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                if (CheckPoint(CurrPos + m_vecArr[i]) == true)
                {
                    m_points.Add(CurrPos + m_vecArr[i]);
                }
            }

            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int vec = point - CurrPos;
            for (int i = 0; i < m_vecArr.Length; i++)
            {
                if (m_vecArr[i].Equals(vec))
                {
                    return true;
                }
            }
            return false;
        }

    }
}
