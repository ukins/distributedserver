﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    public interface IAbstractChessPiece
    {
        EChessPieceType Chessmantype { get; }
        List<UVector2Int> FindPoints();
        bool CanKill(UVector2Int point);
        void Kill(UVector2Int point);
        bool CanMove(int x, int y);
        bool CanMove(UVector2Int point);
        void Move(int x, int y);
        void Move(UVector2Int point);
    }

    public interface IAbstractChessPieceEx : IAbstractChessPiece
    {
        AbstractChessPiece ChessPiece { get; }
    }
}
