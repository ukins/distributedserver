﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    public abstract class AbstractChessPiece : ComponentWithId, IAwake,
        IAbstractChessPiece
    {
        public abstract EChessPieceType Chessmantype { get; }

        public int Faction { get => ChessPiece.Faction; }

        public ChessPieceEntity ChessPiece { get; set; }

        public ChessBoardEntity Board => ChessPiece.Board;

        public UVector2Int CurrPos => ChessPiece.CurrPos;

        protected bool IsOneDimensionalVec(UVector2Int vec)
        {
            if (vec.x == 0 && vec.y == 0)
            {
                return false;
            }
            if (vec.x == 0 || vec.y == 0)
            {
                return true;
            }
            return false;
        }

        public void Awake()
        {
        }


        public abstract List<UVector2Int> FindPoints();

        public abstract bool CanKill(UVector2Int point);

        public virtual void Kill(UVector2Int point)
        {
            var defender = Board[point];
            var def_agent = defender.GetComponentInHerit<IAbstractChessPiece>();

            ChessPiece.SetPos(defender.CurrPos);

            def_agent.Move(new UVector2Int(int.MaxValue, int.MaxValue));
        }

        public bool CanMove(UVector2Int point)
        {
            if (ChessPiece.Board[point] != null)
            {
                return false;
            }

            bool canmove = false;
            var points = FindPoints();
            foreach (var v in points)
            {
                if (point.Equals(v) == true)
                {
                    canmove = true;
                    break;
                }
            }
            if (canmove == false)
            {
                return false;
            }
            return true;
        }
        public bool CanMove(int x, int y)
        {
            UVector2Int vec = new UVector2Int(x, y);
            return CanMove(vec);
        }

        public void Move(UVector2Int point)
        {
            ChessPiece.SetPos(point);
        }

        public void Move(int x, int y)
        {
            UVector2Int vec = new UVector2Int(x, y);
            Move(vec);
        }

    }
}
