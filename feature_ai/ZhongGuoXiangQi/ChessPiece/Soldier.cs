﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 兵，卒 类
    /// </summary>
    public class Soldier : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.Soldier;

        private static readonly UVector2Int[] m_redCrossNot = new UVector2Int[]{
            new UVector2Int(0, 1),
        };
        private static readonly UVector2Int[] m_redCross = new UVector2Int[]{
            new UVector2Int(0, 1),
            new UVector2Int(1, 0),
            new UVector2Int(-1, 0),
        };
        private static readonly UVector2Int[] m_blackCrossNot = new UVector2Int[]{
            new UVector2Int(0, -1),
        };
        private static readonly UVector2Int[] m_blackCross = new UVector2Int[]{
            new UVector2Int(0, -1),
            new UVector2Int(1, 0),
            new UVector2Int(-1, 0),
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 0 || point.x > 8 || point.y < 0 || point.y > 9)
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int currpoint, UVector2Int vec)
        {
            UVector2Int point = currpoint + vec;
            if (CheckRange(point) == false)
            {
                return false;
            }

            ChessPieceEntity chessman = Board[point];
            if (chessman != null && chessman.Faction == (int)Faction)
            {
                return false;
            }

            return true;
        }

        private UVector2Int[] FindVecArr(UVector2Int currpoint)
        {
            UVector2Int[] m_vecArr;
            if (Faction == (int)EChessPieceFaction.RED)
            {
                m_vecArr = currpoint.y > 4 ? m_redCross : m_redCrossNot;
            }
            else
            {
                m_vecArr = currpoint.y < 5 ? m_blackCross : m_blackCrossNot;
            }
            return m_vecArr;
        }

        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            UVector2Int[] m_vecArr = FindVecArr(CurrPos);

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                if (CheckPoint(CurrPos, m_vecArr[i]) == true)
                {
                    m_points.Add(CurrPos + m_vecArr[i]);
                }
            }


            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int[] m_vecArr = FindVecArr(CurrPos);

            UVector2Int vec = point - CurrPos;
            for (int i = 0; i < m_vecArr.Length; i++)
            {
                if (m_vecArr[i].Equals(vec))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
