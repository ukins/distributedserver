﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;

namespace ETT.AI.ZhongGuoXiangQi
{
    /// <summary>
    /// 相，象 类
    /// </summary>
    public class Minister : AbstractChessPiece
    {
        public override EChessPieceType Chessmantype => EChessPieceType.Minister;

        private static readonly UVector2Int[] m_vecArr = new UVector2Int[] {
            new UVector2Int(2, 2),
            new UVector2Int(-2, 2),
            new UVector2Int(2, -2),
            new UVector2Int(-2, -2)
        };

        private readonly List<UVector2Int> m_points = new List<UVector2Int>();

        private bool CheckRange(UVector2Int point)
        {
            if (point.x < 0 || point.x > 8)
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.RED && (point.y > 4 || point.y < 0))
            {
                return false;
            }
            if (Faction == (int)EChessPieceFaction.BLACK && (point.y < 5 || point.y > 9))
            {
                return false;
            }
            return true;
        }

        private bool CheckPoint(UVector2Int currpoint, UVector2Int vec)
        {
            UVector2Int point = currpoint + vec;
            if (CheckRange(point) == false)
            {
                return false;
            }

            ChessPieceEntity chessman = null;

            //检查相（象）眼
            UVector2Int eyepoint = currpoint + new UVector2Int(vec.x / 2, vec.y / 2);
            chessman = Board[eyepoint];
            if (chessman != null)
            {
                return false;
            }

            chessman = Board[point];
            if (chessman != null && chessman.Faction == (int)Faction)
            {
                return false;
            }

            return true;
        }

        public override List<UVector2Int> FindPoints()
        {
            m_points.Clear();

            for (int i = 0; i < m_vecArr.Length; ++i)
            {
                if (CheckPoint(CurrPos, m_vecArr[i]) == true)
                {
                    m_points.Add(CurrPos + m_vecArr[i]);
                }
            }

            return m_points;
        }

        public override bool CanKill(UVector2Int point)
        {
            if (CheckRange(point) == false)
            {
                return false;
            }

            UVector2Int vec = point - CurrPos;
            for (int i = 0; i < m_vecArr.Length; i++)
            {
                if (m_vecArr[i].Equals(vec))
                {
                    //检查相（象）眼
                    UVector2Int eyepoint = CurrPos + new UVector2Int(vec.x / 2, vec.y / 2);
                    ChessPieceEntity chessman = Board[eyepoint];
                    if (chessman == null)
                    {
                        return true;
                    }
                }
            }
            return false;
        }
    }
}
