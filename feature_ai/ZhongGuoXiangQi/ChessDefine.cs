﻿namespace ETT.AI.ZhongGuoXiangQi
{
    public enum EChessPieceFaction
    {
        INVALID,
        RED,
        BLACK,
        MAX
    }

    public enum EChessPieceType
    {
        // 无效值
        Invalid = 0,
        // 兵，卒
        Soldier = 1,
        // 炮，砲
        Cannon = 2,
        // 车
        Chariot = 3,
        // 马
        Knight = 4,
        // 象，相
        Minister = 5,
        // 士，仕
        Scholar = 6,
        // 帅，将
        General = 7,
        // 上限值
        MAX = 8,
    }
}
