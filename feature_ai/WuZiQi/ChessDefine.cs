﻿namespace ETT.AI.WuZiQi
{
    public enum EChessPieceFaction
    {
        INVALID,
        RED,
        BLACK,
        MAX
    }
}
