﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Excel = Microsoft.Office.Interop.Excel;


namespace csv2excel
{
    class Program
    {
        private static string m_excel_path = string.Empty;
        private static string m_csv_path = string.Empty;
        static void Main(string[] args)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            m_excel_path = config.AppSettings.Settings["excel_path"].Value;
            m_csv_path = config.AppSettings.Settings["csv_path"].Value;

            if (Directory.Exists(m_excel_path) == false)
            {
                throw new Exception("Excel路径不存在!");
            }

            if (Directory.Exists(m_csv_path) == false)
            {
                throw new Exception("Csv路径不存在!");
            }

            Excel.Application excelApp = new Excel.Application();
            if (excelApp == null)
            {
                Console.WriteLine("Excel is not properly installed!");
                return;
            }

            List<string> csv_list = new List<string>();

            FileMgr.GetAllFiles(m_csv_path, ref csv_list, "*.csv");

            foreach (string csv_path in csv_list)
            {
                string csv_name = csv_path.Replace("/", "\\").Replace(m_csv_path, "");
                csv_name = csv_name.TrimStart('\\', '/');

                string excel_path = Path.Combine(m_excel_path, csv_name);
                excel_path = excel_path.Replace(".csv", ".xlsx");
                // open a workbook,if not exist, create a new one
                Excel.Workbook workBook;
                if (File.Exists(excel_path))
                {
                    File.Delete(excel_path);
                }
                workBook = excelApp.Workbooks.Add(true);


                try
                {
                    string data = File.ReadAllText(csv_path);
                    ParseCsv(data, workBook);
                    Console.WriteLine(string.Format("转excel成功：{0}", csv_name));
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("转csv失败：{0}", csv_name));
                    Console.WriteLine(e.StackTrace);
                }
                finally
                {
                    workBook.SaveAs(excel_path);
                    workBook.Close();
                }

            }

            ////set visible the Excel will run in background
            //excelApp.Visible = false;
            ////set false the alerts will not display
            //excelApp.DisplayAlerts = false;
            excelApp.Quit();
            excelApp = null;
            GC.Collect();

            Console.WriteLine("Finish!!!");
            Console.ReadLine();
        }

        static void ParseCsv(string data, Excel.Workbook workBook)
        {
            //new a worksheet
            Excel.Worksheet workSheet = workBook.ActiveSheet as Excel.Worksheet;

            //write data
            workSheet = (Excel.Worksheet)workBook.Worksheets.get_Item(1);//获得第i个sheet，准备写入

            StringReader sr = new StringReader(data);

            int rowCount = 0;
            string line = sr.ReadLine();
            while (line != null)
            {
                rowCount++;

                string[] cell_arr = line.Split(',');
                for (int i = 0; i < cell_arr.Length; ++i)
                {
                    workSheet.Cells[rowCount, i + 1] = cell_arr[i];
                }

                line = sr.ReadLine();
            }

            workSheet = null;
            workBook = null;
        }

        public static void WriteFile(string filePath, string content)
        {
            UTF8Encoding utf8WithBom = new UTF8Encoding(true);
            StreamWriter sw = new StreamWriter(filePath, false, utf8WithBom);
            sw.Write(content);
            sw.Dispose();
        }
    }
}
