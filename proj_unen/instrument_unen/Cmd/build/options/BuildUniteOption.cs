﻿using UKon.Cmd;
using System;
using System.Collections.Generic;
using System.IO;
using UNen.DT;
using UNen.Table;

namespace UNen.Cmd
{
    /// <summary>
    /// 指令格式 
    /// build --unite
    /// 将所有表格数据打成一个二进制文件
    /// </summary>
    class BuildUniteOption : AbstractCmdOption
    {
        public override string type => "unite";
        private readonly string[] m_name = new string[] { "--unite", "-u" };
        public override string[] names => m_name;

        private static string TargetDir = string.Empty;
        private static string TargetFile = string.Empty;

        public BuildUniteOption(BuildCommand cmd)
            : base(cmd)
        {
            //string path = ConfigMgr.Instance.baseConfig.AppPath;
            TargetDir = ConfigMgr.sysconf.BuildUnitePath;
            TargetFile = System.IO.Path.Combine(TargetDir, "table.bytes");

        }

        public override bool Process(string[] args)
        {
            List<DITableVO> tablevos = ConfigMgr.tableConf.FindAll();

            if (tablevos.Count == 0)
            {
                DebugMgr.LogError("Fail. 找不到匹配的表格");
                return true;
            }

            CmdUtils.ClearDir(TargetDir);

            FileInfo f = new FileInfo(TargetFile);
            BinaryWriter bw = new BinaryWriter(f.OpenWrite());
            bool result = true;
            try
            {
                foreach (var vo in tablevos)
                {
                    result = BuildUtils.BuildFileUnite(vo, bw);
                    if (result == false)
                    {
                        DebugMgr.LogError(string.Format("{0} build fail.", vo.Name));
                        break;
                    }
                    else
                    {
                        DebugMgr.Log(string.Format("{0} build succ.", vo.Name));
                    }
                }
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                DebugMgr.LogError("Fail. 文件生成失败");

                return false;
            }
            finally
            {
                bw.BaseStream.Close();
                bw.Close();
            }
            if (result == true)
            {
                DebugMgr.Log("Succ. 文件生成成功");
            }
            else
            {
                DebugMgr.Log("Succ. 文件生成成功");
            }
            DebugMgr.WriteLine();
            return result;
        }
    }
}
