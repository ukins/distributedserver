﻿using UKon.Cmd;

namespace UNen.Cmd
{
    public class PrintSysConfOption : AbstractCmdOption
    {
        public override string type => "sysconf";
        private readonly string[] m_name = new string[] { "--sysconf", "-s" };
        public override string[] names => m_name;

        public PrintSysConfOption(PrintCommand cmd)
           : base(cmd)
        {

        }

        public override bool Process(string[] args)
        {
            DebugMgr.Log(ConfigMgr.sysconf.ToString());
            DebugMgr.WriteLine();
            return true;
        }
    }
}
