﻿using UKon.Cmd;

namespace UNen.Cmd
{
    public class PrintCommand : AbstractCommand
    {
        public static readonly string CmdName = "print";
        public override string Id => CmdName;

        public PrintCommand()
        {
            m_options.Clear();
            AddCmdOption(new PrintHelpOption(this));
            AddCmdOption(new PrintSysConfOption(this));
            AddCmdOption(new PrintDataTableOption(this));
            AddCmdOption(new PrintSliceDataTableOption(this));
        }
    }
}
