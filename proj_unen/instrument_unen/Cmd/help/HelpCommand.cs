﻿using UKon.Cmd;

namespace UNen.Cmd
{
    public class HelpCommand : AbstractCommand
    {
        public static readonly string CmdName = "help";
        public override string Id => CmdName;

        public HelpCommand()
        {
            m_options.Clear();
            AddCmdOption(new HelpDefaultOption(this));
        }

    }
}
