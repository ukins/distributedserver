﻿using ETT.Table;
using System;
using System.Collections.Generic;
using System.IO;
using UNen.DT;
using UNen.TableGeneric;

namespace UNen.Cmd
{
    class GeneTableStructUtils
    {
        private static bool CheckCollection(List<SICustomStructFieldVO> fields)
        {
            foreach (var field in fields)
            {
                if (field.Type.FieldType == EFieldType.List ||
                    field.Type.FieldType == EFieldType.Dict)
                {
                    return true;
                }
            }
            return false;
        }


        private static void WriteTryBegin(StreamWriter sw)
        {
            sw.WriteLine("\t\t\ttry");
            sw.WriteLine("\t\t\t{");
        }

        private static void WriteTryEnd(StreamWriter sw, string retstr)
        {
            sw.WriteLine("\t\t\t}");
            sw.WriteLine("\t\t\tcatch (Exception e)");
            sw.WriteLine("\t\t\t{");
            sw.WriteLine("\t\t\t\tLog.Error(e);");
            sw.WriteLine("\t\t\t\treturn {0};", retstr);
            sw.WriteLine("\t\t\t}");
        }

        private static void WriteReferenceNamespace(StreamWriter sw, TableStructureVO vo)
        {
            var fields = vo.FindAll();
            Action<string> CheckEnumExtern = delegate (string typename)
            {
                if (string.IsNullOrWhiteSpace(typename) == false)
                {
                    var enumvo = ConfigMgr.tableGenericConf.FindEnum(typename);
                    if (enumvo != null && string.IsNullOrWhiteSpace(enumvo.Extern) == false)
                    {
                        sw.WriteLine("using {0};", enumvo.Extern);
                    }
                }
            };
            foreach (var field in fields)
            {
                CheckEnumExtern(field.Type.FirstElementType);
                CheckEnumExtern(field.Type.SecondElementType);
            }
        }

        private static void WriteTableStructVO(StreamWriter sw, TableStructureVO vo)
        {
            var fields = vo.FindAll();
            bool iscontainlist = CheckCollection(fields);

            sw.WriteLine("using ETT.Model;");
            sw.WriteLine("using ETT.Table;");
            sw.WriteLine("using System;");
            sw.WriteLine("using System.IO;");
            //sw.WriteLine("using UKon.Config;");
            //sw.WriteLine("using UKon.Log;");

            WriteReferenceNamespace(sw, vo);

            if (iscontainlist == true)
            {
                sw.WriteLine("using System.Collections.Generic;");
            }
            sw.WriteLine();
            sw.WriteLine("namespace {0}.Table", SysDefine.RootNamespace);
            sw.WriteLine("{");
            sw.WriteLine("\tpublic class {0} : AbstractTableStruct", vo.Name);
            sw.WriteLine("\t{");


            foreach (var field in fields)
            {
                var fieldType = field.Type.FieldType;
                var name_lower = field.Name.ToLower();
                var firstelementtype = field.Type.FirstElementType;
                switch (fieldType)
                {
                    case EFieldType.Single:
                        //sw.WriteLine("\t\tprivate {0} m_{1};", firstelementtype, name_lower);
                        //sw.WriteLine("\t\tpublic {0} {1};", firstelementtype, field.Name);
                        sw.WriteLine("\t\tpublic {0} {1} {{ get; private set; }}", DTDefine.GetTypeByString(firstelementtype), field.Name);
                        if (string.IsNullOrEmpty(field.Foreigntable) == false)
                        {
                            sw.WriteLine();
                            sw.WriteLine("\t\tprivate {0}Conf m_{1}_conf;", field.Foreigntable, name_lower);
                            sw.WriteLine("\t\tpublic {0}Conf {1}Conf", field.Foreigntable, field.Name);
                            sw.WriteLine("\t\t{");
                            sw.WriteLine("\t\t\tget");
                            sw.WriteLine("\t\t\t{");
                            sw.WriteLine("\t\t\t\tif (m_{0}_conf == null)", name_lower);
                            sw.WriteLine("\t\t\t\t{");
                            sw.WriteLine("\t\t\t\t\tm_{0}_conf = Context.Table.GetComponent<{1}>()[{2}];", name_lower, field.Foreigntable, field.Name);
                            sw.WriteLine("\t\t\t\t}");
                            sw.WriteLine("\t\t\t\treturn m_{0}_conf;", name_lower);
                            sw.WriteLine("\t\t\t}");
                            sw.WriteLine("\t\t}");
                        }
                        break;
                    case EFieldType.List:
                        sw.WriteLine("\t\tprivate List<{0}> m_{1};", firstelementtype, name_lower);
                        sw.WriteLine("\t\tpublic List<{0}> {1} {{ get {{ return m_{2}; }} }}", field.Type.FirstElementType, field.Name, name_lower);
                        break;
                    case EFieldType.Array:
                        sw.WriteLine("\t\tprivate {0}[] m_{1};", firstelementtype, name_lower);
                        sw.WriteLine("\t\tpublic {0}[] {1} {{ get {{ return m_{2}; }} }}", field.Type.FirstElementType, field.Name, name_lower);
                        break;
                    case EFieldType.Dict:
                        sw.WriteLine("\t\tpublic Dictionary<{0},{1}> {2};",
                            field.Type.FirstElementType, field.Type.SecondElementType, field.Name);
                        break;
                }
                sw.WriteLine();
            }

            #region Deserialize Func
            sw.WriteLine("\t\tpublic bool Deserialize(BinaryReader br)");
            sw.WriteLine("\t\t{");
            WriteTryBegin(sw);

            foreach (var field in fields)
            {
                var fieldType = field.Type.FieldType;
                var name_lower = field.Name.ToLower();
                var firstelementtype = field.Type.FirstElementType;
                var enumvo = ConfigMgr.tableGenericConf.FindEnum(field.Type.FirstElementType);
                var structvo = ConfigMgr.tableGenericConf.FindStruct(field.Type.FirstElementType);

                switch (fieldType)
                {
                    case EFieldType.Single:
                        {
                            if (enumvo != null)
                            {
                                sw.WriteLine("\t\t\t\tm_{0} = ({1})Enum.Parse(typeof({1}), {2});", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("string"));

                                //sw.WriteLine("\t\t\t\tm_{0} = ({1}){2};", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("int"));
                            }
                            else if (structvo != null)
                            {
                                sw.WriteLine("\t\t\t\tm_{0} = new {1}();", name_lower, structvo.Name);
                                sw.WriteLine("\t\t\t\tm_{0}.Deserialize(br);\n", name_lower);
                            }
                            else
                            {
                                sw.WriteLine("\t\t\t\t{0} = {1};", field.Name, SupportFieldType.GetBinaryReaderMethodName(firstelementtype));
                                if (string.IsNullOrEmpty(field.Foreigntable) == false)
                                {
                                    sw.WriteLine("\t\t\t\tm_{0}_conf = null;", name_lower);
                                    sw.WriteLine();
                                }
                            }
                        }
                        break;
                    case EFieldType.List:
                        {
                            sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", field.Name);
                            sw.WriteLine("\t\t\t\tm_{0} = new List<{1}>();", field.Name, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", field.Name);
                            sw.WriteLine("\t\t\t\t{");
                            if (enumvo != null)
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}.Add(({1}){2});", field.Name, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("int"));
                            }
                            else if (structvo != null)
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}.Add(new {1}());", field.Name, DTDefine.GetTypeByString(firstelementtype));
                                sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", field.Name);
                            }
                            else
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}.Add({1});", field.Name, SupportFieldType.GetBinaryReaderMethodName(firstelementtype));
                            }
                            sw.WriteLine("\t\t\t\t}");

                        }
                        break;
                    case EFieldType.Array:
                        {
                            sw.WriteLine("\t\t\t\tint m_{0}_cnt = br.ReadInt32();", name_lower);
                            sw.WriteLine("\t\t\t\tm_{0} = new {1}[m_{0}_cnt];", name_lower, DTDefine.GetTypeByString(firstelementtype));
                            sw.WriteLine("\t\t\t\tfor (int i = 0; i < m_{0}_cnt; ++i)", name_lower);
                            sw.WriteLine("\t\t\t\t{");
                            if (enumvo != null)
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}[i] = ({1}){2};", name_lower, enumvo.Name, SupportFieldType.GetBinaryReaderMethodName("int"));
                            }
                            else if (structvo != null)
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}[i] = new {1}();", name_lower, DTDefine.GetTypeByString(firstelementtype));
                                sw.WriteLine("\t\t\t\t\tm_{0}[i].Deserialize(br);", name_lower);
                            }
                            else
                            {
                                sw.WriteLine("\t\t\t\t\tm_{0}[i] = {1};", name_lower, SupportFieldType.GetBinaryReaderMethodName(firstelementtype));
                            }
                            sw.WriteLine("\t\t\t\t}");
                        }
                        break;
                    case EFieldType.Dict:
                        //sw.WriteLine($"\t\tpublic Dictionary<{0},{1}> {2};",
                        //    field.Type.FirstElementType, field.Type.SecondElementType, field.Name));
                        break;
                }
            }

            WriteTryEnd(sw, "false");

            sw.WriteLine("\t\t\treturn true;");
            sw.WriteLine("\t\t}");
            sw.WriteLine();
            #endregion


            #region CreateFromBinary
            sw.WriteLine("\t\tpublic static {0} CreateFromBinary(BinaryReader br)", vo.Name);
            sw.WriteLine("\t\t{");
            sw.WriteLine("\t\t\tvar obj = new {0}();", vo.Name);
            sw.WriteLine("\t\t\tobj.Deserialize(br);");
            sw.WriteLine("\t\t\treturn obj;");
            sw.WriteLine("\t\t}");
            #endregion

            sw.WriteLine("\t}");
            sw.WriteLine("}");
        }
        public static bool GeneTableStruct(TableStructureVO vo, string path)
        {
            if (vo.Extern == true)
            {
                return true;
            }
            string srcpath = System.IO.Path.Combine(path, vo.Name);
            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            string filepath = System.IO.Path.Combine(info.DirectoryName, $"{vo.Name}.cs");
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(filepath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteTableStructVO(sw, vo);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }

        private static void WriteTableEnumVO(StreamWriter sw)
        {
            //var fields = vo.FindAll();

            var enums = ConfigMgr.tableGenericConf.FindAllEnum();

            sw.WriteLine("using System;");
            //sw.WriteLine("using System.Collections.Generic;");
            sw.WriteLine();
            sw.WriteLine("namespace {0}.Table", SysDefine.RootNamespace);
            sw.WriteLine("{");

            foreach (var vo in enums)
            {
                if (vo.Extern != string.Empty)
                {
                    continue;
                }

                var fields = vo.FindAll();
                sw.WriteLine($"\tpublic enum {vo.Name}");
                sw.WriteLine("\t{");
                for (int i = 0; i < fields.Count; ++i)
                {
                    var field = fields[i];
                    sw.WriteLine($"\t\t// {field.Desc}");
                    sw.WriteLine($"\t\t{field.Name} = {i},");
                }
                sw.WriteLine("\t}");
                sw.WriteLine();
            }

            sw.WriteLine("}");
        }
        public static bool GeneTableEnum(string path)
        {
            string srcpath = System.IO.Path.Combine(path, "TableEnum.cs");

            FileInfo info = new FileInfo(srcpath);

            if (Directory.Exists(info.DirectoryName) == false)
            {
                Directory.CreateDirectory(info.DirectoryName);
            }
            //string filepath = System.IO.Path.Combine(info.DirectoryName, $"{0}VO.cs", vo.Name));
            FileStream fs1 = null;
            StreamWriter sw = null;
            bool result = true;
            try
            {
                fs1 = new FileStream(srcpath, FileMode.Create, FileAccess.Write);//创建写入文件 
                sw = new StreamWriter(fs1);
                WriteTableEnumVO(sw);
            }
            catch (Exception e)
            {
                DebugMgr.LogWarning(e.Message);
                result = false;
            }
            finally
            {
                if (sw != null)
                {
                    sw.Close();
                }
                if (fs1 != null)
                {
                    fs1.Close();
                }
            }
            return result;
        }
    }
}
