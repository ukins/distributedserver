﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UNen.Table;

namespace UNen.Cmd
{
    public class GeneTableFormatUtils
    {  /// <summary>
       /// 生成表格格式文件
       /// </summary>
       /// <param name="vo"></param>
       /// <param name="path"></param>
       /// <returns></returns>
        public static bool GeneFileFormat(TableVO vo, string path)
        {
            bool flag = false;
            try
            {
                StringBuilder sb = new StringBuilder();
                //sb.AppendLine(header.Trim(',')); //csv头
                var fieldvos = vo.FindAllLeaves();
                sb.Append("#,");
                for (int i = 0; i < fieldvos.Count; ++i)
                {
                    sb.Append($"{fieldvos[i].Desc},");
                }
                sb.AppendLine();

                sb.Append("&&,");
                for (int i = 0; i < fieldvos.Count; ++i)
                {
                    sb.Append($"{fieldvos[i].FullName},");
                }
                sb.AppendLine();

                string content = sb.ToString();

                List<DiscreteVO> dvolst = ConfigMgr.tableConf.FindDiscreteVOByTableName(vo.Name);

                foreach (var dvo in dvolst)
                {
                    string fullName = Path.Combine(path, dvo.Path);
                    fullName = fullName.Replace('/', '\\');
                    FileInfo file = new FileInfo(fullName);
                    if (Directory.Exists(file.DirectoryName) == false)
                    {
                        Directory.CreateDirectory(file.DirectoryName);
                    }
                    if (File.Exists(fullName) == false)
                    {
                        using (FileStream fs = new FileStream(fullName, FileMode.CreateNew, FileAccess.Write))
                        {
                            StreamWriter sw = new StreamWriter(fs, Encoding.UTF8);
                            sw.Flush();
                            sw.Write(content);
                            sw.Flush();
                            sw.Close();
                        }
                    }
                    else if (FileUtils.CheckFileIsOpen(fullName) == true)
                    {
                        DebugMgr.LogError("Error!  文件被其他进程占用。请先关闭其他进程");
                        DebugMgr.LogError($"\ttablename={vo.Name},path={fullName}");
                        DebugMgr.WriteLine();
                        return false;
                    }
                    else
                    {
                        Encoding encoding = FileUtils.GetType(fullName);
                        FileStream fs = new FileStream(fullName, FileMode.Open, FileAccess.ReadWrite);

                        string[] fields = null;
                        string[] descfields = null;
                        string[] namefields = null;

                        List<string> allstrlist = new List<string>();

                        using (StreamReader sr = new StreamReader(fs, encoding))
                        {
                            string strLine = "";
                            while ((strLine = sr.ReadLine()) != null)
                            {
                                allstrlist.Add(strLine);
                                fields = strLine.Split(',');
                                if (fields[0].StartsWith("#") && descfields == null)
                                {
                                    descfields = fields;
                                }
                                if (fields[0] == "&&")
                                {
                                    namefields = fields;
                                }
                            }
                            sr.Close();
                        }
                        fs.Close();

                        List<LeafFieldVO> newfields = new List<LeafFieldVO>();
                        for (int i = 0; i < fieldvos.Count; ++i)
                        {
                            bool isexist = false;
                            for (int j = 0; j < namefields.Length; ++j)
                            {
                                if (namefields[j] == fieldvos[i].FullName)
                                {
                                    isexist = true;
                                    break;
                                }
                            }
                            if (isexist == false)
                            {
                                newfields.Add(fieldvos[i]);
                            }
                        }
                        using (StreamWriter sw = new StreamWriter(fullName, false, encoding))
                        {
                            string desc = "#";
                            string name = "&&";
                            for (int i = 1; i < descfields.Length; ++i)
                            {
                                desc = $"{desc},{descfields[i]}";
                                name = $"{name},{namefields[i]}";
                            }
                            if (newfields.Count > 0)
                            {
                                for (int i = 0; i < newfields.Count; ++i)
                                {
                                    desc = $"{desc},{newfields[i].Desc}";
                                    name = $"{name},{newfields[i].FullName}";
                                }
                            }
                            allstrlist[0] = desc;
                            allstrlist[1] = name;

                            for (int i = 0; i < allstrlist.Count; ++i)
                            {
                                sw.WriteLine(allstrlist[i]);
                            }
                            sw.Close();

                            if (newfields.Count > 0)
                            {
                                string strnewfields = newfields[0].FullName;
                                for (int i = 1; i < newfields.Count; ++i)
                                {
                                    strnewfields = $"{strnewfields},{newfields[i].FullName}";
                                }
                                DebugMgr.Log("Refresh!  配置文件新增字段。");
                                DebugMgr.Log($"\tfilepath = {fullName}");
                                DebugMgr.Log("\tnew field list:");
                                DebugMgr.Log($"\t{strnewfields}");
                                DebugMgr.WriteLine();
                            }
                        }


                    }
                }
                flag = true;
            }
            catch
            {
                flag = false;
            }
            return flag;
        }
    }
}
