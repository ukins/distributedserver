﻿using System.Collections.Generic;
using UKon.Cmd;
using UNen.DT;

namespace UNen.Cmd
{
    /// <summary>
    /// 生成表结构
    /// gene --format
    /// </summary>
    class GeneFormatOption : AbstractCmdOption
    {

        public override string type => "table_format";
        private readonly string[] m_name = new string[] { "--table_format", "-tf" };
        public override string[] names => m_name;

        private readonly string TargetDir = string.Empty;


        public GeneFormatOption(GeneCommand cmd)
            : base(cmd)
        {
            TargetDir = ConfigMgr.sysconf.GeneFormatPath;
        }

        public override bool Process(string[] args)
        {
            List<DITableVO> tablevos = ConfigMgr.tableConf.FindAll();

            if (tablevos.Count == 0)
            {
                DebugMgr.LogWarning("Waning. 找不到匹配的表格");
                return true;
            }

            //CmdUtils.ClearDir(TargetDir);

            bool issucc = true;
            foreach (var vo in tablevos)
            {
                bool result = GeneTableFormatUtils.GeneFileFormat(vo, TargetDir);
                if (result == false)
                {
                    issucc = false;
                    DebugMgr.LogWarning(string.Format("Fail. 表结构生成失败! tablename = {0}", vo.Name));
                }
            }
            if (issucc == true)
            {
                DebugMgr.Log("Succ. 表结构生成成功! ");
                DebugMgr.WriteLine();
            }
            return true;
        }
    }
}
