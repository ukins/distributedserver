﻿using System.Collections;
using System.Collections.Generic;

namespace Ceshi
{
    public class RangeVO
    {
        public object Min => InnerMin;
        protected virtual object InnerMin => Min;

        public object Max => InnerMax;
        protected virtual object InnerMax => Max;
    }


    public class RangeVO<T> : RangeVO
    {
        private T m_min;
        protected override object InnerMin => Min;
        public new T Min => m_min;


        private T m_max;
        protected override object InnerMax => Max;
        public new T Max => m_max;

        public RangeVO(T min, T max)
        {
            m_min = min;
            m_max = max;
        }

    }

    public class CeshiList
    {
        private List<uint> m_list = new List<uint>()
        {
            1,2,3,4
        };

        public IEnumerable GetList()
        {
            return m_list;
        }
    }
}
