﻿using ETT.Table.Analysis;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtEnumVO : AbstractDataVO, IStructDecorator
    {
        private List<ProtEnumFieldVO> m_fields = new List<ProtEnumFieldVO>();
        public List<ProtEnumFieldVO> FindAll() => m_fields;

        public override void Parse(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                ProtEnumFieldVO vo = new ProtEnumFieldVO(Name);
                vo.Parse(fields[i]);
                m_fields.Add(vo);
            }
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError("Error! 自定义协议枚举类型名称不能为空");
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                throw new System.Exception($"Error。字段名称不规范; fieldname = {Name}");
            }

            int cnt = ConfigMgr.protoConf.GetEnumCnt(Name);
            if (cnt > 1)
            {
                throw new System.Exception($"Error。枚举结构重名; protoenumname = {Name}");
            }

            HashSet<string> m_set = new HashSet<string>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_set.Contains(m_fields[i].Name) == true)
                {
                    DebugMgr.LogError($"Error。枚举字段重名; protoenumname = {Name}, field name = {m_fields[i].Name}");
                    return false;
                }
                m_set.Add(m_fields[i].Name);
            }

            return true;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append($"name=\'{Name}\' ");
            }
            sb.AppendLine(">");

            for (int i = 0; i < m_fields.Count; i++)
            {
                sb.Append(m_fields[i].ToXMLString(prefix + "\t"));
                sb.AppendLine();
            }
            sb.Append(prefix);
            sb.Append("</field>");
            return sb.ToString();
        }

    }
}
