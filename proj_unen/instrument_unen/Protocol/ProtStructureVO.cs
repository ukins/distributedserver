﻿using ETT.Table.Analysis;
using System.Collections.Generic;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtStructureVO : AbstractDataVO, IStructDecorator
    {
        private List<ProtFieldVO> m_fields = new List<ProtFieldVO>();
        public List<ProtFieldVO> FindAll() => m_fields;

        public bool IsPartial { get; protected set; }

        public override void Parse(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
            IsPartial = ExtractBool(node, "ispartial");

            XmlNodeList fields = node.SelectNodes("field");
            for (int i = 0; i < fields.Count; ++i)
            {
                var field = fields[i];
                ProtFieldVO vo = new ProtFieldVO(Name);
                vo.Parse(fields[i]);
                m_fields.Add(vo);
            }
        }
        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError("Error! 自定义协议类型名称不能为空");
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", Name));
                return false;
            }

            HashSet<string> m_set = new HashSet<string>();
            for (int i = 0; i < m_fields.Count; i++)
            {
                if (m_set.Contains(m_fields[i].Name) == true)
                {
                    DebugMgr.LogError($"Error。枚举字段重名; protoenumname = {Name}, field name = {m_fields[i].Name}");
                    return false;
                }
                m_set.Add(m_fields[i].Name);
            }

            int cnt = ConfigMgr.protoConf.GetStructCnt(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError($"Error。协议结构重名; protostructname = {Name}");
                return false;
            }

            bool result = m_fields.TrueForAll(c => c.CheckStruct());
            return result;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            sb.Append("/>");
            return sb.ToString();
        }
    }
}
