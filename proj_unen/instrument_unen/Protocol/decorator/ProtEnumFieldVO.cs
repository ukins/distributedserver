﻿using ETT.Table.Analysis;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtEnumFieldVO : AbstractDataVO, IStructDecorator
    {
        private string m_ownername;
        public ProtEnumFieldVO(string owername)
        {
            m_ownername = owername;
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError(string.Format("Error! 字段名称不能为空，fieldownername={0}", m_ownername));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。字段名称不规范; fieldname = {0}", Name));
                return false;
            }

            return true;
        }

        public override void Parse(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            sb.Append("/>");
            return sb.ToString();
        }
    }
}
