﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Text;
using System.Xml;

namespace UNen.Protocol
{
    public class ProtFieldVO : AbstractDataVO, IStructDecorator
    {
        /// <summary>
        /// 字段类型
        /// </summary>
        protected string m_type = string.Empty;
        protected TypeParser m_typeparser;
        public TypeParser Type { get { return m_typeparser; } }

        public string Desc { get; protected set; }

        private string m_ownername;
        public ProtFieldVO(string owername)
        {
            m_ownername = owername;
        }

        protected void ParseField(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
            m_type = ExtractString(node, "type");
            Desc = ExtractString(node, "desc");

            m_typeparser = new TypeParser(m_type);
        }

        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name))
            {
                DebugMgr.LogError(string.Format("Error! field结构name属性不能为空，field name={0}", FullName));
                return false;
            }

            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。field结构name属性命名不规范; field name = {0}", FullName));
                return false;
            }

            if (string.IsNullOrWhiteSpace(m_type))
            {
                DebugMgr.LogError(string.Format("Error! field结构type属性类型不能为空，field name={0}", FullName));
                return false;
            }
            return true;
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            if (m_type != string.Empty)
            {
                sb.Append(string.Format("type=\'{0}\' ", m_type));
            }
            if (Desc != string.Empty)
            {
                sb.Append(string.Format("desc=\'{0}\' ", Desc));
            }
            sb.Append("/>");
            return sb.ToString();
        }

    }
}
