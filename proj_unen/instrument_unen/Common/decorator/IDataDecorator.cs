﻿namespace UNen
{
    public interface IDataDecorator :IStructDecorator
    {
        bool CheckInternalData();

        bool CheckExternalData();
    }
}
