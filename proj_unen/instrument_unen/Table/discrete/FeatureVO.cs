﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace UNen.Table
{
    public class FeatureVO : ITableVerify
    {
        public string Name { get; private set; }

        private List<ModuleVO> m_modules = new List<ModuleVO>();

        public int CountModule(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public List<DiscreteVO> FindDiscreteVOsByName(string tablename)
        {
            List<DiscreteVO> lst = new List<DiscreteVO>();
            foreach (var module in m_modules)
            {
                lst.AddRange(module.FindDiscreteVOsByName(tablename));
            }
            return lst;
        }
        public DiscreteVO FindDiscreteVOByTablePath(string path)
        {
            foreach (var module in m_modules)
            {
                var lst = module.FindAllDiscreteVO();
                foreach (var vo in lst)
                {
                    if (vo.Path == path)
                    {
                        return vo;
                    }
                }
            }
            return null;
        }

        public List<DiscreteVO> FindAllDiscreteVO()
        {
            List<DiscreteVO> lst = new List<DiscreteVO>();
            foreach (var module in m_modules)
            {
                lst.AddRange(module.FindAllDiscreteVO());
            }
            return lst;
        }

        public bool Analyze(XmlNode node)
        {
            Name = AttrUtils.ExtractString(node, "name");
            if (string.IsNullOrWhiteSpace(Name) == true)
            {
                DebugMgr.LogWarning(string.Format("表路径结构解析失败! feature.name = {0}", Name));
                return false;
            }

            bool result = true;
            m_modules.Clear();

            XmlNodeList list = node.SelectNodes("module");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new ModuleVO(this);
                result = vo.Analyze(list[i]);
                if (result == false)
                {
                    DebugMgr.LogError("Fail! table module 解析失败");
                    return false;
                }
                m_modules.Add(vo);
            }

            return true;
        }
        public bool VerifyStruct()
        {
            int cnt = ConfigMgr.tableConf.CountFeature(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Fail! feature名称重复 feature.name = {0}", Name));
                return false;
            }

            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].VerifyStruct() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifySelfData()
        {
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].VerifySelfData() == false)
                {
                    return false;
                }
            }
            return true;
        }

        public bool VerifyLinkData()
        {
            for (int i = 0; i < m_modules.Count; ++i)
            {
                if (m_modules[i].VerifyLinkData() == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
