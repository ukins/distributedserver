﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Text;

namespace UNen.DT
{
    public class FieldTypeInspector
    {
        //private FieldVO m_fieldvo;
        //public FieldTypeInspector(FieldVO fieldvo)
        //{
        //    m_fieldvo = fieldvo;
        //}

        private static void ShowLogError1(LeafFieldVO m_fieldvo)
        {
            if (m_fieldvo.Root != null)
            {
                DebugMgr.LogError($"Error！ 字段类型错误。tablename={m_fieldvo.Root.Name}, name = {m_fieldvo.FullName}, type = {m_fieldvo.Type.Value}");
            }
            else
            {
                DebugMgr.LogError($"Error！ 字段类型错误。name = {m_fieldvo.FullName}, type = {m_fieldvo.Type.Value}");
            }

            StringBuilder sb = new StringBuilder();
            sb.Append("默认仅支持以下几种类型：");
            for (int i = 0; i < SysDefine.ElementTypeConst.Length; ++i)
            {
                var tp = SysDefine.ElementTypeConst[i];
                if (i != SysDefine.ElementTypeConst.Length - 1)
                {
                    sb.AppendFormat("{0}, ", tp);
                }
                else
                {
                    sb.Append(tp);
                }
            }
            DebugMgr.LogWarning(sb.ToString());
            DebugMgr.LogWarning("若需要自定义类型，请在table_generic.config表中定义");
        }

        public static bool CheckFieldType(LeafFieldVO m_fieldvo)
        {
            var result = true;
            if (m_fieldvo.Type.FieldType == EFieldType.Invalid)
            {
                DebugMgr.LogError($"Error！ 字段类型无效。tablename={m_fieldvo.Root.Name}, fieldname = {m_fieldvo.FullName}");
                return false;
            }
            else if (m_fieldvo.Type.FieldType == EFieldType.Single)
            {
                result = SysDefine.CheckTypeIsDefined(m_fieldvo.Type.Value);
                if (result == false)
                {
                    ShowLogError1(m_fieldvo);
                    return false;
                }
            }
            else if (m_fieldvo.Type.FieldType == EFieldType.Array || m_fieldvo.Type.FieldType == EFieldType.List)
            {
                result = SysDefine.CheckTypeIsDefined(m_fieldvo.Type.FirstElementType);
                if (result == false)
                {
                    ShowLogError1(m_fieldvo);
                    return false;
                }
            }
            else if (m_fieldvo.Type.FieldType == EFieldType.Dict)
            {
                result = SysDefine.CheckTypeIsDefined(m_fieldvo.Type.FirstElementType);
                if (result == false)
                {
                    ShowLogError1(m_fieldvo);
                    return false;
                }
                result = SysDefine.CheckTypeIsDefined(m_fieldvo.Type.SecondElementType);
                if (result == false)
                {
                    ShowLogError1(m_fieldvo);
                    return false;
                }
            }
            return true;
        }
    }
}
