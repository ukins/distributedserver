﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Xml;

namespace UNen.DT
{
    public class SITableVO : TableVO, IStructDecorator
    {
        protected override void ParseChild(XmlNode node)
        {
            ParseChildren<SIFieldVO, SINodeVO>(node);
        }

        protected bool CheckStructFunc(IStructDecorator vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("CheckStructFunc 类型转换失败");
                return false;
            }
            return vo.CheckStruct();
        }

        public bool CheckStruct()
        {
            if (string.IsNullOrWhiteSpace(Name) == true)
            {
                DebugMgr.LogError(string.Format("Error! table结构name属性不能为空，table={0}", Name));
                return false;
            }
            if (AttrUtils.CheckNamingSpecification(Name) == false)
            {
                DebugMgr.LogError(string.Format("Error。table结构name属性命名不规范; tablename = {0}", Name));
                return false;
            }

            var m_leaves = FindLeaves();
            var primarykeys = m_leaves.FindAll(c => c.IsPrimaryKey == true);
            if (primarykeys.Count > 1)
            {
                DebugMgr.LogError(string.Format("Error! 唯一主键重复; tablename = {0}", Name));
                return false;
            }
            var unionkeys = m_leaves.FindAll(c => c.IsUnionKey == true);
            if (unionkeys.Count > 2)
            {
                DebugMgr.LogError(string.Format("Error! 联合主键不能超过两个字段; tablename = {0}", Name));
                return false;
            }

            if (primarykeys.Count > 1 && unionkeys.Count > 1)
            {
                DebugMgr.LogError(string.Format("Error! 唯一主键不能与联合主键同时使用; tablename = {0}", Name));
                return false;
            }
            if (primarykeys.Count == 0 && unionkeys.Count == 0)
            {
                DebugMgr.LogError(string.Format("Error! 配置表不能没有主键或联合主键; tablename = {0}", Name));
                return false;
            }

            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckStructFunc(m_fields[i] as IStructDecorator);
                if (result == false)
                {
                    return false;
                }
            }

            int cnt = ConfigMgr.tableConf.CountVO(Name);
            if (cnt > 1)
            {
                DebugMgr.LogError(string.Format("Error。表格重复; tablename = {0}", Name));
                return false;
            }

            if (IsEnum == true)
            {
                IFieldVO fieldvo = m_fields.Find(c => c.Name.ToLower() == "id");
                if(fieldvo == null)
                {
                    DebugMgr.LogError($"Error!枚举表格不存在id字段");
                    return false;
                }
                fieldvo = m_fields.Find(c => c.Name.ToLower() == "name");
                if (fieldvo == null)
                {
                    DebugMgr.LogError($"Error!枚举表格不存在name字段");
                    return false;
                }
            }

            return true;
        }
    }
}
