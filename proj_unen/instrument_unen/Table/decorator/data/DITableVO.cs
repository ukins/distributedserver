﻿using ETT.Table.Analysis;
using System.Collections.Generic;
using System.Xml;
using UNen.Table;

namespace UNen.DT
{
    public class DITableVO : SITableVO, IDataDecorator 
    {
        private AbstractDataTable dataTable = null;
        public AbstractDataTable GetDataTable()
        {
            return dataTable;
        }

        protected override void ParseChild(XmlNode node)
        {
            ParseChildren<DIFieldVO, DINodeVO>(node);
        }

        protected bool CheckInternalDataFunc(IDataDecorator  vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("DITableVO.CheckInternalDataFunc 类型转换失败");
                return false;
            }
            return vo.CheckInternalData();
        }

        protected bool CheckExternalDataFunc(IDataDecorator  vo)
        {
            if (vo == null)
            {
                DebugMgr.LogError("DITableVO.CheckExternalDataFunc 类型转换失败");
                return false;
            }
            return vo.CheckExternalData();
        }

        public bool CheckInternalData()
        {
            List<DiscreteVO> discretes = ConfigMgr.tableConf.FindDiscreteVOByTableName(Name);
            if (discretes.Count == 0)
            {
                DebugMgr.LogWarning(string.Format("Warning。该表结构未被使用; tablename = {0}", Name));
            }

            //分表数据整合之后再验证所有数据有效性
            dataTable = discretes[0].GetDataTable().Clone();
            for (int i = 1; i < discretes.Count; ++i)
            {
                dataTable.Union(discretes[i].GetDataTable());
            }

            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckInternalDataFunc(m_fields[i] as IDataDecorator);
                if (result == false)
                {
                    return false;
                }
            }

            //联合主键查重
            if (UnionKey.First != null && UnionKey.Second != null)
            {
                Dictionary<string, HashSet<string>> m_unionvalue = new Dictionary<string, HashSet<string>>();
                for (int i = 0; i < dataTable.Rows.Count; ++i)
                {
                    GenericDataRow row = dataTable.Rows[i];
                    string value1 = row[UnionKey.First.Name][0].Value;
                    string value2 = row[UnionKey.Second.Name][0].Value;
                    if (m_unionvalue.ContainsKey(value1) == true)
                    {
                        var secondset = m_unionvalue[value1];
                        if (secondset.Contains(value2) == true)
                        {
                            DebugMgr.LogError(string.Format("Error.联合主键重复 tablename={0},key1={1},key2={2}", Name, value1, value2));
                            return false;
                        }
                        else
                        {
                            secondset.Add(value2);
                        }
                    }
                    else
                    {
                        HashSet<string> secondset = new HashSet<string>();
                        secondset.Add(value2);
                        m_unionvalue.Add(value1, secondset);
                    }
                }
            }
            return true;
        }

        public bool CheckExternalData()
        {
            bool result = true;
            for (int i = 0; i < m_fields.Count; i++)
            {
                result = CheckExternalDataFunc(m_fields[i] as IDataDecorator);
                if (result == false)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
