﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace UNen.Table
{
    interface ITableVerify
    {
        /// <summary>
        /// 验证结构，并对一些关联信息进行设置
        /// </summary>
        /// <param name="mgr"></param>
        /// <returns></returns>
        bool VerifyStruct();

        /// <summary>
        /// 验证数据，验证自身数据的有效性
        /// </summary>
        /// <returns></returns>
        bool VerifySelfData();

        /// <summary>
        /// 验证数据，验证关联数据的有效性
        /// </summary>
        /// <returns></returns>
        bool VerifyLinkData();
    }
}
