﻿namespace UNen
{
    class DebugMgr
    {
        public static void Init()
        {
            ETT.Model.Log.Init(new ClientLogAdapter());
        }

        public static void Log(string log)
        {
            ETT.Model.Log.Debug(log);
        }


        public static void LogWarning(string log)
        {
            ETT.Model.Log.Warning(log);
        }


        public static void LogError(string log)
        {
            ETT.Model.Log.Error(log);
        }

        public static void WriteLine()
        {
            ETT.Model.Log.Debug(string.Empty);
        }

    }
}
