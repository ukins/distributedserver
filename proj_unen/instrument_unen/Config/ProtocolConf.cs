﻿using ETT.Table;
using ETT.Table.Analysis;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using UNen.Protocol;

namespace UNen
{
    public class ProtocolConf
    {
        public static readonly string FILE_NAME = "proto.config";
        private XmlDocument xmldoc = null;

        private List<ProtStructureVO> m_structvos = new List<ProtStructureVO>();
        public List<ProtStructureVO> FindAllStruct() => m_structvos;

        private List<ProtEnumVO> m_enumvos = new List<ProtEnumVO>();
        public List<ProtEnumVO> FindAllEnum() => m_enumvos;

        public int GetStructCnt(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_structvos.Count; i++)
            {
                if (m_structvos[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public int GetEnumCnt(string name)
        {
            int cnt = 0;
            for (int i = 0; i < m_enumvos.Count; i++)
            {
                if (m_enumvos[i].Name == name)
                {
                    cnt++;
                }
            }
            return cnt;
        }

        public HashSet<string> FindAllCustomType()
        {
            HashSet<string> m_set = new HashSet<string>();
            foreach (var vo in m_structvos)
            {
                if (m_set.Contains(vo.Name) == true)
                {
                    continue;
                }
                m_set.Add(vo.Name);
            }
            foreach (var vo in m_structvos)
            {
                var fields = vo.FindAll();
                foreach (var field in fields)
                {
                    bool isSysType = SysDefine.CheckType(field.Type.FirstElementType, SysDefine.ProtoTypeConst);
                    if (isSysType == true)
                    {
                        continue;
                    }
                    if (m_set.Contains(field.Type.FirstElementType) == false)
                    {
                        m_set.Add(field.Type.FirstElementType);
                    }
                    if (m_set.Contains(field.Type.Value) == false)
                    {
                        m_set.Add(field.Type.Value);
                    }
                }
            }

            var protocoltables = ConfigMgr.tableConf.FindDiscreteVOByTableName("ProtocolTable");
            foreach (var vo in protocoltables)
            {
                var tb = vo.GetDataTable();
                for (int i = 0; i < tb.Rows.Count; ++i)
                {
                    GenericDataRow row = tb.Rows[i];
                    //var proto_param_names = row["Parameter.Name"];
                    var proto_param_types = row["Parameter.Type"];
                    //var proto_param_descs = row["Parameter.Desc"];
                    int len = proto_param_types.Count;
                    for (int k = 0; k < len; k++)
                    {
                        TypeParser parser = new TypeParser(proto_param_types[k].Value);
                        bool isSysType = SysDefine.CheckType(parser.FirstElementType, SysDefine.ProtoTypeConst);
                        if (isSysType == true)
                        {
                            continue;
                        }
                        if (m_set.Contains(parser.FirstElementType) == false)
                        {
                            m_set.Add(parser.FirstElementType);
                        }
                        if (m_set.Contains(parser.Value) == false)
                        {
                            m_set.Add(parser.Value);
                        }
                    }
                }
            }

            return m_set;
        }

        //public bool Init()
        //{
        //    bool result = Load();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    result = Analyze();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    return Verify();
        //}

        public bool Load()
        {
            string path = ConfigMgr.sysconf.AppPath;
            string filepath = Path.Combine(path, FILE_NAME);
            xmldoc = new XmlDocument();
            try
            {
                xmldoc.Load(filepath);
            }
            catch (XmlException e)
            {
                DebugMgr.LogWarning(e.Message);
                return false;
            }
            return true;
        }

        public bool Analyze()
        {
            var xmlelement = xmldoc.SelectSingleNode("/config/structures");
            if (xmlelement == null)
            {
                return false;
            }
            m_structvos.Clear();
            m_enumvos.Clear();

            XmlNodeList list = null;

            list = xmlelement.SelectNodes("structure");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new ProtStructureVO();
                vo.Parse(list[i]);
                m_structvos.Add(vo);
            }

            xmlelement = xmldoc.SelectSingleNode("/config/enums");
            if (xmlelement == null)
            {
                return false;
            }
            list = xmlelement.SelectNodes("enum");
            for (int i = 0; i < list.Count; ++i)
            {
                var vo = new ProtEnumVO();
                vo.Parse(list[i]);
                m_enumvos.Add(vo);
            }

            return true;
        }

        public bool Verify()
        {
            for (int i = 0; i < m_structvos.Count; ++i)
            {
                if (m_structvos[i].CheckStruct() == false)
                {
                    return false;
                }
            }

            for (int i = 0; i < m_enumvos.Count; ++i)
            {
                if (m_enumvos[i].CheckStruct() == false)
                {
                    return false;
                }
            }

            DebugMgr.Log("Succ! proto.config 解析成功");
            return true;
        }
    }
}
