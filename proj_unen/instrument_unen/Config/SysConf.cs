﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml;

namespace UNen
{
    public class PathVO
    {
        /// <summary>
        /// 绝对路径
        /// </summary>
        public string path = string.Empty;
        public string xmlnodename = string.Empty;
        public string defaultstr = string.Empty;
        public bool IsMustExist = false;

        public override string ToString()
        {
            return $"{xmlnodename}:\t{path}";
        }
    }

    public class SysConf
    {
        public static readonly string FILE_NAME = "unen.config";
        private XmlDocument xmldoc = null;
        //public static readonly EAppMode Mode;

        //static SysConf()
        //{
        //    Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
        //    string mode = config.AppSettings.Settings["Mode"].Value;
        //    switch (mode.ToLower())
        //    {
        //        case "client":
        //            Mode = EAppMode.Client;
        //            break;
        //        case "server":
        //            Mode = EAppMode.Server;
        //            break;
        //    }
        //}

        private List<PathVO> m_pathvo = new List<PathVO>()
        {
            new PathVO
            {
                xmlnodename =  "src_proj_path",
                IsMustExist = true,
            },
            new PathVO
            {
                xmlnodename =  "build_alone_path",
                defaultstr = "build_alone",
            },
            new PathVO
            {
                xmlnodename =  "build_unite_path",
                defaultstr = "build_unite",
            },
            new PathVO
            {
                xmlnodename =  "gene_format_path",
                defaultstr = "gene_format",
            },
            new PathVO
            {
                xmlnodename =  "gene_code_client_path",
                defaultstr = "gene_code_client",
            },
            new PathVO
            {
                xmlnodename =  "gene_code_server_path",
                defaultstr = "gene_code_server",
            },
            new PathVO
            {
                xmlnodename =  "gene_dir_path",
                defaultstr = "gene_dir",
            },
            new PathVO
            {
                xmlnodename =  "gene_proto_server_path",
                defaultstr = "gene_proto_server",
            }
        };

        public string GetPathByName(string name)
        {
            for (int i = 0; i < m_pathvo.Count; ++i)
            {
                if (string.CompareOrdinal(m_pathvo[i].xmlnodename.ToLower(), name.ToLower()) == 0)
                {
                    return m_pathvo[i].path;
                }
            }
            return string.Empty;
        }

        public string GetPathByIndex(int idx)
        {
            if (idx >= 0 && idx < m_pathvo.Count)
            {
                return m_pathvo[idx].path;
            }
            return string.Empty;
        }

        /// <summary>
        /// 配置表所在目录的路径
        /// 可以是绝对路径，也可以是相对于UNen工具的路径
        /// </summary>
        public string SrcProjPath
        {
            get { return m_pathvo[0].path; }
        }

        public bool CoverSrcProjPath(string path)
        {
            if (Directory.Exists(path) == false)
            {
                var newpath = Path.Combine(AppPath, path);
                if (Directory.Exists(newpath) == false)
                {
                    return false;
                }
                m_pathvo[0].path = newpath;
            }
            else
            {
                m_pathvo[0].path = path;
            }
            return true;
        }

        public string BuildAlonePath
        {
            get { return m_pathvo[1].path; }
        }

        public string BuildUnitePath
        {
            get { return m_pathvo[2].path; }
        }

        public string GeneFormatPath
        {
            get { return m_pathvo[3].path; }
        }

        public string GeneCodeClientPath
        {
            get { return m_pathvo[4].path; }
        }

        public string GeneCodeServerPath
        {
            get { return m_pathvo[5].path; }
        }

        public string GeneDirPath
        {
            get { return m_pathvo[6].path; }
        }

        public string GeneProtoServerPath
        {
            get { return m_pathvo[7].path; }
        }

        public string AppPath
        {
            get { return AppDomain.CurrentDomain.BaseDirectory; }
        }

        //public bool Init()
        //{
        //    bool result = Load();
        //    if (result == false)
        //    {
        //        return false;
        //    }
        //    return Analyze();
        //}

        public bool Load()
        {
            string path = ConfigMgr.sysconf.AppPath;
            string filepath = Path.Combine(path, FILE_NAME);
            xmldoc = new XmlDocument();
            try
            {
                xmldoc.Load(filepath);
            }
            catch (XmlException e)
            {
                DebugMgr.LogWarning(e.Message);
                return false;
            }
            return true;
        }

        public bool Analyze()
        {
            var xmlelement = xmldoc.SelectSingleNode("/config/redirection");
            if (xmlelement == null)
            {
                return false;
            }

            for (int i = 0; i < m_pathvo.Count; ++i)
            {
                bool result = ReadXmlNode(xmlelement, m_pathvo[i]);
                if (result == false)
                {
                    return false;
                }
            }

            return true;
        }

        private bool ReadXmlNode(XmlNode element, PathVO vo)
        {
            XmlNode node = element.SelectSingleNode(vo.xmlnodename);
            if (node != null && node.InnerText.Length > 0)
            {
                vo.path = node.InnerText;
                if (Directory.Exists(vo.path) == false)
                {
                    vo.path = Path.Combine(AppPath, vo.path);
                    if (vo.IsMustExist == true && Directory.Exists(vo.path) == false)
                    {
                        DebugMgr.LogError($"Error. {vo.xmlnodename} Path Invalid; path = {vo.path}");
                        return false;
                    }
                }
            }
            else
            {
                if (vo.IsMustExist == false && vo.defaultstr != string.Empty)
                {
                    vo.path = Path.Combine(AppPath, vo.defaultstr);
                }
                else
                {
                    vo.path = string.Empty;
                    DebugMgr.LogError(string.Format("Error. {0} Path Is Not Exist;", vo.xmlnodename));
                    return false;
                }
            }

            return true;
        }

        public override string ToString()
        {
            return string.Join("\n", m_pathvo);
        }
    }
}
