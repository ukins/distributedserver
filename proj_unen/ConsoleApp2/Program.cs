﻿using Facade.Battle;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;

namespace ConsoleApp2
{
    public enum EPhase
    {
        Start,
        End
    }
    interface ITree
    {
        string Name { get; set; }
        int Age { get; set; }
    }

    [JsonObject(MemberSerialization.OptOut)]
    class Tree : ITree
    {
        public string Name { get; set; }
        public int Age { get; set; }
    }
    [JsonObject(MemberSerialization.OptIn)]
    class Product
    {
        public enum EPoint
        {
            A,
            B,
        }
        public static event Action<Product> OnTriggerEvent;

        [JsonProperty]
        public string Name { get; set; }
        [JsonProperty]
        public string[] Size { get; set; }

        [JsonProperty]
        private int a = 1;
        //public readonly List<ITree> values = new List<ITree>();

        public void SayHello()
        {
            Console.WriteLine($"Hello, {Name}");
        }
    }
    class Program
    {
        public static bool Contain<T>(T[] arr, T v)
        {
            for (int i = 0; i < arr.Length; ++i)
            {
                if (arr[i].Equals(v))
                {
                    return true;
                }
            }

            return false;
        }

        static void Main(string[] args)
        {
            //EPhase Phase = EPhase.Start;
            //foreach (var name in Enum.GetNames(typeof(Object)))
            //{
            //    if (string.CompareOrdinal(name.ToLower(), "end") == 0)
            //    {
            //        Phase = (EPhase)Enum.Parse(typeof(EPhase), name);
            //    }
            //}
            //Console.WriteLine(Phase);

            //int[] arr = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


            //var lst = arr.OrderBy(d => Guid.NewGuid()).Take(3);
            //foreach (var item in lst)
            //{
            //    Console.WriteLine(item);
            //}

            //string input = "card.get(hp,123,tgt) && card.cout_1(123,123,456)";
            //string pattern = @"(?<method>[a-z]+\.[a-z]+(_[a-z0-9])?\([^\(\)]*\))";

            //Regex reg = new Regex(pattern);
            //var matchs = reg.Matches(input);

            //int n = int.Parse(input.Substring(0, input.Length - 1));
            //foreach (var match in matchs)
            //for (int i = 0; i < matchs.Count; ++i)
            //{
            //    var match = matchs[i];
            //    foreach (var item in match.Groups)
            //    {
            //        Console.WriteLine(item);
            //    }
            //}
            //var result = Regex.Replace(input, pattern, "M$1");
            //Console.WriteLine(result);

            //FirstBigger(1);


            //LogicalConstrainer logical = new LogicalConstrainer();
            //string code = "(M||M)&&M()";
            //bool result = logical.Parse(code);
            //Console.WriteLine($"解析{result.ToString()}");

            //HashSet<int> lst = new HashSet<int>();
            //for(int i=0;i<10;++i)
            //{
            //    lst.Add(i);
            //}

            //for(int i=0;i<lst.Count;++i)
            //{
            //    Console.WriteLine(lst.ElementAt(i));
            //}

            //Console.WriteLine(lst.ElementAt(10));

            //Dictionary<int, string> m_dict = new Dictionary<int, string>();
            //m_dict.Add(1, "abc1");
            //m_dict.Add(2, "abc2");
            //m_dict.Add(3, "abc3");
            //m_dict.Add(4, "abc4");
            //m_dict.Add(5, "abc5");

            //var lst = Enumerable.ToList(m_dict.Values);
            //foreach(var o in lst)
            //{
            //    Console.WriteLine(o);
            //}

            //bool result = Boolean.TryParse("atrue".ToLower(), out bool value);
            //Console.WriteLine(value);

            //Product product = new Product()
            //{
            //    Name = "Apple",
            //    Size = new string[] { "Small", "Medium", "Large" }
            //};
            ////product.values.Add(new Tree() { Name = "Tree1", Age = 10 });
            ////product.values.Add(new Tree() { Name = "Tree2", Age = 20 });
            ////product.values.Add(new Tree() { Name = "Tree3", Age = 30 });

            ////string output = JsonConvert.SerializeObject(product,Newtonsoft.Json.Formatting.Indented);
            //string output = JsonConvert.SerializeObject(product);
            //Console.WriteLine(output);

            //Product product2 = JsonConvert.DeserializeObject<Product>(output);
            //Console.WriteLine(product2);
            //Console.WriteLine(product2.Name);
            //product2.SayHello();

            //uint[] arr = new uint[4] { 1, 2, 3, 4 };

            //bool result = Contain<uint>(arr, 5);
            //Console.WriteLine(result);

            //int frameRate = 30;
            //int frameMilliseconds = 1000 / frameRate;

            //Stopwatch stopwatch = new Stopwatch();
            //int overTime = 0;
            //while (true)
            //{
            //    stopwatch.Restart();

            //    //root.Step((frameMilliseconds + overTime) * 0.001f);
            //    Console.WriteLine((frameMilliseconds + overTime) * 0.001f);

            //    stopwatch.Stop();
            //    int stepTime = (int)stopwatch.ElapsedMilliseconds;

            //    if (stepTime <= frameMilliseconds)
            //    {
            //        Thread.Sleep(frameMilliseconds - stepTime);
            //        overTime = 0;
            //    }
            //    else
            //    {
            //        overTime = stepTime - frameMilliseconds;
            //    }
            //}

            //Dictionary<int, Dictionary<int, int>> dict = new Dictionary<int, Dictionary<int, int>>();

            //for (int i = 0; i < 10; ++i)
            //{
            //    dict[i][i] = i;
            //}

            //Console.WriteLine(string.Format("力量{0}", 5, 10));

            long epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc).Ticks;
            Console.WriteLine((DateTime.UtcNow.Ticks - epoch) / 10000);

        }

        static string FirstBigger(string s)
        {
            return s.Substring(0, 1).ToUpper() + s.Substring(1);
        }
    }
}
