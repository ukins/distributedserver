﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace Facade.Battle
{
    /// <summary>
    /// 逻辑约束器
    /// </summary>
    public class LogicalConstrainer
    {
        private readonly Dictionary<string, int> m_keyword_prority;

        public LogicalConstrainer()
        {
            m_keyword_prority = new Dictionary<string, int>();
            m_keyword_prority.Add("&&", 2);
            m_keyword_prority.Add("||", 1);
            m_keyword_prority.Add("(", 0);
            m_keyword_prority.Add(")", 0);
        }

        public bool Parse(string code)
        {
            string pattern = string.Empty;

            pattern = @"\s";
            code = Regex.Replace(code, pattern, "");

            pattern = @"[^\(\)M&\|]";
            var match = Regex.Match(code, pattern);
            if (match.Success == true)
            {
                Console.WriteLine("Error！逻辑表达式无法解析，存在非法字符");
                return false;
            }

            pattern = @"([\(\)M&|])";
            code = Regex.Replace(code, pattern, "$1 ");

            pattern = @"&\s+&";
            code = Regex.Replace(code, pattern, "&&");

            pattern = @"\|\s+\|";
            code = Regex.Replace(code, pattern, "||");

            code = code.Trim();

            List<string> value_list = new List<string>();
            bool result = TransToRPN(code, ref value_list);

            if (result == true)
            {
                result = SimulateCalc(value_list);
            }

            return result;
        }

        /// <summary>
        /// 逆波兰表达式
        /// </summary>
        /// <param name="code"></param>
        /// <param name="value_list"></param>
        /// <returns></returns>
        private bool TransToRPN(string code, ref List<string> value_list)
        {
            string[] str_list = code.Split(' ');
            foreach (var str in str_list)
            {
                if (m_keyword_prority.ContainsKey(str) == false && str != "M")
                {
                    Console.WriteLine($"Error 关键字{str}使用不当");
                    return false;
                }
            }
            Stack<string> operator_stack = new Stack<string>();
            Stack<string> method_stack = new Stack<string>();

            for (int i = 0; i < str_list.Length; ++i)
            {
                var str = str_list[i];
                if (string.CompareOrdinal(str, "M") == 0)
                {
                    method_stack.Push(str);
                    continue;
                }

                if (operator_stack.Count == 0)
                {
                    operator_stack.Push(str);
                    continue;
                }

                if (string.CompareOrdinal(str, "(") == 0)
                {
                    operator_stack.Push(str);
                    continue;
                }

                if (string.CompareOrdinal(str, ")") == 0)
                {
                    while (string.CompareOrdinal(operator_stack.Peek(), "(") != 0)
                    {
                        method_stack.Push(operator_stack.Pop());
                    }
                    operator_stack.Pop();
                    continue;
                }

                string top_key = operator_stack.Peek();
                if (string.CompareOrdinal(operator_stack.Peek(), "(") == 0)
                {
                    operator_stack.Push(str);
                    continue;
                }

                if (m_keyword_prority[str] > m_keyword_prority[top_key])
                {
                    operator_stack.Push(str);
                }
                else
                {
                    do
                    {
                        method_stack.Push(operator_stack.Pop());
                    }
                    while (operator_stack.Count > 0 && m_keyword_prority[operator_stack.Peek()] >= m_keyword_prority[str]);

                    operator_stack.Push(str);
                }
            }

            while (operator_stack.Count > 0)
            {
                method_stack.Push(operator_stack.Pop());
            }

            value_list.Clear();
            value_list.AddRange(method_stack.ToList());
            value_list.Reverse();
            return true;
        }

        private bool SimulateCalc(List<string> str_list)
        {
            Stack<string> calc_stack = new Stack<string>();

            for (int i = 0; i < str_list.Count; ++i)
            {
                var str = str_list[i];
                if (string.CompareOrdinal(str, "M") == 0)
                {
                    calc_stack.Push(str);
                    continue;
                }
                else
                {
                    if (string.CompareOrdinal(str, "&&") == 0)
                    {
                        var m_1 = calc_stack.Pop();
                        var m_2 = calc_stack.Pop();

                        //M && M = M
                        calc_stack.Push(m_1);
                    }
                    else if (string.CompareOrdinal(str, "||") == 0)
                    {

                        var m_1 = calc_stack.Pop();
                        var m_2 = calc_stack.Pop();

                        //M || M = M
                        calc_stack.Push(m_1);
                    }
                }
            }

            if (calc_stack.Count != 1 || string.CompareOrdinal(calc_stack.Peek(), "M") != 0)
            {
                Console.WriteLine($"Error, 逻辑表达式无效，无法进行正常计算");
                return false;
            }

            return true;
        }

        private void ProcessChar(string c)
        {

        }
    }
}
