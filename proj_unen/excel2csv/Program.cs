﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Office.Core;
using System.IO;

using Excel = Microsoft.Office.Interop.Excel;


namespace excel2csv
{
    class Program
    {
        private static string m_excel_path = string.Empty;
        private static string m_csv_path = string.Empty;

        static void Main(string[] args)
        {
            Configuration config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);

            m_excel_path = config.AppSettings.Settings["excel_path"].Value;
            m_csv_path = config.AppSettings.Settings["csv_path"].Value;

            if (Directory.Exists(m_excel_path) == false)
            {
                throw new Exception("Excel路径不存在!");
            }

            if (Directory.Exists(m_csv_path) == false)
            {
                throw new Exception("Csv路径不存在!");
            }

            List<string> xlsList = new List<string>();

            FileMgr.GetAllFiles(m_excel_path, ref xlsList, "*.xlsx");


            Excel.Application excelApp = new Excel.Application();

            for (int i = 0; i < xlsList.Count; ++i)
            {
                string excel_name = xlsList[i].Replace("/", "\\").Replace(m_excel_path, "");
                excel_name = excel_name.TrimStart('\\', '/');

                Excel.Workbook workBook = excelApp.Workbooks.Open(xlsList[i]);

                try
                {
                    Excel.Worksheet worksheet = ((Excel.Sheets)(workBook.Worksheets)).get_Item(1) as Excel.Worksheet;
                    if (worksheet != null)
                    {
                        ParseCsv(worksheet, excel_name);
                        Console.WriteLine(string.Format("转csv成功：{0}", excel_name));
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(string.Format("转csv失败：{0}", excel_name));
                    Console.WriteLine(e.StackTrace);
                }
                finally
                {
                    workBook.Close();
                }
            }

            excelApp.Quit();

            Console.WriteLine("Finish!!!");
            Console.ReadLine();
        }


        static void ParseCsv(Excel._Worksheet worksheet, string filename)
        {
            string csvName = Path.Combine(m_csv_path, filename);
            csvName = csvName.Replace(".xlsx", ".csv");

            FileInfo csvInfo = new FileInfo(csvName);

            if (Directory.Exists(csvInfo.DirectoryName) == false)
            {
                Directory.CreateDirectory(csvInfo.DirectoryName);
            }

            Excel.Range range = worksheet.UsedRange;
            int rowCount = range.Rows.Count;
            int columnCount = range.Columns.Count;
            List<string> rows = new List<string>();
            Excel.Range cell;
            for (int i = 1; i <= rowCount; i++)
            {
                cell = worksheet.Cells[i, 1] as Excel.Range;
                if (Convert.ToString(cell.Value) == "##")
                {
                    continue;
                }
                List<string> rowCells = new List<string>();
                for (int j = 1; j <= columnCount; j++)
                {
                    cell = worksheet.Cells[i, j] as Excel.Range;
                    string celldata = Convert.ToString(cell.Value);
                    char[] escapeChars = { ',', '"' };
                    string cellStr = celldata;
                    if (celldata != null && (celldata.IndexOfAny(escapeChars) != -1))
                    {
                        cellStr = "\"";
                        char[] sepa = { '"' };
                        string[] results = celldata.Split(sepa);
                        string escapeStr = String.Join("\"\"", results);
                        cellStr += escapeStr;
                        cellStr += "\"";
                    }
                    rowCells.Add(cellStr);
                }
                string rowStr = String.Join(",", rowCells.ToArray());
                rows.Add(rowStr);
            }

            string csvContent = String.Join("\r\n", rows);
            WriteFile(csvName, csvContent);
        }

        public static void WriteFile(string filePath, string content)
        {
            UTF8Encoding utf8WithBom = new UTF8Encoding(true);
            StreamWriter sw = new StreamWriter(filePath, false, utf8WithBom);
            sw.Write(content);
            sw.Dispose();
        }
    }
}

