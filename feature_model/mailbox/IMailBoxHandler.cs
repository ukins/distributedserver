﻿using ETT.Model.network;

namespace ETT.Model.mailbox
{
    public interface IMailBoxHandler
    {
        ETTask Handle(Session session, Entitas entitas, object actorMessage);
    }
}
