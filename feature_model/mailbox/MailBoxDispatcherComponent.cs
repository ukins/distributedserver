﻿using ETT.Model.network;
using System;
using System.Collections.Generic;

namespace ETT.Model.mailbox
{
    public class MailBoxDispatcherComponent : Component, IAwake
    {
        private readonly Dictionary<EMailBoxType, IMailBoxHandler> m_dict = new Dictionary<EMailBoxType, IMailBoxHandler>();

        public void Awake()
        {
            m_dict.Clear();

            List<Type> types = Context.Event.GetAttrTypes(typeof(MailBoxHandlerAttribute));

            foreach (var type in types)
            {
                object[] attrs = type.GetCustomAttributes(typeof(MailBoxHandlerAttribute), false);
                if (attrs.Length == 0)
                {
                    continue;
                }

                MailBoxHandlerAttribute attr = attrs[0] as MailBoxHandlerAttribute;
                if (attr == null)
                {
                    continue;
                }

                object obj = Activator.CreateInstance(type);
                IMailBoxHandler handler = obj as IMailBoxHandler;
                if (handler == null)
                {
                    throw new Exception($"actor handler not inherit IMailBoxHandler:{obj.GetType().FullName}");
                }

                m_dict.Add(attr.EMailBoxType, handler);
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            m_dict.Clear();
        }

        //public async ETTask Handle(MailBoxComponent mailbox, MessageInfo messageInfo)
        //{
        //    if (m_dict.TryGetValue(mailbox.MailboxType, out IMailBoxHandler handler))
        //    {
        //        await handler.Handle(messageInfo.Session, mailbox.Entitas, messageInfo.ProtoId, messageInfo.Message);
        //    }
        //}
        public async ETTask Handle(MailBoxComponent mailbox, Session session, IProtoPacker message)
        {
            if (m_dict.TryGetValue(mailbox.MailboxType, out IMailBoxHandler handler))
            {
                await handler.Handle(session, mailbox.Entitas, message);
            }
        }
    }
}
