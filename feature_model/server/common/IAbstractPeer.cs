﻿namespace ETT.Model.server
{
    public interface IAbstractPeer : IEntitas
    {
        EPeerType PeerType { get; }

        int RootId { get; set; }

        string InnerAddr { get; }


    }
}
