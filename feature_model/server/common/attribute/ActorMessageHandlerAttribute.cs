﻿using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class ActorMessageHandlerAttribute : BaseAttribute
    {
        public EPeerType ActorType { get; }

        public ActorMessageHandlerAttribute(EPeerType eActor)
        {
            ActorType = eActor;
        }
    }
}
