﻿using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class MessageAttribute : BaseAttribute
    {
        public uint ProtoId { get; }

        public MessageAttribute(uint id)
        {
            ProtoId = id;
        }
    }
}
