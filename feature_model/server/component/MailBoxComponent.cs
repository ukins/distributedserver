﻿using ETT.Model.network;
using System;
using System.Collections.Generic;

namespace ETT.Model.mailbox
{
    public struct MessageInfo
    {
        public Session Session;
        public uint ProtoId;
        public IProtoPacker Message;
    }

    public enum EMailBoxType
    {
        Dispatcher,
        GateSession
    }

    public class MailBoxComponent : Component, IAwake, IAwake<EMailBoxType>
    {
        public EMailBoxType MailboxType { get; protected set; }


        public void Awake()
        {
        }

        public void Awake(EMailBoxType mailboxtype)
        {
            MailboxType = mailboxtype;
        }


        public async ETTask Add(Session session, IProtoPacker message)
        {
            MailBoxDispatcherComponent dispatcher = Context.Game.GetComponent<MailBoxDispatcherComponent>();

            await dispatcher.Handle(this, session, message);
            //long instance_id = InstanceId;

            //while (true)
            //{
            //    if (InstanceId != instance_id)
            //    {
            //        return;
            //    }
            //    try
            //    {
            //        MessageInfo info = await GetAsync();
            //        if (info.Message == null)
            //        {
            //            return;
            //        }
            //        await dispatcher.Handle(this, info);
            //    }
            //    catch (Exception e)
            //    {
            //        Log.Error(e);
            //    }
            //}
        }
    }
}
