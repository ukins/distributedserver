﻿using ETT.Model.network;
using System;
using System.Collections.Generic;

namespace ETT.Model
{
    public class ProtoComponent : Component, IAwake<bool>
    {
        private class ProtoNode
        {
            public uint ProtoId;
            public Type Type;
            public IProtoPacker Instance;

        }

        private readonly Dictionary<uint, ProtoNode> m_id_protos = new Dictionary<uint, ProtoNode>();
        private readonly Dictionary<Type, ProtoNode> m_type_protos = new Dictionary<Type, ProtoNode>();

        public ProtoComponent() { }

        private bool IsServer { get; set; }

        public void Awake(bool isServer)
        {
            IsServer = isServer;

            m_id_protos.Clear();
            m_type_protos.Clear();

            var messages = Context.Event.GetAttrTypes(typeof(MessageAttribute));
            foreach (var type in messages)
            {
                object[] attr_list = type.GetCustomAttributes(typeof(MessageAttribute), false);
                if (attr_list.Length == 0)
                {
                    continue;
                }

                if (attr_list[0] is MessageAttribute message_attr)
                {
                    var node = new ProtoNode
                    {
                        ProtoId = message_attr.ProtoId,
                        Type = type,
                        Instance = Activator.CreateInstance(type) as IProtoPacker
                    };
                    m_id_protos.Add(node.ProtoId, node);
                    m_type_protos.Add(node.Type, node);
                }
            }
        }

        public uint GetProtoId(Type type)
        {
            var node = m_type_protos[type];
            return node.ProtoId;
        }

        public Type GetType(uint proto_id)
        {
            var node = m_id_protos[proto_id];
            return node.Type;
        }

        // 客户端为了0GC需要消息池，服务端消息需要跨协程不需要消息池
        public IProtoPacker GetInstance(uint proto_id)
        {
            if (IsServer == true)
            {

                Type type = GetType(proto_id);
                if (type == null)
                {
                    // 服务端因为有人探测端口，有可能会走到这一步，如果找不到proto_id，抛异常
                    throw new Exception($"not found proto: {proto_id}");
                }
                return Activator.CreateInstance(type) as IProtoPacker;
            }
            else
            {
                return m_id_protos[proto_id].Instance;
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();
            m_id_protos.Clear();
            m_type_protos.Clear();
        }
    }
}
