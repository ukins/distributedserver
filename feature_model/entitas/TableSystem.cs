﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public sealed class TableSystem : Entitas, IRootComponent
    {
        public string Name { get; set; }
        public int RootId { get; set; }

        public FactoryEntity Factory { get; }

        public TableSystem()
        {
            Root = this;
            Factory = new FactoryEntity(this);
        }
    }
}
