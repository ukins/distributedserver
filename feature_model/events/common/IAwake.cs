﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IAwake
    {
        void Awake();
    }

    public interface IAwake<T>
    {
        void Awake(T t);
    }

    public interface IAwake<T1, T2>
    {
        void Awake(T1 t1, T2 t2);
    }

    public interface IAwake<T1, T2, T3>
    {
        void Awake(T1 t1, T2 t2, T3 t3);
    }
}
