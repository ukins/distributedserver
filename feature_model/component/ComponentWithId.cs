﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class ComponentWithId : Component
    {
        public long Id { get; set; }

        public ComponentWithId()
        {
            this.Id = InstanceId;
        }

        public ComponentWithId(long id)
        {
            this.Id = id;
        }
    }
}
