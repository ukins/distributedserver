﻿using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public abstract class Component : IDisposable
    {
        public long InstanceId { get; set; }

        private bool m_is_from_pool;
        public bool IsFromPool
        {
            get
            {
                return m_is_from_pool;
            }
            set
            {
                m_is_from_pool = value;
                if (m_is_from_pool == false)
                {
                    return;
                }
            }
        }

        public bool IsDisposed => InstanceId == 0;

        public Component Parent { get; set; }

        public T GetParent<T>() where T : Component
        {
            return this.Parent as T;
        }

        public Entitas Entitas => Parent as Entitas;

        public IRootComponent Root { get; set; }

        protected Component()
        {
        }

        public virtual void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }

            // 触发Destroy事件
            Context.Event.Destroy(this);
            Context.Event.Remove(InstanceId);

            InstanceId = 0;

            if (m_is_from_pool == true)
            {
                Root.Factory.Recycle(this);
            }
        }
    }
}
