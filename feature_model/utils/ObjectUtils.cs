﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public static class ObjectUtils
    {
        public static void Swap<T>(ref T t1, ref T t2)
        {
            T t3 = t1;
            t1 = t2;
            t2 = t3;
        }
    }
}
