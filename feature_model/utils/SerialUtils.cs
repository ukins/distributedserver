﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public static class SerialUtils
    {
        private static Dictionary<int, long> m_instance_dict = new Dictionary<int, long>();

        private static ushort m_serial_num;

        public static long GenerateId(int peer_id)
        {
            long time = TimeUtils.ClientNowSeconds();
            return (peer_id << 48) + (time << 16) + (++m_serial_num);
        }

        public static int GetPeerId(long v)
        {
            return (int)(v >> 48);
        }

        public static long GenerateInstanceId(int peer_id)
        {
            if (m_instance_dict.ContainsKey(peer_id) == false)
            {
                m_instance_dict.Add(peer_id, ((long)peer_id) << 48);
            }
            m_instance_dict[peer_id]++;
            return m_instance_dict[peer_id];
        }
    }
}
