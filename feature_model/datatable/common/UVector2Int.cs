﻿using ETT.Model;
using System;

namespace ETT.Model
{
    public struct UVector2Int : IComparable, IComparable<UVector2Int>, IEquatable<UVector2Int>
    {
        public static readonly UVector2Int Invalid = Parse(int.MaxValue, int.MaxValue);

        public int x;
        public int y;

        public UVector2Int(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static UVector2Int Parse(int x, int y)
        {
            UVector2Int point;
            point.x = x;
            point.y = y;
            return point;
        }

        public static UVector2Int Parse(string s)
        {
            UVector2Int point = Parse(0, 0);
            s = s.Replace("(", "").Replace(")", "");
            string[] nums = s.Split(new char[] { ';', '_', '|', '?', '!', '/', ':', ' ' });
            DataTypeUtils.Parse(ref point.x, nums, 0);
            DataTypeUtils.Parse(ref point.y, nums, 1);
            return point;
        }

        public static UVector2Int operator +(UVector2Int point, UVector2Int vec)
        {
            UVector2Int newpoint;
            newpoint.x = point.x + vec.x;
            newpoint.y = point.y + vec.y;
            return newpoint;
        }

        public static UVector2Int operator -(UVector2Int point, UVector2Int vec)
        {
            UVector2Int newpoint;
            newpoint.x = point.x - vec.x;
            newpoint.y = point.y - vec.y;
            return newpoint;
        }

        public static UVector2Int operator *(UVector2Int point, int scale)
        {
            UVector2Int newpoint;
            newpoint.x = point.x * scale;
            newpoint.y = point.y * scale;
            return newpoint;
        }

        public int CompareTo(object obj)
        {
            if (obj == null || (obj is UVector2Int) == false)
            {
                return 1;
            }
            UVector2Int other = (UVector2Int)obj;
            return CompareTo(other);
        }

        public int CompareTo(UVector2Int other)
        {
            if (x > other.x)
            {
                return 1;
            }
            else if (x < other.x)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }
            else if (y < other.y)
            {
                return -1;
            }

            return 0;
        }

        public bool Equals(UVector2Int other)
        {
            return CompareTo(other) == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || (obj is UVector2Int) == false)
            {
                return false;
            }
            return Equals((UVector2Int)obj);
        }

        public override int GetHashCode()
        {
            return ((x << 16) ^ y).GetHashCode();
        }

        public override string ToString()
        {
            return $"({x},{y})";
        }
    }
}
