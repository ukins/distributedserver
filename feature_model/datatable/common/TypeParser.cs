﻿using System.Linq;

namespace ETT.Table
{
    public class TypeParser
    {
        private string m_value = string.Empty;
        public string Value { get { return m_value; } }

        private EFieldType m_fieldType = EFieldType.Invalid;
        public EFieldType FieldType { get { return m_fieldType; } }

        private string m_element_fisrt = string.Empty;
        public string FirstElementType { get { return m_element_fisrt; } }

        private string m_element_second = string.Empty;
        public string SecondElementType { get { return m_element_second; } }

        public bool IsCollection
        {
            get { return (m_fieldType > EFieldType.Single); }
        }

        private int m_element_cnt = 0;
        public int ElementCnt { get { return m_element_cnt; } }

        public TypeParser(string value)
        {
            m_value = value;

            string[] strArr = value.Split(new char[] { '(', ')' });
            var list = strArr.ToList();
            list.RemoveAll(c => string.IsNullOrEmpty(c.Trim()));
            strArr = list.ToArray();

            if (strArr.Length == 1)
            {
                m_fieldType = EFieldType.Single;
                m_element_fisrt = strArr[0];
            }
            else if (strArr.Length == 2)
            {
                if (strArr[0].ToLower() == "array")
                {
                    m_fieldType = EFieldType.Array;
                    m_element_fisrt = strArr[1];
                }
                else if (strArr[0].ToLower() == "list")
                {
                    m_fieldType = EFieldType.List;
                    m_element_fisrt = strArr[1];
                }
            }
            else if (strArr.Length == 3)
            {
                if (strArr[0].ToLower() == "dict" || strArr[0].ToLower() == "dictionary")
                {
                    m_fieldType = EFieldType.Dict;
                    m_element_fisrt = strArr[1];
                    m_element_second = strArr[2];
                }

            }

        }
    }
}
