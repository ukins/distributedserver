﻿using System;

namespace ETT.Model
{
    public struct UVector3Int : IComparable, IComparable<UVector3Int>, IEquatable<UVector3Int>
    {
        public static readonly UVector3Int Invalid = Parse(int.MaxValue, int.MaxValue, int.MaxValue);

        public int x;
        public int y;
        public int z;

        public UVector3Int(int x, int y, int z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static UVector3Int Parse(int x, int y, int z)
        {
            UVector3Int point;
            point.x = x;
            point.y = y;
            point.z = z;
            return point;
        }
        
        public static UVector3Int Parse(string s)
        {
            UVector3Int point = Parse(0, 0, 0);
            s = s.Replace("(", "").Replace(")", "");
            string[] nums = s.Split(new char[] { '_', '|', '?', '!', '/', ':', ' ' });
            DataTypeUtils.Parse(ref point.x, nums, 0);
            DataTypeUtils.Parse(ref point.y, nums, 1);
            DataTypeUtils.Parse(ref point.z, nums, 2);
            return point;
        }

        public static UVector3Int operator +(UVector3Int point, UVector3Int vec)
        {
            UVector3Int newpoint;
            newpoint.x = point.x + vec.x;
            newpoint.y = point.y + vec.y;
            newpoint.z = point.z + vec.z;
            return newpoint;
        }

        public static UVector3Int operator -(UVector3Int point, UVector3Int vec)
        {
            UVector3Int newpoint;
            newpoint.x = point.x - vec.x;
            newpoint.y = point.y - vec.y;
            newpoint.z = point.z - vec.z;
            return newpoint;
        }

        public int CompareTo(object obj)
        {
            if (obj == null || (obj is UVector3Int) == false)
            {
                return 1;
            }
            UVector3Int other = (UVector3Int)obj;
            return CompareTo(other);
        }

        public int CompareTo(UVector3Int other)
        {
            if (x > other.x)
            {
                return 1;
            }
            else if (x < other.x)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }
            else if (y < other.y)
            {
                return -1;
            }

            if (z > other.z)
            {
                return 1;
            }
            else if (z < other.z)
            {
                return -1;
            }

            return 0;
        }

        public bool Equals(UVector3Int other)
        {
            return CompareTo(other) == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || (obj is UVector3Int) == false)
            {
                return false;
            }
            return Equals((UVector3Int)obj);
        }

        public override int GetHashCode()
        {
            return (((x << 16) ^ y) << 16 ^ z).GetHashCode();
        }

        public override string ToString()
        {
            return string.Format("({0},{1},{2})", x, y, z);
        }
    }
}
