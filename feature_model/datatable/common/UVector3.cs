﻿using System;

namespace ETT.Model
{
    public struct UVector3 : IComparable, IComparable<UVector3>, IEquatable<UVector3>
    {
        public static readonly UVector3 Invalid = Parse(float.MaxValue, float.MaxValue, float.MaxValue);

        public float x;
        public float y;
        public float z;

        public UVector3(float x, float y, float z)
        {
            this.x = x;
            this.y = y;
            this.z = z;
        }

        public static UVector3 Parse(float x, float y, float z)
        {
            UVector3 point;
            point.x = x;
            point.y = y;
            point.z = z;
            return point;
        }

        public static UVector3 Parse(string s)
        {
            UVector3 point = Parse(0, 0, 0);
            s = s.Replace("(", "").Replace(")", "");
            string[] nums = s.Split(new char[] { '_', '|', '?', '!', '/', ':', ' ' });
            DataTypeUtils.Parse(ref point.x, nums, 0);
            DataTypeUtils.Parse(ref point.y, nums, 1);
            DataTypeUtils.Parse(ref point.z, nums, 2);

            return point;
        }

        public static UVector3 operator +(UVector3 point, UVector3 vec)
        {
            UVector3 newpoint;
            newpoint.x = point.x + vec.x;
            newpoint.y = point.y + vec.y;
            newpoint.z = point.z + vec.z;
            return newpoint;
        }

        public static UVector3 operator -(UVector3 point, UVector3 vec)
        {
            UVector3 newpoint;
            newpoint.x = point.x - vec.x;
            newpoint.y = point.y - vec.y;
            newpoint.z = point.z - vec.z;
            return newpoint;
        }

        public int CompareTo(object obj)
        {
            if (obj == null || (obj is UVector3) == false)
            {
                return 1;
            }
            UVector3 other = (UVector3)obj;
            return CompareTo(other);
        }

        public int CompareTo(UVector3 other)
        {
            if (x > other.x)
            {
                return 1;
            }
            else if (x < other.x)
            {
                return -1;
            }

            if (y > other.y)
            {
                return 1;
            }
            else if (y < other.y)
            {
                return -1;
            }

            if (z > other.z)
            {
                return 1;
            }
            else if (z < other.z)
            {
                return -1;
            }

            return 0;
        }

        public bool Equals(UVector3 other)
        {
            return CompareTo(other) == 0;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || (obj is UVector3) == false)
            {
                return false;
            }
            return Equals((UVector3)obj);
        }

        public override int GetHashCode()
        {
            int xcode = x.GetHashCode();
            int ycode = y.GetHashCode();
            int zcode = z.GetHashCode();

            return ((((xcode << 16) ^ ycode) << 16) ^ zcode).GetHashCode();
        }

        public override string ToString()
        {
            return $"({x},{y})";
        }
    }
}
