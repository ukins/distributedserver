﻿using System.Text;
using System.Xml;

namespace ETT.Table.Analysis
{
    public class LeafFieldVO : AbstractDataVO, IFieldVO
    {
        /// <summary>
        /// 字段类型
        /// </summary>
        protected string m_type = string.Empty;
        protected TypeParser m_typeparser;
        public TypeParser Type { get { return m_typeparser; } }

        public bool InCollection
        {
            get
            {
                var parent = Owner as NodeFieldVO;
                while (parent != null)
                {
                    if (parent.Type.IsCollection == true)
                    {
                        return true;
                    }
                    parent = parent.Owner as NodeFieldVO;
                }
                return Type.IsCollection;
            }
        }
        /// <summary>
        /// 该字段是否主键
        /// </summary>
        public bool IsPrimaryKey { get; protected set; }

        /// <summary>
        /// 该字段是否联合主键
        /// </summary>
        public bool IsUnionKey { get; protected set; }

        /// <summary>
        /// 外接表
        /// </summary>
        public string Foreigntable { get; protected set; }

        /// <summary>
        /// 描述
        /// </summary>
        public string Desc { get; protected set; }

        /// <summary>
        /// 与容器属性配合使用
        /// </summary>
        public bool IsVertical { get; protected set; } = true;

        protected void ParseField(XmlNode node)
        {
            XmlNode = node;
            Name = ExtractString(node, "name");
            m_type = ExtractString(node, "type");
            IsPrimaryKey = ExtractBool(node, "primarykey");
            IsUnionKey = ExtractBool(node, "unionkey");
            Foreigntable = ExtractString(node, "foreigntable");
            Desc = ExtractString(node, "desc");
            //ExtractValue(node, "isvertical", out m_isvertical, true);

            m_typeparser = new TypeParser(m_type);
        }

        public override void Parse(XmlNode node)
        {
            ParseField(node);
        }

        public override string ToXMLString(string prefix)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append(prefix);
            sb.Append("<field ");
            if (Name != string.Empty)
            {
                sb.Append(string.Format("name=\'{0}\' ", Name));
            }
            if (m_type != string.Empty)
            {
                sb.Append(string.Format("type=\'{0}\' ", m_type));
            }
            if (IsPrimaryKey == true)
            {
                sb.Append(string.Format("primarykey=\'{0}\' ", "true"));
            }
            if (IsUnionKey == true)
            {
                sb.Append(string.Format("unionkey=\'{0}\' ", "true"));
            }
            if (Foreigntable != string.Empty)
            {
                sb.Append(string.Format("foreigntable=\'{0}\' ", Foreigntable));
            }
            if (Desc != string.Empty)
            {
                sb.Append(string.Format("desc=\'{0}\' ", Desc));
            }
            if (IsVertical == false)
            {
                sb.Append(string.Format("isvertical=\'{0}\' ", "false"));
            }
            sb.Append("/>");
            return sb.ToString();
        }
    }
}
