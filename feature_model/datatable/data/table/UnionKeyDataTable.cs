﻿namespace ETT.Table.Analysis
{
    public class UnionKeyDataTable : AbstractDataTable
    {
        public LeafFieldVO UnionKey1 => Columns.UnionKey.First;
        public LeafFieldVO UnionKey2 => Columns.UnionKey.Second;

        protected readonly UnionKeyDataRowCollection m_rows;
        public override GenericDataRowCollection Rows => m_rows;

        public UnionKeyDataTable(TableVO vo) : base(vo)
        {
            m_rows = new UnionKeyDataRowCollection(this);
        }

        public override void Union(AbstractDataTable table)
        {
            var unionkeytable = table as UnionKeyDataTable;
            if (unionkeytable == null || unionkeytable.TableName != TableName)
            {
                return;
            }
            m_rows.Union(unionkeytable.m_rows);
        }

        //public override string ToString()
        //{
        //    return string.Format("{0}\n{1}", m_columns, m_rows);
        //}

        public override AbstractDataTable Clone()
        {
            UnionKeyDataTable table = new UnionKeyDataTable(m_columns);
            table.m_rows.Union(m_rows);
            return table;
        }
    }
}
