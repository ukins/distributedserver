﻿using ETT.Model;
using System.Collections.Generic;
using System.IO;

namespace ETT.Table.Analysis
{
    public class GenericDataNodeField : GenericDataField
    {
        private readonly List<GenericDataField> m_fields = new List<GenericDataField>();

        private NodeFieldVO m_fieldvo;
        public NodeFieldVO FieldVO { get { return m_fieldvo; } }

        private List<GenericDataNodeField> m_nextlines = new List<GenericDataNodeField>();

        private List<GenericDataLeafField> m_allleaves = new List<GenericDataLeafField>();
        public List<GenericDataLeafField> FindAllLeafFields()
        {
            if (m_allleaves.Count == 0)
            {
                for (int k = 0; k < m_nextlines.Count; k++)
                {
                    var node = m_nextlines[k];
                    for (int i = 0; i < node.m_fields.Count; i++)
                    {
                        if (node.m_fields[i] is GenericDataLeafField)
                        {
                            m_allleaves.Add(node.m_fields[i] as GenericDataLeafField);
                        }
                        else
                        {
                            m_allleaves.AddRange((node.m_fields[i] as GenericDataNodeField).FindAllLeafFields());
                        }
                    }
                }
            }

            return m_allleaves;
        }

        public List<GenericDataLeafField> FindAllLeafFields(string columnname)
        {
            var allleaves = FindAllLeafFields();

            return allleaves.FindAll(c => c.Column.FullName == columnname);
        }

        protected internal GenericDataNodeField(IFieldVO column, int rowidx) : base(column, rowidx)
        {
            m_fieldvo = column as NodeFieldVO;
            var fields = m_fieldvo.FindFields();
            for (int i = 0; i < fields.Count; ++i)
            {
                GenericDataField field = null;
                if (fields[i] is LeafFieldVO)
                {
                    field = new GenericDataLeafField(fields[i], rowidx);
                }
                else if (fields[i] is NodeFieldVO)
                {
                    field = new GenericDataNodeField(fields[i], rowidx);
                }

                if (field != null)
                {
                    m_fields.Add(field);
                }
                else
                {
                    Log.Error("GenericDataRow Create Field Failed");
                }
            }
        }
        public override void SetValue(string[] row_fields, ref int idx)
        {
            for (int i = 0; i < m_fields.Count; i++)
            {
                m_fields[i].SetValue(row_fields, ref idx);
            }
            m_nextlines.Clear();
            m_nextlines.Add(this);
        }

        public override void Merge(GenericDataField field)
        {
            var node = field as GenericDataNodeField;

            bool isnextline = true;
            for (int i = 0; i < node.m_fields.Count; i++)
            {
                var leaffield = node.m_fields[i] as GenericDataLeafField;
                if (leaffield != null)
                {
                    if (string.IsNullOrEmpty(leaffield.Value.Trim()) == true)
                    {
                        isnextline = false;
                    }
                    break;
                }
            }
            if (isnextline == true && Column.Type.IsCollection)
            {
                m_nextlines.Add(node);
            }
            else
            {
                var lastline = m_nextlines[m_nextlines.Count - 1];
                for (int i = 0; i < lastline.m_fields.Count; i++)
                {
                    if (lastline.m_fields[i] is GenericDataLeafField)
                    {
                        var leaffield = lastline.m_fields[i] as GenericDataLeafField;
                        if (leaffield.Column.Type.IsCollection == true)
                        {
                            leaffield.Merge(node.m_fields[i]);
                        }
                    }
                    else
                    {
                        var nodefield = lastline.m_fields[i] as GenericDataNodeField;
                        if (nodefield != null)
                        {
                            nodefield.Merge(node.m_fields[i]);
                        }
                    }
                }
            }
        }

        private void BuildSingleNode(BinaryWriter bw, GenericDataNodeField node)
        {
            for (int i = 0; i < node.m_fields.Count; i++)
            {
                node.m_fields[i].Build(bw);
            }
        }

        public override void Build(BinaryWriter bw)
        {
            if (Column.Type.IsCollection == true)
            {
                bw.Write(m_nextlines.Count);
                for (int i = 0; i < m_nextlines.Count; i++)
                {
                    var line = m_nextlines[i];
                    BuildSingleNode(bw, line);
                }
            }
            else
            {
                BuildSingleNode(bw, this);
            }
        }
    }
}
