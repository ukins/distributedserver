﻿namespace ETT.Table.Analysis
{
    public class UnionKeyDataRowCollection : GenericDataRowCollection
    {
        public UnionKeyDataRowCollection(UnionKeyDataTable table) : base(table) { }

        private GenericDataRow m_lastRow;
        public override void Add(GenericDataRow row)
        {
            string unionkeyname1 = Table.Columns.UnionKey.First.FullName;
            string unionkeyname2 = Table.Columns.UnionKey.Second.FullName;

            var leaffield1 = row.FindLeafField(unionkeyname1);
            var leaffield2 = row.FindLeafField(unionkeyname2);
            if (string.IsNullOrEmpty(leaffield1.Value.Trim()) == true
                && string.IsNullOrEmpty(leaffield2.Value.Trim()) == true)
            {
                m_lastRow?.Merge(row);
            }
            else
            {
                m_lastRow = row;
                m_list.Add(row);
            }
        }

        public void Union(UnionKeyDataRowCollection rows)
        {
            if (ReferenceEquals(this, rows) == false)
            {
                m_list.AddRange(rows.m_list);
            }
        }
    }
}
