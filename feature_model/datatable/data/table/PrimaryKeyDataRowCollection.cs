﻿namespace ETT.Table.Analysis
{
    public class PrimaryKeyDataRowCollection : GenericDataRowCollection
    {
        public PrimaryKeyDataRowCollection(PrimaryKeyDataTable table) : base(table) { }

        private GenericDataRow m_lastRow;
        public override void Add(GenericDataRow row)
        {
            var columnname = m_table.Columns.PrimaryKey.FullName;
            var leaffield = row.FindLeafField(columnname);
            if (string.IsNullOrEmpty(leaffield.Value.Trim()) == true)
            {
                m_lastRow?.Merge(row);
            }
            else
            {
                m_lastRow = row;
                m_list.Add(row);
            }
        }

        public void Union(PrimaryKeyDataRowCollection rows)
        {
            if (ReferenceEquals(this, rows) == false)
            {
                m_list.AddRange(rows.m_list);
            }
        }
    }
}
