﻿using System.IO;

namespace ETT.Table.Analysis
{
    public class GenericDataLeafField : GenericDataField
    {
        private LeafFieldVO m_fieldvo;
        public LeafFieldVO FieldVO { get { return m_fieldvo; } }

        protected string m_strvalue = string.Empty;
        public string Value => m_strvalue;

        private bool isfreeze = false;

        protected internal GenericDataLeafField(IFieldVO column, int rowidx) : base(column, rowidx)
        {
            m_fieldvo = column as LeafFieldVO;
        }

        public override void SetValue(string[] row_fields, ref int idx)
        {
            m_strvalue = row_fields[idx];
            idx++;
            if (m_fieldvo.Type.IsCollection == true)
            {
                m_slice.AddRange(m_strvalue.Split('|'));
            }
            else
            {
                m_slice.Add(m_strvalue);
            }
        }

        public override void Merge(GenericDataField field)
        {
            var leaf = field as GenericDataLeafField;
            if (m_fieldvo.InCollection == false || isfreeze == true)
            {
                return;
            }
            if (string.IsNullOrEmpty(leaf.Value.Trim()) == true)
            {
                isfreeze = true;
                return;
            }

            m_slice.Add(leaf.Value);
        }

        public override void Build(BinaryWriter bw)
        {
            if (m_fieldvo.Type.IsCollection == true)
            {
                bw.Write(m_slice.Count);
                for (int i = 0; i < m_slice.Count; ++i)
                {
                    DTDefine.WriteData(bw, m_slice[i], m_fieldvo.Type.FirstElementType);
                }
            }
            else
            {
                DTDefine.WriteData(bw, m_strvalue, m_fieldvo.Type.FirstElementType);
            }
        }
    }
}
