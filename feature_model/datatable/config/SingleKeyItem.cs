﻿using System;

namespace ETT.Table
{
    public abstract class SingleKeyItem<K> : AbstractItem
        where K : IComparable
    {
        public abstract K PrimaryKey { get; }

    }
}
