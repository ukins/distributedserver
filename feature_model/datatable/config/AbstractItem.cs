﻿using System.IO;

namespace ETT.Table
{
    public abstract class AbstractItem
    {
        public abstract bool Deserialize(BinaryReader br);
    }
}
