﻿using System.Collections.Generic;
using System.IO;
using System;

namespace ETT.Table
{
    public abstract class UnionKeyTable<T, K1, K2> : AbstractTable<T>, IUnionKeyTable
        where T : UnionKeyItem<K1, K2>, new()
        where K1 : IComparable
        where K2 : IComparable
    {
        protected readonly Dictionary<K1, Dictionary<K2, T>> m_dict = new Dictionary<K1, Dictionary<K2, T>>();

        public abstract AbstractItem FindVO(string key1, string key2);

        public T GenericFindVO(K1 unionkey1, K2 unionkey2)
        {
            if (m_dict.ContainsKey(unionkey1) == false)
            {
                return null;
            }
            var dict2 = m_dict[unionkey1];
            if (dict2 == null || dict2.ContainsKey(unionkey2) == false)
            {
                return null;
            }
            return dict2[unionkey2];
        }

        public List<T> GenericRange(K1 min, K1 max)
        {
            List<T> lst = new List<T>();
            foreach (var pair in m_dict)
            {
                if (pair.Key.CompareTo(min) >= 0 && pair.Key.CompareTo(max) <= 0)
                {
                    lst.AddRange(pair.Value.Values);
                }
            }
            return lst;
        }

        public override bool Deserialize(BinaryReader br)
        {
            var len = br.ReadUInt32();
            for (int i = 0; i < len; ++i)
            {
                T conf = new T();
                conf.Deserialize(br);
                if (m_dict.ContainsKey(conf.UnionKey1) == false)
                {
                    m_dict.Add(conf.UnionKey1, new Dictionary<K2, T>());
                }
                var dict2 = m_dict[conf.UnionKey1];
                dict2.Add(conf.UnionKey2, conf);
                m_list.Add(conf);
            }
            //m_isDeserialized = true;
            return true;
        }

        //public override bool Deserialize(StringReader sr)
        //{
        //    string[] titlefields = null;
        //    T lastConf = null;
        //    while (sr.Peek() > -1)
        //    {
        //        string lineStr = sr.ReadLine();

        //        string[] fields = lineStr.Split(',');
        //        if (fields[0].StartsWith("#"))
        //        {
        //            continue;
        //        }
        //        if (fields[0] == "&&")
        //        {
        //            titlefields = fields;
        //            FilterTitleFields(ref titlefields);
        //            continue;
        //        }
        //        if (titlefields == null)
        //        {
        //            continue;
        //        }
        //        fields = FilterDataFields(fields, ref titlefields);

        //        T conf = new T();
        //        bool result = conf.Deserialize(ref fields);
        //        if (result == false)
        //        {
        //            continue;
        //        }
        //        if (conf.UnionKey1.Equals(default(K1)) == true
        //            && conf.UnionKey2.Equals(default(K2)) == true)
        //        {
        //            if (lastConf == null)
        //            {
        //                continue;
        //            }
        //            lastConf.Append(conf);
        //        }
        //        else
        //        {
        //            lastConf = conf;
        //            if (m_dict.ContainsKey(conf.UnionKey1) == false)
        //            {
        //                m_dict.Add(conf.UnionKey1, new Dictionary<K2, T>());
        //            }

        //            var dict2 = m_dict[conf.UnionKey1];
        //            if (dict2.ContainsKey(conf.UnionKey2) == false)
        //            {
        //                dict2.Add(conf.UnionKey2, conf);
        //                m_list.Add(conf);
        //            }
        //        }
        //    }
        //    m_isDeserialized = true;
        //    return true;
        //}

    }
}