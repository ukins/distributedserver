﻿using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Table
{
    public interface IAbstractTable
    {
        string Name { get; }
        List<string> Path { get; }
        TableVO TableVO { get; }

        CSVDataTable CSVDT
        {
            get;
            set;
        }

        bool Deserialize(BinaryReader br);

        IEnumerable Range(string min, string max);
    }

    public interface ISingleKeyTable
    {
        AbstractItem FindVO(string key);


    }

    public interface IUnionKeyTable
    {
        AbstractItem FindVO(string key1, string key2);
    }
}
