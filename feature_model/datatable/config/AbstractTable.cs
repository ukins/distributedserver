﻿using ETT.Model;
using ETT.Table.Analysis;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;

namespace ETT.Table
{
    public abstract class AbstractTable<T> : Component, IAbstractTable
        where T : AbstractItem
    {
        public abstract string Name { get; }
        public abstract List<string> Path { get; }

        public abstract string XmlContent { get; }

        protected TableVO m_tablevo = new TableVO();
        public TableVO TableVO => m_tablevo;

        public CSVDataTable CSVDT
        {
            get;
            set;
        }

        public AbstractTable()
        {
            XmlDocument doc = new XmlDocument();
            doc.LoadXml(XmlContent);
            m_tablevo.Parse(doc.SelectSingleNode("table"));
        }

        protected readonly List<T> m_list = new List<T>();
        public List<T> FindAllVO()
        {
            return m_list;
        }

        public abstract bool Deserialize(BinaryReader br);

        public abstract IEnumerable Range(string min, string max);

        protected string[] FilterDataFields(string[] datafields, ref string[] titlefields)
        {
            List<string> lst = new List<string>();

            for (int i = 0; i < titlefields.Length; ++i)
            {
                if (titlefields[i] == string.Empty)
                {
                    continue;
                }
                if (i >= datafields.Length)
                {
                    lst.Add("");
                }
                else
                {
                    lst.Add(datafields[i].Trim());
                }
            }

            return lst.ToArray();
        }
    }
}
