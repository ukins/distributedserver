﻿using ETT.Model.log;
using System;

namespace ETT.Model
{
    public static class Log
    {
        private static ILog adapter;

        public static void Init(ILog log)
        {
            adapter = log;
        }

        public static void Trace(string message)
        {
            adapter.Trace(message);
        }

        public static void Trace(string message, params object[] args)
        {
            adapter.Trace(message, args);
        }

        public static void Warning(string message)
        {
            adapter.Warning(message);
        }
        public static void Warning(string message, params object[] args)
        {
            adapter.Warning(message, args);
        }

        public static void Info(string message)
        {
            adapter.Info(message);
        }
        public static void Info(string message, params object[] args)
        {
            adapter.Info(message, args);
        }

        public static void Debug(string message)
        {
            adapter.Debug(message);
        }
        public static void Debug(string message, params object[] args)
        {
            adapter.Debug(message, args);
        }

        public static void Error(Exception e)
        {
            adapter.Error(e.ToString());
        }

        public static void Error(string message)
        {
            adapter.Error(message);
        }
        public static void Error(string message, params object[] args)
        {
            adapter.Error(message, args);
        }

        public static void Fatal(Exception e)
        {
            adapter.Fatal(e.ToString());
        }

        public static void Fatal(string message)
        {
            adapter.Fatal(message);
        }

        public static void Fatal(string message, params object[] args)
        {
            adapter.Fatal(message, args);
        }

        public static void Msg(object message)
        {
            //adapter.Debug(MongoHelper.ToJson(message));
        }
    }
}
