using System.Threading;

namespace ETT.Model
{
    public class ETCancellationTokenSource : Component, IAwake, IAwake<long>
    {
        public CancellationTokenSource CancellationTokenSource;

        public void Awake()
        {
            CancellationTokenSource = new CancellationTokenSource();
        }

        public void Awake(long afterTimeCancel)
        {
            CancellationTokenSource = new CancellationTokenSource();
            CancelAfter(afterTimeCancel).Coroutine();
        }

        public void Cancel()
        {
            this.CancellationTokenSource.Cancel();
            this.Dispose();
        }

        public async ETVoid CancelAfter(long afterTimeCancel)
        {
            await Context.Game.GetComponent<TimeComponent>().WaitAsync(afterTimeCancel);
            this.CancellationTokenSource.Cancel();
            this.Dispose();
        }

        public CancellationToken Token
        {
            get
            {
                return this.CancellationTokenSource.Token;
            }
        }

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            base.Dispose();

            this.CancellationTokenSource?.Dispose();
            this.CancellationTokenSource = null;
        }

    }
}