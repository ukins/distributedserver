﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model.Command
{
    public class CommandOptionAttribute : BaseAttribute
    {
        public string Command { get; }

        public CommandOptionAttribute(string cmd)
        {
            Command = cmd;
        }
    }
}
