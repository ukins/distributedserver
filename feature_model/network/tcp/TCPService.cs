﻿using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ETT.Model.network
{
    public sealed class TCPService : AbstractService, IAwake, IAwake<IPEndPoint, Action<AbstractChannel>>
    {
        public RecyclableMemoryStreamManager MemoryStreamManager = new RecyclableMemoryStreamManager();

        private readonly Dictionary<long, TCPChannel> m_channel_dict = new Dictionary<long, TCPChannel>();
        private readonly SocketAsyncEventArgs m_inn_args = new SocketAsyncEventArgs();
        private Socket m_acceptor;

        private HashSet<long> m_need_start_send_channel = new HashSet<long>();

        public void Awake(IPEndPoint ipEndPoint, Action<AbstractChannel> acceptCallBack)
        {
            AcceptCallback += acceptCallBack;

            m_acceptor = new Socket(AddressFamily.InterNetwork, SocketType.Stream, ProtocolType.Tcp);
            m_acceptor.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
            m_inn_args.Completed += OnComplete;

            m_acceptor.Bind(ipEndPoint);
            m_acceptor.Listen(1000);

            AcceptAsync();

            Log.Debug("TCPService Start Succ");
        }

        public void Awake()
        {

        }

        public override void Dispose()
        {
            foreach (var pair in m_channel_dict)
            {
                pair.Value.Dispose();
            }
            m_channel_dict.Clear();

            m_acceptor?.Close();
            m_acceptor = null;
            m_inn_args.Dispose();
        }

        private void OnComplete(object sender, SocketAsyncEventArgs e)
        {
            switch (e.LastOperation)
            {
                case SocketAsyncOperation.Accept:
                    ThreadSync.Instance.Post(OnAcceptComplete, e);
                    break;
                default:
                    throw new Exception($"socket accept error:{e.LastOperation}");
            }
        }

        private void AcceptAsync()
        {
            m_inn_args.AcceptSocket = null;
            if (m_acceptor.AcceptAsync(m_inn_args) == true)
            {
                return;
            }
            OnAcceptComplete(m_inn_args);
        }

        private void OnAcceptComplete(object o)
        {
            if (m_acceptor == null)
            {
                return;
            }

            SocketAsyncEventArgs e = (SocketAsyncEventArgs)o;

            if (e.SocketError != SocketError.Success)
            {
                Log.Error($"accept error {e.SocketError}");
                AcceptAsync();
                return;
            }

            TCPChannel channel = Root.Factory.CreateWithId<TCPChannel, Socket, TCPService>(e.AcceptSocket, this, false);
            m_channel_dict[channel.Id] = channel;

            try
            {
                OnAccept(channel);
            }
            catch (Exception exception)
            {
                Log.Error(exception);
            }

            if (m_acceptor == null)
            {
                return;
            }

            AcceptAsync();
        }

        public override AbstractChannel ConnectChannel(IPEndPoint iPEndPoint)
        {
            TCPChannel channel = Root.Factory.CreateWithId<TCPChannel, IPEndPoint, TCPService>(iPEndPoint, this, false);
            m_channel_dict[channel.Id] = channel;
            return channel;
        }

        public override AbstractChannel ConnectChannel(string address)
        {
            IPEndPoint ip_end_point = NetworkUtils.ToIPEndPoint(address);
            return this.ConnectChannel(ip_end_point);
        }

        public override AbstractChannel GetChannel(long id)
        {
            m_channel_dict.TryGetValue(id, out TCPChannel channel);
            return channel;
        }

        public void MarkNeedStartSend(long id)
        {
            m_need_start_send_channel.Add(id);
        }


        public override void Remove(long id)
        {
            if (m_channel_dict.TryGetValue(id, out TCPChannel channel) == false)
            {
                return;
            }
            if (channel == null)
            {
                return;
            }
            m_channel_dict.Remove(id);
            channel.Dispose();
        }

        public override void Update()
        {
            foreach (long id in m_need_start_send_channel)
            {
                if (m_channel_dict.TryGetValue(id, out TCPChannel channel) == false)
                {
                    continue;
                }
                if (channel.IsSending)
                {
                    continue;
                }
                try
                {
                    channel.BeginSend();
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }
            }

            m_need_start_send_channel.Clear();
        }
    }
}
