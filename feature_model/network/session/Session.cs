﻿using ETT.Model.proto;
using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading;

namespace ETT.Model.network
{
    public class Session : Entitas, IAbstractChannel, IAwake<AbstractChannel>
    {
        private static int RpcId { get; set; }

        private AbstractChannel Channel { get; set; }

        private readonly Dictionary<int, Action<IResponse>> m_request_dict = new Dictionary<int, Action<IResponse>>();

        public NetworkComponent Network => GetParent<NetworkComponent>();

        public int Error { get => Channel.Error; set => Channel.Error = value; }

        public IPEndPoint RemoteAddress => Channel.RemoteAddress;

        public MemoryStream Stream => Channel.Stream;

        private readonly byte[] opcodeBytes = new byte[2];

        public event Action<Session, int> OnErrorCallback;

        public void Awake(AbstractChannel channel)
        {
            Channel = channel;

            m_request_dict.Clear();

            long id = Id;
            channel.ErrorCallback += (c, e) =>
              {
                  OnErrorCallback?.Invoke(this, e);
                  this.Network.Remove(id);
              };
            channel.ReadCallback += this.OnRead;
        }

        public void Start()
        {
            Channel.Start();
        }

        public void Send(IProtoPacker message)
        {
            uint proto_id = Context.Game.GetComponent<ProtoComponent>().GetProtoId(message.GetType());

            Send(proto_id, message);
        }

        public void Send(uint proto_id, IProtoPacker message)
        {
            //待优化
            if (this.IsDisposed)
            {
                throw new Exception("session已经被Dispose了");
            }

            MemoryStream stream = this.Stream;
            stream.Seek(0, SeekOrigin.Begin);

            BinaryWriter writer = new BinaryWriter(stream);
            writer.Write(proto_id);
            message.Pack(writer);

            stream.Seek(0, SeekOrigin.Begin);

            Send(stream);
        }

        public void Send(MemoryStream stream)
        {
            Channel.Send(stream);
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            Network.Remove(Id);
            OnErrorCallback = null;

            base.Dispose();

            foreach (var pair in m_request_dict)
            {
                //pair.Value.Invoke(new GenericMessage { Error = this.Error });
            }

            Channel.Dispose();

            m_request_dict.Clear();
        }

        public void DisposeWithoutRemove()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            Channel.Dispose();

            m_request_dict.Clear();
        }

        private void OnRead(MemoryStream stream)
        {
            try
            {
                Run(stream);
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        private void Run(MemoryStream stream)
        {
            var stream_buffer = ((RecyclableMemoryStream)stream).GetBuffer();
            uint proto_id = BitConverter.ToUInt32(stream_buffer, 0);

            stream.Seek(NetworkDefine.ProtoIndexLength, SeekOrigin.Begin);

            IProtoPacker message;
            try
            {
                message = Context.Game.GetComponent<ProtoComponent>().GetInstance(proto_id);
                BinaryReader reader = new BinaryReader(stream);
                message.UnPack(reader);
            }
            catch (Exception e)
            {
                Log.Error($"proto_id: {proto_id} {this.Network.Count} {e} ");
                this.Error = ErrorCode.ERR_PacketParserError;
                Network.Remove(Id);
                return;
            }

            if (!(message is IResponse response))
            {
                Network.Dispatch(this, proto_id, message);
                return;
            }

            if (m_request_dict.TryGetValue(response.RpcId, out Action<IResponse> action) == false)
            {
                throw new Exception($"not found rpc, response message: {response.ToJson()}");
            }

            m_request_dict.Remove(response.RpcId);

            action?.Invoke(response);
        }

        public ETTask<IResponse> CallWithoutException(IRequest request)
        {
            int rpc_id = ++RpcId;
            var tcs = new ETTaskCompletionSource<IResponse>();

            m_request_dict[rpc_id] = (response) =>
            {
                try
                {
                    if(response is ErrorResponse error_response)
                    {
                        tcs.SetException(new Exception($"session close, errorcode: {error_response.Error} {error_response.Message}"));
                        return;
                    }
                    tcs.SetResult(response);
                }
                catch (Exception e)
                {
                    tcs.SetException(new Exception($"Rpc Error:{request.GetType().FullName}", e));
                }
            };

            request.RpcId = rpc_id;
            Send(request);
            return tcs.Task;
        }

        public ETTask<IResponse> CallWithoutException(IRequest request,CancellationToken cancellationToken)
        {
            int rpc_id = ++RpcId;
            var tcs = new ETTaskCompletionSource<IResponse>();

            m_request_dict[rpc_id] = (response) =>
            {
                try
                {
                    if (response is ErrorResponse error_response)
                    {
                        tcs.SetException(new Exception($"session close, errorcode: {error_response.Error} {error_response.Message}"));
                        return;
                    }
                    tcs.SetResult(response);
                }
                catch (Exception e)
                {
                    tcs.SetException(new Exception($"Rpc Error:{request.GetType().FullName}", e));
                }
            };

            cancellationToken.Register(() => m_request_dict.Remove(rpc_id));

            request.RpcId = rpc_id;
            Send(request);
            return tcs.Task;
        }


        public ETTask<IResponse> Call(IRequest request)
        {
            int rpc_id = ++RpcId;

            var tcs = new ETTaskCompletionSource<IResponse>();

            m_request_dict[rpc_id] = (response) =>
              {
                  try
                  {
                      //if (ErrorCode.IsRpcNeedThrowException(response.Error))
                      //{
                      //    throw new RpcException(response.Error, response.Message);
                      //}
                      tcs.SetResult(response);
                  }
                  catch (Exception e)
                  {
                      tcs.SetException(new Exception($"Rpc Error:{request.GetType().FullName}", e));
                  }
              };

            request.RpcId = rpc_id;
            Send(request);
            return tcs.Task;
        }

        public ETTask<IResponse> Call(IRequest request, CancellationToken cancellationToken)
        {
            int rpc_id = ++RpcId;

            var tcs = new ETTaskCompletionSource<IResponse>();

            m_request_dict[rpc_id] = (response) =>
            {
                //if (ErrorCode.IsRpcNeedThrowException(response.Error))
                //{
                //    tcs.SetException(new Exception($"Rpc Error: {request.GetType().FullName} {response.Error}"));
                //}

                tcs.SetResult(response);
            };

            cancellationToken.Register(() => m_request_dict.Remove(rpc_id));

            request.RpcId = rpc_id;
            this.Send(request);
            return tcs.Task;
        }

        public void Reply(IResponse response)
        {
            if (IsDisposed == true)
            {
                throw new Exception("session已经被Dispose了");
            }
            Send(response);
        }
    }
}
