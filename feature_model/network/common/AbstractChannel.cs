﻿using System;
using System.IO;
using System.Net;

namespace ETT.Model.network
{

    public enum EChannelType
    {
        Connect,
        Accept,
    }

    public abstract class AbstractChannel : ComponentWithId, IAbstractChannel
    {
        public EChannelType ChannelType { get; private set; }

        public AbstractService Service { get; private set; }

        public virtual MemoryStream Stream { get; protected set; }

        public int Error { get; set; }

        public IPEndPoint RemoteAddress { get; protected set; }

        public event Action<AbstractChannel, int> ErrorCallback;

        public event Action<MemoryStream> ReadCallback;

        protected void OnRead(MemoryStream stream)
        {
            ReadCallback?.Invoke(stream);
        }

        protected void OnError(int e)
        {
            Error = e;
            ErrorCallback?.Invoke(this, e);
        }

        protected void Awake(AbstractService service, EChannelType channelType)
        {
            Service = service;
            ChannelType = channelType;
        }

        public abstract void Start();

        public abstract void Send(MemoryStream stream);

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();
            Service?.Remove(Id);
        }
    }
}
