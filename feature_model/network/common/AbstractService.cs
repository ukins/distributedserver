﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace ETT.Model.network
{
    public abstract class AbstractService : Component
    {
        public abstract AbstractChannel GetChannel(long id);

        public event Action<AbstractChannel> AcceptCallback;

        public string Name { get; set; }

        protected void OnAccept(AbstractChannel channel)
        {
            AcceptCallback?.Invoke(channel);
        }

        public abstract AbstractChannel ConnectChannel(IPEndPoint iPEndPoint);

        public abstract AbstractChannel ConnectChannel(string address);

        public abstract void Remove(long channelId);

        public abstract void Update();

    }
}
