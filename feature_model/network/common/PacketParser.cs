﻿using Microsoft.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model.network
{
    public enum EParserState
    {
        PacketHead,
        PacketBody
    }

    public class PacketParser
    {
        private readonly CircularBuffer buffer;
        private EParserState state;
        private bool isOK;
        private int packetSize;
        private MemoryStream stream;

        public MemoryStream GetPacket()
        {
            isOK = false;
            return stream;
        }

        public PacketParser(CircularBuffer buffer, MemoryStream stream)
        {
            this.buffer = buffer;
            this.stream = stream;
        }

        public bool Parse()
        {
            if (isOK == true)
            {
                return true;
            }

            bool finish = false;

            while (finish == false)
            {
                switch (state)
                {
                    case EParserState.PacketHead:
                        {
                            if (buffer.Length < NetworkDefine.PacketSizeLength)
                            {
                                finish = true;
                            }
                            else
                            {
                                ParseHead();
                            }
                        }
                        break;
                    case EParserState.PacketBody:
                        {
                            if (buffer.Length < NetworkDefine.PacketSizeLength)
                            {
                                finish = true;
                            }
                            else
                            {
                                ParseBody();
                                finish = true;
                            }
                        }
                        break;

                }
            }
            return isOK;
        }

        private void ParseHead()
        {
            var stream_buffer = ((RecyclableMemoryStream)stream).GetBuffer();
            buffer.Read(stream_buffer, 0, NetworkDefine.PacketSizeLength);

            packetSize = BitConverter.ToInt32(stream_buffer, 0);
            if (this.packetSize > ushort.MaxValue || this.packetSize < 2)
            {
                throw new Exception($"recv packet size error: {this.packetSize}");
            }
            this.state = EParserState.PacketBody;
        }

        private void ParseBody()
        {
            stream.Seek(0, SeekOrigin.Begin);
            stream.SetLength(packetSize);
            var stream_buffer = ((RecyclableMemoryStream)stream).GetBuffer();
            buffer.Read(stream_buffer, 0, packetSize);
            isOK = true;
            state = EParserState.PacketHead;
        }


    }
}
