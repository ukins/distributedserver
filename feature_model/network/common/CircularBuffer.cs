﻿using System;
using System.Collections.Generic;
using System.IO;

namespace ETT.Model.network
{
    public class CircularBuffer //: Stream
    {
        public int ChunkSize = 8192;

        private readonly Queue<byte[]> bufferQueue = new Queue<byte[]>();
        private readonly Queue<byte[]> bufferCache = new Queue<byte[]>();

        public int LastIndex { get; set; }

        public int FirstIndex { get; set; }

        private byte[] lastBuffer;

        public CircularBuffer()
        {
            this.AddLast();
        }

        public long Length
        {
            get
            {
                int n = 0;
                if (bufferQueue.Count == 0)
                {
                    n = 0;
                }
                else
                {
                    n = (bufferQueue.Count - 1) * ChunkSize + LastIndex - FirstIndex;
                }
                if (n < 0)
                {
                    //Log.Error("CircularBuffer count < 0: {0}, {1}, {2}".Fmt(this.bufferQueue.Count, this.LastIndex, this.FirstIndex));
                    Log.Error($"CircularBuffer count < 0: {bufferQueue.Count}, {LastIndex}, {FirstIndex}");
                }
                return n;
            }
        }
        public void AddLast()
        {
            byte[] buffer;
            if (bufferCache.Count > 0)
            {
                buffer = bufferCache.Dequeue();
            }
            else
            {
                buffer = new byte[ChunkSize];
            }
            bufferQueue.Enqueue(buffer);
            lastBuffer = buffer;
        }

        public void RemoveFirst()
        {
            bufferCache.Enqueue(bufferQueue.Dequeue());
        }

        public byte[] First
        {
            get
            {
                if (bufferQueue.Count == 0)
                {
                    AddLast();
                }
                return bufferQueue.Peek();
            }
        }

        public byte[] Last
        {
            get
            {
                if (bufferQueue.Count == 0)
                {
                    AddLast();
                }
                return lastBuffer;
            }
        }

        //public async ETTask ReadAsync(Stream stream)
        //{
        //    long buff_length = Length;
        //    int send_size = ChunkSize - FirstIndex;
        //    if (send_size > buff_length)
        //    {
        //        send_size = (int)buff_length;
        //    }

        //    await stream.WriteAsync(First, FirstIndex, send_size);

        //    FirstIndex += send_size;
        //    if (FirstIndex == ChunkSize)
        //    {
        //        FirstIndex = 0;
        //        RemoveFirst();
        //    }
        //}

        //public void Read(Stream stream, int count)
        //{
        //    if (count > Length)
        //    {
        //        throw new System.Exception($"bufferList length < count, {Length} {count}");
        //    }

        //    int alreadyCopyCount = 0;
        //    while (alreadyCopyCount < count)
        //    {
        //        int n = count - alreadyCopyCount;
        //        if (ChunkSize - FirstIndex > n)
        //        {
        //            stream.Write(First, FirstIndex, n);
        //            FirstIndex += n;
        //            alreadyCopyCount += n;
        //        }
        //        else
        //        {
        //            stream.Write(First, FirstIndex, ChunkSize - FirstIndex);
        //            alreadyCopyCount += ChunkSize - FirstIndex;
        //            FirstIndex = 0;
        //            RemoveFirst();
        //        }
        //    }
        //}

        public void Write(Stream stream)
        {
            int count = (int)(stream.Length - stream.Position);

            int alreadyCopyCount = 0;
            while (alreadyCopyCount < count)
            {
                if (LastIndex == ChunkSize)
                {
                    AddLast();
                    LastIndex = 0;
                }

                int n = count - alreadyCopyCount;
                if (ChunkSize - LastIndex > n)
                {
                    stream.Read(lastBuffer, LastIndex, n);
                    LastIndex += count - alreadyCopyCount;
                    alreadyCopyCount += n;
                }
                else
                {
                    stream.Read(lastBuffer, LastIndex, ChunkSize - LastIndex);
                    alreadyCopyCount += ChunkSize - LastIndex;
                    LastIndex = ChunkSize;
                }
            }
        }

        //public async ETTask<int> WriteAsync(Stream stream)
        //{
        //    int size = ChunkSize - LastIndex;
        //    int n = await stream.ReadAsync(Last, LastIndex, size);

        //    if (n == 0)
        //    {
        //        return 0;
        //    }

        //    LastIndex += n;

        //    if (LastIndex == ChunkSize)
        //    {
        //        AddLast();
        //        LastIndex = 0;
        //    }
        //    return n;
        //}

        public int Read(byte[] buffer, int offset, int count)
        {
            if (buffer.Length < offset + count)
            {
                throw new System.Exception($"bufferList length < coutn, buffer length: {buffer.Length} {offset} {count}");
            }

            if (Length < count)
            {
                count = (int)Length;
            }

            int alreadyCopyCount = 0;
            while (alreadyCopyCount < count)
            {
                int n = count - alreadyCopyCount;
                if (ChunkSize - FirstIndex > n)
                {
                    Array.Copy(First, FirstIndex, buffer, alreadyCopyCount + offset, n);
                    FirstIndex += n;
                    alreadyCopyCount += n;
                }
                else
                {
                    Array.Copy(First, FirstIndex, buffer, alreadyCopyCount + offset, ChunkSize - FirstIndex);
                    alreadyCopyCount += ChunkSize - FirstIndex;
                    FirstIndex = 0;
                    RemoveFirst();
                }
            }

            return count;
        }

        public void Write(byte[] buffer, int offset, int count)
        {
            int alreadyCopyCount = 0;
            while (alreadyCopyCount < count)
            {
                if (LastIndex == ChunkSize)
                {
                    AddLast();
                    LastIndex = 0;
                }

                int n = count - alreadyCopyCount;
                if (ChunkSize - LastIndex > n)
                {
                    Array.Copy(buffer, alreadyCopyCount + offset, lastBuffer, LastIndex, n);
                    LastIndex += count - alreadyCopyCount;
                    alreadyCopyCount += n;
                }
                else
                {
                    Array.Copy(buffer, alreadyCopyCount + offset, lastBuffer, LastIndex, ChunkSize - LastIndex);
                    alreadyCopyCount += ChunkSize - LastIndex;
                    LastIndex = ChunkSize;
                }
            }
        }
    }
}
