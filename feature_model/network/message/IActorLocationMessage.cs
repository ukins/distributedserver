﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IActorLocationMessage : IActorRequest
    {
    }

    public interface IActorLocationRequest : IActorRequest
    {

    }

    public interface IActorLocationResponse : IActorResponse
    {

    }
}
