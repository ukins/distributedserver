﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public interface IActorMessage : IMessage
    {
        long ActorId { get; set; }
    }

    public interface IActorRequest : IRequest
    {
        long ActorId { get; set; }
    }

    public interface IActorResponse : IResponse
    {
    }
}
