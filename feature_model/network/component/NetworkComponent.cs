﻿using System.Collections.Generic;

namespace ETT.Model.network
{
    public abstract class NetworkComponent : Component, IUpdate
    {
        protected AbstractService Service { get; set; }
        public ENetworkProtocol EProtocol { get; protected set; }

        protected readonly Dictionary<long, Session> m_session_dict = new Dictionary<long, Session>();

        //public IMessageDispatcher MessageDispatcher { get; set; }

        public int Count => m_session_dict.Count;

        public void Update()
        {
            Service?.Update();
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();

            foreach (var pair in m_session_dict)
            {
                pair.Value.DisposeWithoutRemove();
            }
            m_session_dict.Clear();

            Service?.Dispose();
            Service = null;
        }

        public virtual void Remove(long id)
        {
            if (m_session_dict.TryGetValue(id, out Session session) == false)
            {
                return;
            }
            m_session_dict.Remove(id);
            session.Dispose();
        }

        public Session Get(long id)
        {
            m_session_dict.TryGetValue(id, out Session session);
            return session;
        }

        public abstract void Dispatch(Session session, uint proto_id, IProtoPacker message);

        public void SetServiceName(string name)
        {
            if (Service != null)
            {
                Service.Name = name;
                Log.Debug(name);
            }
        }
    }
}
