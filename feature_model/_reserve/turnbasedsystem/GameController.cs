﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UKon.turnbasedsystem
{
    public abstract class GameController
    {
        protected List<GamePlayer> m_players = new List<GamePlayer>();
        public void AddPlayer(GamePlayer player)
        {
            m_players.Add(player);
        }
    }
}
