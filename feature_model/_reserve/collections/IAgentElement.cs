﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UKon.Collections
{
    public interface IAgentElement<K>
    {
        K Key { get; }
    }
}
