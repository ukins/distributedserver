﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace UKon.Collections
{
    interface IDictionaryAgent<K, T>
    {
        T this[K key] { get; }

        int Count { get; }

        void Add(T value);

        T Remove(K key);
    }
}
