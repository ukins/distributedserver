﻿namespace UKon.Shell
{
    public abstract class ShellType
    {
        public object Value => InnerValue;

        protected virtual object InnerValue => Value;
    }

    public class ShellType<T> : ShellType
    {
        private T m_value;

        public ShellType(T value) => m_value = value;

        protected override object InnerValue => Value;
        public new T Value => m_value;

        public override string ToString()
        {
            return m_value.ToString();
        }
    }
}
