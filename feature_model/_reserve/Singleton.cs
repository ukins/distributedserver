﻿#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：Singleton
// 文件功能描述：泛型单例
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 21:49:40
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon
{
    public class Singleton<T> where T : class, new()
    {
        private static T m_instance;
        private static readonly object syslock = new object();

        public static T Instance
        {
            get
            {
                if (m_instance == null)
                {
                    lock (syslock)
                    {
                        if (m_instance == null)
                        {
                            m_instance = new T();
                        }
                    }
                }
                return m_instance;
            }
        }
    }
}
