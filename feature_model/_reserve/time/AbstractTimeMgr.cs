﻿using System;
using System.Collections.Generic;
using UKon.Timer;

#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：TimeMgr
// 文件功能描述：实现帧循环功能
// 后续增加延时执行功能
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/10/24 21:56:59
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon
{

    public delegate void TimeEventHandler(bool iscomplete);

    /// <summary>
    /// 帧循环时间管理器
    /// </summary>
    public class AbstractTimeMgr //: Singleton<AbstractTimeMgr>
    {
        //每帧时间间隔，20毫秒
        public virtual int FrameTime { get { return 20; } }

        protected long m_currServerTime;
        protected long m_lastServerTime;
        protected long m_baseServerTime;
        protected uint m_tickCnt;
        protected bool m_isRunning;
        //private DateTimeOffset m_base_time;
        protected long m_deltaTime;

        /// <summary>
        /// 服务器时间戳，以毫秒为单位
        /// </summary>
        public long CurrTimeMS
        {
            get => m_currServerTime;
        }

        /// 服务器时间戳，以秒为单位
        public int CurrTimeS
        {
            get => (int)(m_currServerTime * 0.001);
        }

        public DateTime CurrTime
        {
            get => Convert.ToDateTime(m_currServerTime);
        }

        public bool IsRunning
        {
            get => m_isRunning;
        }
        public long DeltaTime
        {
            get => m_deltaTime;
        }

        public void Start(long currtime)
        {
            m_isRunning = true;
            m_tickCnt = 0;

            m_baseServerTime = currtime;
            m_lastServerTime = currtime;

        }

        public void Stop()
        {
            m_isRunning = false;
        }


        protected HashSet<AbstractTimer> m_available_tickers = new HashSet<AbstractTimer>();

        /// <summary>
        /// 添加到tick执行队列，以FrameTime所定的频率执行Tick方法
        /// </summary>
        /// <param name="timer"></param>
        public void AddTick(AbstractTimer timer)
        {
            if (m_available_tickers.Contains(timer) == false)
            {
                m_available_tickers.Add(timer);
            }
        }

        /// <summary>
        /// 从tick队列中移除
        /// </summary>
        /// <param name="timer"></param>
        public void RemoveTick(AbstractTimer timer)
        {
            if (m_available_tickers.Contains(timer) == true)
            {
                m_available_tickers.Remove(timer);
            }
        }

        public virtual void Tick(long currtime)
        {
            if (m_isRunning == false)
            {
                return;
            }

            m_currServerTime = currtime;

            bool isexpired = false;

            //while循环用于处理掉帧的情况
            while (m_currServerTime - m_baseServerTime > m_tickCnt * FrameTime)
            {
                isexpired = true;
                m_tickCnt++;
                if (m_tickCnt == uint.MaxValue)
                {
                    m_baseServerTime = m_currServerTime;
                    m_tickCnt = 0;
                }
            }

            if (isexpired == true)
            {
                m_deltaTime = m_currServerTime - m_lastServerTime;
                foreach (var t in m_available_tickers)
                {
                    if (t.IsPause == false && t.IsComplete == false)
                    {
                        t.Tick();
                    }
                }
                m_lastServerTime = m_currServerTime;
            }

            //m_available_tickers.RemoveWhere(c => c.IsComplete == true);
        }

    }
}
