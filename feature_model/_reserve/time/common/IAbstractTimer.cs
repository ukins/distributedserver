﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：IAbstractTimer
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/10 20:45:38
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Timer
{
    public interface IAbstractTimer
    {
        bool IsPause { get; }

        bool IsComplete { get; }

        /// <summary>
        /// 开始
        /// </summary>
        void Start();

        /// <summary>
        /// 暂停
        /// </summary>
        void Pause();

        /// <summary>
        /// 循环执行的方法
        /// </summary>
        void Tick();

        /// <summary>
        /// 恢复
        /// </summary>
        void Resume();

        /// <summary>
        /// 结束
        /// </summary>
        void Stop();
    }
}
