﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：TimeDelayer
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/16 10:00:33
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Timer
{
    /// <summary>
    /// 延时定时器，以毫秒为单位
    /// </summary>
    public class DelayTicker : AbstractTimer
    {
        protected long m_currIntervalValue;

        public DelayTicker(TimeEventHandler handler, long interval)
        {
            m_handler = handler;
            m_intervalValue = interval;
        }

        public override void Start()
        {
            m_isPause = false;
            m_isComplete = false;
            m_startValue = m_timeMgr.CurrTimeMS;
            m_currIntervalValue = 0;
        }

        public override void Tick()
        {
            if (m_isComplete == true)
            {
                return;
            }
            m_currIntervalValue += m_timeMgr.DeltaTime;
            if (m_currIntervalValue > m_intervalValue)
            {
                m_handler?.Invoke(true);
                m_isComplete = true;
                m_currIntervalValue = 0;
            }
        }

        public override void Stop()
        {
            m_isComplete = true;
        }
    }
}
