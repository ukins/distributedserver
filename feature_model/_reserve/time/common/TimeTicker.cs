﻿namespace UKon.Timer
{
    /// <summary>
    /// 定时器，处理时间以毫秒为单位
    /// </summary>
    public class TimeTicker : AbstractTimer
    {
        protected int m_currLoopCnt;
        protected long m_currIntervalValue;
        protected bool m_isimmediate;

        public TimeTicker(TimeEventHandler handler, long interval, int loopCnt = 0, bool isimmediate = true)
        {
            m_handler = handler;
            m_intervalValue = interval;
            m_loopCnt = loopCnt;
            m_isimmediate = isimmediate;
        }

        public override void Start()
        {
            m_isPause = false;
            m_isComplete = false;
            m_startValue = m_timeMgr.CurrTimeMS;
            m_currLoopCnt = 0;
            m_currIntervalValue = 0;
            if (m_isimmediate == true)
            {
                m_handler?.Invoke(false);
            }
        }

        public override void Tick()
        {
            if (m_isComplete == true)
            {
                return;
            }
            m_currIntervalValue += m_timeMgr.DeltaTime;
            if (m_currIntervalValue > m_intervalValue)
            {
                if (m_loopCnt == 0)
                {
                    m_handler?.Invoke(false);
                    m_currIntervalValue = 0;
                }
                else
                {
                    m_currLoopCnt++;
                    if (m_currLoopCnt < m_loopCnt)
                    {
                        m_handler?.Invoke(false);
                        m_currIntervalValue = 0;
                    }
                    else if (m_currLoopCnt == m_loopCnt)
                    {
                        m_handler?.Invoke(true);
                        m_isComplete = true;
                        m_currIntervalValue = 0;
                    }
                }
            }
        }

        public override void Stop()
        {
            m_isComplete = true;
        }
    }
}
