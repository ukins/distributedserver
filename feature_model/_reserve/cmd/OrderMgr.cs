﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：OrderMgr
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2018/12/5 9:50:58
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace UKon.Cmd
{
    using OrderDict = Dictionary<string, Dictionary<string, AbstractOrder>>;

    public class OrderMgr
    {
        private OrderDict m_dict = new OrderDict();

        public void Register(AbstractOrder order)
        {
            if (m_dict.ContainsKey(order.Cmd) == false)
            {
                m_dict.Add(order.Cmd, new Dictionary<string, AbstractOrder>());
            }
            var optionDict = m_dict[order.Cmd];
            foreach (var option in order.Option)
            {
                if (optionDict.ContainsKey(option) == false)
                {
                    optionDict.Add(option, order);
                }
                else
                {
                    Log.Error(string.Format("Order.Option Duplicated Cmd={0},Option={1}", order.Cmd, option));
                }
            }
        }

        public void UnRegister(AbstractOrder order)
        {
            if (m_dict.ContainsKey(order.Cmd) == false)
            {
                return;
            }
            var optionDict = m_dict[order.Cmd];
            foreach (var option in order.Option)
            {
                if (optionDict.ContainsKey(option) == true)
                {
                    optionDict.Remove(option);
                }
            }
        }

        public void Process(string cmd)
        {
            string orderStr = string.Empty;
            string optionStr = string.Empty;
            string paramStr = string.Empty;

            cmd = cmd.Trim();
            int idx = cmd.IndexOf(' ');
            orderStr = cmd.Substring(0, idx);

            cmd = cmd.Substring(idx).Trim();
            idx = cmd.IndexOf(' ');
            if (idx > 0)
            {
                optionStr = cmd.Substring(0, idx);

                paramStr = cmd.Substring(idx).Trim();
            }
            else
            {
                optionStr = cmd;
            }

            //Log.LogMgr.Instance.Log(string.Format("Order = {0}", order));
            //Log.LogMgr.Instance.Log(string.Format("Option = {0}", option));
            //Log.LogMgr.Instance.Log(string.Format("Param = {0}", param));

            if (m_dict.ContainsKey(orderStr) == false)
            {
                Log.Error(string.Format("Cmd is not exist. cmd={0}", orderStr));
                return;
            }

            var order = m_dict[orderStr];
            bool result = false;
            if (order.ContainsKey(optionStr) == true)
            {
                order[optionStr].Execute(new string[] { paramStr });
                result = true;
            }
            else
            {
                if (order.ContainsKey(string.Empty) == true)
                {
                    order[string.Empty].Execute(new string[] { optionStr });
                    result = true;
                }
            }
            if (result == false)
            {
                Log.Error("Cmd Invalid");
            }

        }
    }
}
