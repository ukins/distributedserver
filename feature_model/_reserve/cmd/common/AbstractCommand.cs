﻿using ETT.Model;
using System.Collections.Generic;

namespace UKon.Cmd
{
    public abstract class AbstractCommand //: IRegisterator<string>
    {
        public abstract string Id { get; }

        protected List<AbstractCmdOption> m_options = new List<AbstractCmdOption>();

        protected AbstractCmdOption DefaultOption;
        public bool IsDefaultOption(AbstractCmdOption option)
        {
            return DefaultOption == option;
        }

        protected bool AddCmdOption(AbstractCmdOption option)
        {
            if (m_options.Exists(c => c.type == option.type) == false)
            {
                m_options.Add(option);
                return true;
            }
            Log.Error("Error! CmdOption 重复注册");
            return false;
        }

        protected AbstractCmdOption FindOption(string name)
        {
            for (int i = 0; i < m_options.Count; ++i)
            {
                for (int j = 0; j < m_options[i].names.Length; ++j)
                {
                    if (m_options[i].names[j] == name)
                    {
                        return m_options[i];
                    }
                }
            }
            return null;
        }

        protected bool ExistOption(string name)
        {
            for (int i = 0; i < m_options.Count; ++i)
            {
                for (int j = 0; j < m_options[i].names.Length; ++j)
                {
                    if (m_options[i].names[j] == name)
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        protected AbstractCmdOption m_validoption;
        public virtual bool Process(string[] args)
        {
            if (args.Length == 1)
            {
                m_validoption = DefaultOption;
                m_validoption.Process(args);
                return true;
            }
            m_validoption = FindOption(args[1]);
            if (m_validoption == null)
            {
                m_validoption = DefaultOption;
            }
            if (m_validoption == null)
            {
                bool result = ExistOption(args[1].ToLower());
                if (result == true)
                {
                    Log.Warning("cmd option is case sensitive!");
                    return false;
                }
                //LogMgr.Instance.LogError("Please input valid option!");
                return false;
            }

            bool matchresult = false;
            matchresult = m_validoption.Match(args);

            if (matchresult == true)
            {
                m_validoption.Process(args);
            }

            return true;
        }
    }
}
