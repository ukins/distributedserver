﻿using ETT.Model.network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model.proto
{
    public abstract class AbstractMessage : AbstractProtoPacker, IMessage
    {

        public override void Pack(BinaryWriter writer)
        {
        }

        public override void UnPack(BinaryReader reader)
        {
        }

    }

    public abstract class AbstractRequest : AbstractProtoPacker, IRequest
    {
        public int RpcId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(RpcId);
        }

        public override void UnPack(BinaryReader reader)
        {
            RpcId = reader.ReadInt32();
        }

    }

    public abstract class AbstractResponse : AbstractProtoPacker, IResponse
    {
        public int Error { get; set; }
        public string Message { get; set; } = string.Empty;
        public int RpcId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(Error);
            writer.Write(Message);
            writer.Write(RpcId);
        }

        public override void UnPack(BinaryReader reader)
        {
            Error = reader.ReadInt32();
            Message = reader.ReadString();
            RpcId = reader.ReadInt32();
        }
    }


    public class ErrorResponse : AbstractResponse
    {

    }
}
