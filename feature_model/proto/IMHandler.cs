﻿using ETT.Model.network;
using System;

namespace ETT.Model.proto
{
    public interface IMHandler
    {
        ETVoid Handle(Session session, IProtoPacker message);
        Type GetMessageType();
    }
}
