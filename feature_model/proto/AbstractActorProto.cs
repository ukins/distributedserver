﻿using ETT.Model.network;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model.proto
{
    public abstract class AbstractLocationMessage : AbstractProtoPacker, IActorLocationMessage
    {
        public int RpcId { get; set; }
        public long ActorId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(RpcId);
            writer.Write(ActorId);
        }

        public override void UnPack(BinaryReader reader)
        {
            RpcId = reader.ReadInt32();
            ActorId = reader.ReadInt64();
        }
    }

    public abstract class AbstractLocationRequest : AbstractProtoPacker, IActorLocationRequest
    {
        public int RpcId { get; set; }
        public long ActorId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(RpcId);
            writer.Write(ActorId);
        }

        public override void UnPack(BinaryReader reader)
        {
            RpcId = reader.ReadInt32();
            ActorId = reader.ReadInt64();
        }

    }

    public abstract class AbstractLocationResponse : AbstractProtoPacker, IActorLocationResponse
    {
        public int Error { get; set; }
        public string Message { get; set; } = string.Empty;
        public int RpcId { get; set; }

        public override void Pack(BinaryWriter writer)
        {
            writer.Write(Error);
            writer.Write(Message);
            writer.Write(RpcId);
        }

        public override void UnPack(BinaryReader reader)
        {
            Error = reader.ReadInt32();
            Message = reader.ReadString();
            RpcId = reader.ReadInt32();
        }
    }
}
