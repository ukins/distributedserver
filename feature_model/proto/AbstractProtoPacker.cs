﻿using ETT.Model.network;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace ETT.Model.proto
{
    public abstract class AbstractProtoPacker : IProtoPacker
    {
        //待优化

        public AbstractProtoPacker() { }

        public abstract void Pack(BinaryWriter writer);

        public abstract void UnPack(BinaryReader reader);

        public virtual string ToJson()
        {
            return JsonConvert.SerializeObject(this);
        }

    }
}
