﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ETT.Model
{
    public class LockQueue : Component
    {
        private readonly Queue<ETTaskCompletionSource<CoroutineLock>> queue = new Queue<ETTaskCompletionSource<CoroutineLock>>();

        public override void Dispose()
        {
            if (this.IsDisposed)
            {
                return;
            }

            base.Dispose();

            this.queue.Clear();
        }

        public void Enqueue(ETTaskCompletionSource<CoroutineLock> tcs)
        {
            this.queue.Enqueue(tcs);
        }

        public ETTaskCompletionSource<CoroutineLock> Dequeue()
        {
            return this.queue.Dequeue();
        }

        public int Count => queue.Count;
    }
}
