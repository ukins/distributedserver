﻿Shader "CelStyle/HighQuality-Standard-Mod" {
    Properties {
        _Alpha ("_Alpha", range(0,1)) = 1
        _EdgeSize("边界宽度",range(0, 1)) = 0.4  
        _EdgeColorRatio("自动边界颜色比例",range(0, 1)) = 0  
        _EdgeColor("边界颜色",Color)=(0,0,0,1)

		_InnerLineColor("内线颜色",Color) = (0,0,0,1)
		_InnerLineThreshold("内线阈值", range(0,1)) = 0.5

        _MainTex ("漫反射贴图", 2D) = "white" {}  
        _LitTex("明暗贴图", 2D) = "white" {}
        _SSSTex("阴影贴图（SSS）", 2D) = "white" {}

        _Shininess ("发光值", range(0,20)) = 1.05
        _ShadowThreshold ("阴影阈值", range(0,1)) = 0.5
		_ShadowThreshold2("阴影阈值2", range(0,1)) = 0.3

        _SpecularColor ("高光颜色", Color) = (0,0,0,0.5)
        _SpecularPower ("高光强度", range(0.1,2)) = 1.05
        _SpecularRange ("高光范围调节", range(0.1, 1)) = 0
        _SpecularBrightness ("高光亮度调节", range(0.5, 2)) = 0.5
    }  
    SubShader {  
        Tags {
            "RenderType" = "Opaque" 
            "Queue" = "Geometry"
        }
        pass{  
            Name "STAND_OUTLINE"
            Cull Front  
            ZWrite On  
            CGPROGRAM  
            #pragma vertex vert  
            #pragma fragment frag  
            #include "CelStyle-Edge-HighQuality.cginc"  
            ENDCG  
        }//end of pass  
       
        Pass {
            Tags {
                "LightMode"="ForwardBase"
            }  
            Cull Back 

            Name "HIGHQUALITY_STARDARD_PASS"  

            CGPROGRAM  
            #pragma vertex vert  
            #pragma fragment frag  
            #pragma multi_compile_fwdbase  

            #define DEFINE_CELSTYLE_STAND_PARAMS
            #define DEFINE_CELSTYLE_SHADOW_PARAMS
            #define DEFINE_CELSTYLE_SPECULAR_LIGHT

            #include "CelStyle-Lighting.cginc"

            struct v2f {  
                DEFINE_STAND_LIGNT_V2F
            };  

            v2f vert (appdata_full v) {  
                v2f o;  
                HANDLE_STAND_LIGHT_V2F_VERT
                return o;  
            }

            sampler2D _SSSTex;
			fixed4 _InnerLineColor;
			float _InnerLineThreshold;
			float _ShadowThreshold2;

            float4 frag(v2f i) : COLOR  
            {  
                HANDLE_STAND_LIGHT_V2F_FRAG
                #include "CelStyle-HighQuality-Standard-Code-Mod.cginc"
                return finalColor;
            }
            ENDCG
        }
    }   
    FallBack "Diffuse"  
}
