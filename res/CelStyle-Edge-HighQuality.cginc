#include "UnityCG.cginc"  
#include "CelStyle-Edge.cginc"

struct a2v {
    float4 vertex : POSITION;
    float3 normal : NORMAL;
    float2 texcoord : TEXCOORD0;
    //float4 vertexColor : COLOR;
};

struct v2f {
    float4 pos : POSITION;
    float4 color : COLOR;
    float2 texcoord : TEXCOORD0;
};

sampler2D  _MainTex;
sampler2D _SSSTex;
float     _EdgeColorRatio;

v2f vert(a2v v) {
    v2f o;
    o.pos = UnityObjectToClipPos(v.vertex);
    float3 norm = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
    float2 offset = TransformViewToProjection(norm.xy);
    o.pos.xy += offset * _EdgeSize *EDGE_BASESIZE;
    o.texcoord = v.texcoord;
    o.color = _EdgeColor;
    return o;
}

float4 frag(v2f i):COLOR  
{  
    fixed4 cLight = tex2D(_MainTex, i.texcoord);
    clip(cLight.a - 0.1);
    fixed4 cSSS = tex2D(_SSSTex, i.texcoord);
    fixed4 cDark = cLight * cSSS;
    cDark = cDark *0.5f;// *cDark * cDark;
    float4 color = cDark * (1-_EdgeColorRatio) + i.color*_EdgeColorRatio;
    color.a = _Alpha;
    return color; 

}  

