﻿using ETT.Client;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Editor
{
    public class EditorFacade : Entitas, IRootComponent, IAbstractFacade
    {
        public int RootId { get; set; }

        public FactoryEntity Factory { get; }

        public EditorFacade()
        {
            Root = this;
            Factory = new FactoryEntity(this);
        }

        public void Init()
        {

        }

        public void Update()
        {

        }

        public void LateUpdate()
        {

        }

        public void Destroy()
        {
        }
    }
}
