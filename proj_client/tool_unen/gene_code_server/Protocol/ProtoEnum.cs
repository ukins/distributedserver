namespace ETT.Server
{
	/// <summary>
	/// generic协议枚举 Range[0,10000)
	/// <summary>
	public enum EGenericProto
	{
		C2C_Ntf_NtfConnectStart = 1000,
		C2C_Ntf_NtfConnectSucc = 1001,
		C2C_Ntf_NtfConnectFail = 1002,
		C2C_Ntf_NtfConnectClose = 1003,
	}

	/// <summary>
	/// login协议枚举 Range[10000,20000)
	/// <summary>
	public enum ELoginProto
	{
		C2L_Req_Login = 10000,
		L2C_Res_Login = 10001,
	}

	/// <summary>
	/// lobby协议枚举 Range[20000,30000)
	/// <summary>
	public enum ELobbyProto
	{
		C2G_Req_ReqGetRoomList = 20000,
		G2C_Res_ResGetRoomList = 20001,
		C2G_Req_ReqGetRoomVO = 20010,
		G2C_Res_ResGetRoomVO = 20011,
		C2G__ReqCreate = 20020,
		G2C__ResCreate = 20021,
		C2G_Req_ReqQuitRoom = 20025,
		G2C_Res_ResQuitRoom = 20026,
		C2G_Req_ReqJoin = 20030,
		G2C_Res_ResJoin = 20031,
		G2C_Ntf_NtfJoin = 20032,
		C2G_Req_ReqDealJoin = 20040,
		G2C_Res_ResDealJoin = 20041,
		G2C_Ntf_NtfDealJoin = 20042,
		G2C_Ntf_NtfStartMatch = 20051,
	}

	/// <summary>
	/// chinesechess协议枚举 Range[30000,40000)
	/// <summary>
	public enum EChinesechessProto
	{
		C2G_Req_ReqSelect = 30000,
		G2C_Res_ResSelect = 30001,
		G2C_Ntf_NtfSelect = 30002,
		C2G_Req_ReqMove = 30010,
		G2C_Res_ResMove = 30011,
		G2C_Ntf_NtfMove = 30012,
		C2G_Req_ReqKill = 30020,
		G2C_Res_ResKill = 30021,
		G2C_Ntf_NtfKill = 30022,
		G2C_Ntf_NtfTurn = 30030,
		G2C_Ntf_NtfKickOut = 30040,
		G2C_Ntf_NtfGameOver = 30050,
	}

}
