using ETT.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Text;
using UKon;
using UKon.Config;
using UKon.Log;

namespace ETT.Server.Table
{
	public class AwardTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => ID;

		public uint ID { get; private set; }

		private List<ItemOffset> m_reward;
		public List<ItemOffset> Reward { get { return m_reward; } }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				ID = br.ReadUInt32();

				int m_reward_cnt = br.ReadInt32();
				m_reward = new List<ItemOffset>();
				for (int i = 0; i < m_reward_cnt; ++i)
				{
					m_reward.Add(new ItemOffset());
					m_reward[i].Deserialize(br);
				}
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class AwardTable : SingleKeyTable<AwardTableConf,uint>
	{
		public override string Name { get { return "AwardTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"flobby/table/award",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='AwardTable' >
						<field name='ID' type='uint' primarykey='true' desc='奖励id' />
						<field name='Reward' type='list(ItemOffset)' isvertical='false' >
							<field name='ItemID' type='uint' foreigntable='ItemTable' desc='奖励id数组' />
							<field name='ItemCnt' type='uint' desc='奖励个数数组' />
						</field>
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
