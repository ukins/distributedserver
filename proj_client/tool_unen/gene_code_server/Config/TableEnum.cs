using System;

namespace ETT.Server.Table
{
	public enum ESortingLayer
	{
		// 无效值
		Invalid = 0,
		// 战斗UI层
		Battle = 1,
		// 战斗UI提示层
		BattleTips = 2,
		// 普通界面层
		Normal = 3,
		// 对话框层
		Dialog = 4,
		// 普通提示层
		Tips = 5,
		// 新手引导层
		Guide = 6,
		// 加载层
		Loading = 7,
		// 上限值
		MAX = 8,
	}

	public enum EActor
	{
		// 无效值
		Invalid = 0,
		// 客户端
		Client = 1,
		// 登陆服务器
		Login = 2,
		// 网关服务器
		Gate = 3,
		// 数据库服务器
		DB = 4,
		// 世界库服务器
		World = 5,
		// 上限值
		MAX = 6,
	}

	public enum EProtoStyle
	{
		// 无效值
		Invalid = 0,
		// 请求
		Req = 1,
		// 回答
		Res = 2,
		// 通知
		Ntf = 3,
		// 上限值
		MAX = 4,
	}

	public enum ECacheType
	{
		// 不缓存
		Never = 0,
		// 缓存，依赖窗口出于现实状态则缓存，否则不缓存
		Normal = 1,
		// 始终缓存
		Always = 2,
		// 上限值
		MAX = 3,
	}

	public enum ENumericKind
	{
		// 数值
		Numeric = 0,
		// 百分比
		Percent = 1,
	}

}
