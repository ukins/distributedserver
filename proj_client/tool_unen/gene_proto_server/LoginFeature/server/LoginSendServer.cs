using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Login
{
	public class LoginSendServer
	{
		public static readonly LoginSendServer Instance = new LoginSendServer();
		private LoginSendServer() { }

		private NetMgr m_netMgr = NetMgr.Instance;

		/// <summary>
		/// 登陆协议
		/// </summary>
		/// <param name="name">用户名</param>
		/// <param name="pwd">密码</param>
		public bool ReqLogin(string name, string pwd)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELoginProto.REQ_LOGIN;
			package.Stream.Write(name);
			package.Stream.Write(pwd);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 创建协议
		/// </summary>
		/// <param name="name">用户名</param>
		/// <param name="pwd">密码</param>
		public bool ReqCreate(string name, string pwd)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELoginProto.REQ_CREATE;
			package.Stream.Write(name);
			package.Stream.Write(pwd);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求同步时间
		/// </summary>
		/// <param name=""></param>
		public bool ReqSync()
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELoginProto.REQ_SYNC;
			m_netMgr.Send(package);
			return true;
		}
	}
}
