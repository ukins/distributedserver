using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Login
{
	public class LoginRecvServer : AbstractRecvServer
	{

		/// <summary>
		/// 登陆协议返回
		/// </summary>
		/// <param name="returncode">返回结果0:成功，1:失败</param>
		/// <param name="playerid">玩家id</param>
		/// <param name="req_name">用户名</param>
		/// <param name="req_pwd">密码</param>
		[CustomProtocolID(protoid: (uint)ELoginProto.RES_LOGIN)]
		public bool ResLogin(uint returncode, uint playerid, string req_name, string req_pwd)
		{
			return true;
		}

		/// <summary>
		/// 登陆协议返回
		/// </summary>
		/// <param name="returncode">返回结果0:成功，1:失败</param>
		/// <param name="playerid">玩家id</param>
		[CustomProtocolID(protoid: (uint)ELoginProto.RES_CREATE)]
		public bool ResCreate(uint returncode, uint playerid)
		{
			return true;
		}

		/// <summary>
		/// 返回同步时间
		/// </summary>
		/// <param name="synctime">服务器时间(秒)</param>
		[CustomProtocolID(protoid: (uint)ELoginProto.RES_SYNC)]
		public bool ResSync(long synctime)
		{
			return true;
		}
	}
}
