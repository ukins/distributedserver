using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Generic
{
	public class GenericRecvServer : AbstractRecvServer
	{

		/// <summary>
		/// 连接开始
		/// </summary>
		/// <param name="desc">描述</param>
		[CustomProtocolID(protoid: (uint)EGenericProto.NTF_CONNECT_START)]
		public bool NtfConnectStart(string desc)
		{
			return true;
		}

		/// <summary>
		/// 连接成功
		/// </summary>
		/// <param name="desc">描述</param>
		[CustomProtocolID(protoid: (uint)EGenericProto.NTF_CONNECT_SUCC)]
		public bool NtfConnectSucc(string desc)
		{
			return true;
		}

		/// <summary>
		/// 连接失败
		/// </summary>
		/// <param name="desc">描述</param>
		[CustomProtocolID(protoid: (uint)EGenericProto.NTF_CONNECT_FAIL)]
		public bool NtfConnectFail(string desc)
		{
			return true;
		}

		/// <summary>
		/// 连接断开
		/// </summary>
		/// <param name="desc">描述</param>
		[CustomProtocolID(protoid: (uint)EGenericProto.NTF_CONNECT_CLOSE)]
		public bool NtfConnectClose(string desc)
		{
			return true;
		}
	}
}
