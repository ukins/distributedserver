using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Generic
{
	public class GenericSendServer
	{
		public static readonly GenericSendServer Instance = new GenericSendServer();
		private GenericSendServer() { }

		private NetMgr m_netMgr = NetMgr.Instance;

		/// <summary>
		/// 连接开始
		/// </summary>
		/// <param name="desc">描述</param>
		public bool NtfConnectStart(string desc)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EGenericProto.NTF_CONNECT_START;
			package.Stream.Write(desc);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 连接成功
		/// </summary>
		/// <param name="desc">描述</param>
		public bool NtfConnectSucc(string desc)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EGenericProto.NTF_CONNECT_SUCC;
			package.Stream.Write(desc);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 连接失败
		/// </summary>
		/// <param name="desc">描述</param>
		public bool NtfConnectFail(string desc)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EGenericProto.NTF_CONNECT_FAIL;
			package.Stream.Write(desc);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 连接断开
		/// </summary>
		/// <param name="desc">描述</param>
		public bool NtfConnectClose(string desc)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)EGenericProto.NTF_CONNECT_CLOSE;
			package.Stream.Write(desc);
			m_netMgr.Send(package);
			return true;
		}
	}
}
