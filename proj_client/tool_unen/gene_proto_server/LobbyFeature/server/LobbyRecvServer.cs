using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Lobby
{
	public class LobbyRecvServer : AbstractRecvServer
	{

		/// <summary>
		/// 返回房间列表
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="roomlist">房间简易信息列表</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_GET_ROOM_LIST)]
		public bool ResGetRoomList(uint errcode, List<CCRoomVO> roomlist)
		{
			return true;
		}

		/// <summary>
		/// 返回单个房间信息
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="roomvo">详细房间信息</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_GET_ROOM)]
		public bool ResGetRoomVO(uint errcode, CCRoomVO roomvo)
		{
			return true;
		}

		/// <summary>
		/// 响应创建房间
		/// </summary>
		/// <param name="errcode">返回结果</param>
		/// <param name="roomvo">详细房间信息</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_CREATE)]
		public bool ResCreate(uint errcode, CCRoomVO roomvo)
		{
			return true;
		}

		/// <summary>
		/// 响应退出房间
		/// </summary>
		/// <param name="errcode">返回结果</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_QUIT_ROOM)]
		public bool ResQuitRoom(uint errcode)
		{
			return true;
		}

		/// <summary>
		/// 响应加入房间
		/// </summary>
		/// <param name="errcode">返回结果</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_JOIN)]
		public bool ResJoin(uint errcode)
		{
			return true;
		}

		/// <summary>
		/// 加入房间
		/// </summary>
		/// <param name="applier">申请者信息</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.NTF_JOIN)]
		public bool NtfJoin(CCPlayerVO applier)
		{
			return true;
		}

		/// <summary>
		/// 处理加入请求
		/// </summary>
		/// <param name="errcode">返回结果</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.RES_DEAL_JOIN)]
		public bool ResDealJoin(uint errcode)
		{
			return true;
		}

		/// <summary>
		/// 响应加入房间
		/// </summary>
		/// <param name="isaccept">是否被接受</param>
		/// <param name="accepter">对方玩家信息</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.NTF_DEAL_JOIN)]
		public bool NtfDealJoin(bool isaccept, CCPlayerVO accepter)
		{
			return true;
		}

		/// <summary>
		/// 开始比赛通知
		/// </summary>
		/// <param name="starttime">开始时间</param>
		/// <param name="roomvo">详细房间信息</param>
		[CustomProtocolID(protoid: (uint)ELobbyProto.NTF_START_MATCH)]
		public bool NtfStartMatch(long starttime, CCRoomVO roomvo)
		{
			return true;
		}
	}
}
