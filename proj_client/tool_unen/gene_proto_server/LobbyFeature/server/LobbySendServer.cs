using UKon.Proto;
using UKon.Network;
using System.Collections.Generic;

namespace ETT.Client.Lobby
{
	public class LobbySendServer
	{
		public static readonly LobbySendServer Instance = new LobbySendServer();
		private LobbySendServer() { }

		private NetMgr m_netMgr = NetMgr.Instance;

		/// <summary>
		/// 请求房间列表
		/// </summary>
		/// <param name=""></param>
		public bool ReqGetRoomList()
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_GET_ROOM_LIST;
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求单个房间信息
		/// </summary>
		/// <param name="roomid">房间id</param>
		public bool ReqGetRoomVO(uint roomid)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_GET_ROOM;
			package.Stream.Write(roomid);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求创建房间
		/// </summary>
		/// <param name=""></param>
		public bool ReqCreate()
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_CREATE;
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求退出房间
		/// </summary>
		/// <param name=""></param>
		public bool ReqQuitRoom()
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_QUIT_ROOM;
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 请求加入房间
		/// </summary>
		/// <param name="roomid">房间id</param>
		public bool ReqJoin(uint roomid)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_JOIN;
			package.Stream.Write(roomid);
			m_netMgr.Send(package);
			return true;
		}

		/// <summary>
		/// 处理加入请求
		/// </summary>
		/// <param name="isaccept">处理结果</param>
		public bool ReqDealJoin(bool isaccept)
		{
			NetPacket package = NetPacket.Pop();
			package.ProtoId = (uint)ELobbyProto.REQ_DEAL_JOIN;
			package.Stream.Write(isaccept);
			m_netMgr.Send(package);
			return true;
		}
	}
}
