using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.Chinesechess
{

	/// <summary>
	/// 请求选中棋子 【Client, Gate】
	/// </summary>
	[Message((uint)EChinesechessProto.C2G_Req_ReqSelect)]
	public class C2G_Req_ReqSelect : AbstractRequest
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 返回选中棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Res_ResSelect)]
	public class G2C_Res_ResSelect : AbstractResponse
	{
		public uint errcode {get; set;}
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(errcode);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			errcode = reader.ReadUInt32();
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知选中棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfSelect)]
	public class G2C_Ntf_NtfSelect : AbstractMessage
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 请求移动棋子 【Client, Gate】
	/// </summary>
	[Message((uint)EChinesechessProto.C2G_Req_ReqMove)]
	public class C2G_Req_ReqMove : AbstractRequest
	{
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 返回移动棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Res_ResMove)]
	public class G2C_Res_ResMove : AbstractResponse
	{
		public uint errcode {get; set;}
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(errcode);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			errcode = reader.ReadUInt32();
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 通知移动棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfMove)]
	public class G2C_Ntf_NtfMove : AbstractMessage
	{
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求击杀棋子 【Client, Gate】
	/// </summary>
	[Message((uint)EChinesechessProto.C2G_Req_ReqKill)]
	public class C2G_Req_ReqKill : AbstractRequest
	{
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 返回击杀棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Res_ResKill)]
	public class G2C_Res_ResKill : AbstractResponse
	{
		public uint errcode {get; set;}
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(errcode);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			errcode = reader.ReadUInt32();
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知击杀棋子 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfKill)]
	public class G2C_Ntf_NtfKill : AbstractMessage
	{
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知顺序 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfTurn)]
	public class G2C_Ntf_NtfTurn : AbstractMessage
	{
		public bool isOnTurn {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(isOnTurn);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			isOnTurn = reader.ReadBoolean();
		}
	}

	/// <summary>
	/// 被踢出房间 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfKickOut)]
	public class G2C_Ntf_NtfKickOut : AbstractMessage
	{
		public uint errcode {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(errcode);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			errcode = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 游戏结束 【Gate, Client】
	/// </summary>
	[Message((uint)EChinesechessProto.G2C_Ntf_NtfGameOver)]
	public class G2C_Ntf_NtfGameOver : AbstractMessage
	{
		public bool result {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(result);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			result = reader.ReadBoolean();
		}
	}
}
