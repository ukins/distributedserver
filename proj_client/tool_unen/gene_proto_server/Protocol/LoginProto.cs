using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.Login
{

	/// <summary>
	/// 登陆协议 【Client, Login】
	/// </summary>
	[Message((uint)ELoginProto.C2L_Req_Login)]
	public class C2L_Req_Login : AbstractRequest
	{
		public string name {get; set;}
		public string pwd {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(name);
			writer.Write(pwd);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			name = reader.ReadString();
			pwd = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆协议 【Login, Client】
	/// </summary>
	[Message((uint)ELoginProto.L2C_Res_Login)]
	public class L2C_Res_Login : AbstractResponse
	{
		public long key {get; set;}
		public string addr {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(addr);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			addr = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Client, Gate】
	/// </summary>
	[Message((uint)ELoginProto.C2G_Req_Login)]
	public class C2G_Req_Login : AbstractRequest
	{
		public long key {get; set;}
		public string AccountName {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(key);
			writer.Write(AccountName);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			key = reader.ReadInt64();
			AccountName = reader.ReadString();
		}
	}

	/// <summary>
	/// 登陆网关 【Gate, Client】
	/// </summary>
	[Message((uint)ELoginProto.G2C_Res_Login)]
	public class G2C_Res_Login : AbstractResponse
	{
		public long playerid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(playerid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			playerid = reader.ReadInt64();
		}
	}
}
