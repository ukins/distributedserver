
配置表工具使用说明
======

概述
-----
CSV文件在目前的游戏开发过程中使用广泛
+ 优点：文件体积小，结构简单，容易通过代码进行自定义操作
+ 缺点：Excel对csv格式文件的支持不太好，容易乱码

Excel文本编辑器应用也十分广泛，功能强大，然而对CSV的支持不好。

综上考虑，通过开发excel2csv及unen两个工具，结合Excel与CSV以提升开发效率。

***
组成（excel2csv及unen）
------

>**excel2csv**

功能很简单，就是将xlsx格式的文件转换成csv格式

excel2csv需要在excel2csv.exe.config文件中配置两个地址

excel_path为所有xlsx文件所在的*根目录*，value中的内容为*全局路径*

csv_path为所有csv文件所在的*根目录*，value中的内容为*全局路径*

具体如下图所示：

![这里是alt qiphon](readmeres/excel2csv_conf.png "qiphon")

该工具主要是给策划使用的。
***

>**unen**

功能较复杂，一般由程序使用

|组成       |作用           |
|-----------|:------------:|
|unen.exe   |执行程序       |
|unen.config|配置unen.exe运行过程中会用到的几个路径|
|table.config|表格结构描述文件|
|table_generic.config|table.config中用到的自定义枚举及结构|

功能:

+ 扫描表结构有效性
+ 扫描表内数据有效性
+ 扫描表间数据有效性
+ 生成配置表及自定义结构C#文件

用法：

>在unen.config中配置相关路径，该文件在第一次使用时配置，之后不需要改动，除非工程目录发生变化
+ src_proj_path：csv根目录
+ build_alone_path:配置表编成二进制文件时，二进制文件的存放路径，每一个csv表格对应一个二进制文件。
+ build_unite_path:配置表编成二进制文件时，二进制文件的存放路径，所有csv数据都放在一个二进制文件之内。
+ gene_format_path:在table.config中表格结构描述完成之后，可在此目录生成对应的csv文件，不过没有数据
+ gene_code_client_path:自动生成的客户端C#文件对应的存放目录
+ gene_code_server_path:自动生成的服务器C#文件对应的存放目录（*目前弃用*）

>在table.config中配置需要使用到的表格结构

|关键字       |描述           |
|-----------|:------------:|
|config   |根节点       |
|tables|配置表结构根节点|
|table|单个配置表的表结构根节点|
|field|表格中的字段节点，分容器节点跟叶子节点，容器节点只支持name,type。叶子节点支持更多关键字|
|name|节点名称，可用于table与field，同类型不可以重名且不能为空|
|path|用于table，表示csv表格的相对路径，不能为空|
|type|用于field，表示field的类型，容器节点可以是list,array,dict。叶子节点可以是常用的数值与字符串类型，也可以是table_generic中定义的自定义类型，不能为空|
|desc|字段描述文字，可不填|
|primarykey|唯一主键，每一个table节点中必须有主键字段，可以是primarykey或者unionkey|
|unionkey|联合主键，必须两个unionkey配合使用|
|foreigntable|外界表字段，该字段对应另一张配置表的唯一主键字段|

***

field节点支持的数据类型

+ 基础数据类型
    + bool
    + byte
    + short
    + ushort
    + int
    + uint
    + long
    + ulong
    + float
    + string
    + time(对应C#DateTimeOffset类)
    + timespan(对应C#TimeSpan类)
    + vector2
    + vector2int
    + vector3
    + vector3int
+ 容器类型
    + list
    + array
    + dict(或者dictionary)

注：容器类型可以包含基础类型，容器类型之间原则上不应该存在嵌套关系

>在table_generic.config中配置需要使用到的自定义数据结构

包括两部分：自定义枚举类型Enums与自定义结构体Structures

|Enums关键字|描述|
|-----------|:------------:|
|name(enum)|枚举类型名称|
|name(field)|枚举值名称|
|desc|枚举值描述文字|
***

|Structure关键字|描述|
|-----------|:------------:|
|name(structure)|结构体类型名称|
|name(field)|结构体字段名称|
|type|结构体字段类型|
|desc|结构体字段描述文字|
***

>unen.exe

程序启动时会自动
+ 扫描表结构有效性
+ 扫描表数据有效性

如需要生成相应代码
+ 输入 gene --table 或者 gene -t 命令

>综上

该工具基本功能如上所述，如工作中有额外需求，后续可继续开发改进。