﻿using System.Collections.Generic;
using System.Text;
using UnityEditor;
using UnityEngine;

public class GeneOption
{
    //private static string GetPath(Transform child, Transform root)
    //{
    //    string path = child.name;

    //    while (child.parent != root)
    //    {
    //        path = child.parent.name + "/" + path;
    //        child = child.parent;
    //    }

    //    return path;
    //}

    //private static string GetDesc(UIAdapter adapter)
    //{
    //    string desc = adapter.Desc;
    //    if (desc == string.Empty)
    //    {
    //        desc = adapter.gameObject.name;
    //    }
    //    desc = desc.ToLower();
    //    return desc;
    //}

    //private static string GetFirstBigChar(string str)
    //{
    //    if (str.Length > 0)
    //    {
    //        return str[0].ToString().ToUpper() + str.Substring(1);
    //    }
    //    return string.Empty;
    //}

    //private static string GetEventDesc(EUIBindType bindtype)
    //{
    //    string str = bindtype.ToString();
    //    return str.Replace("On", "").Replace("Event", "");
    //}

    //public static void GeneCSharpWnd()
    //{
    //    if (Selection.gameObjects.Length == 0)
    //    {
    //        EditorUtility.DisplayDialog("提示", "请选择目标对象", "OK");
    //        return;
    //    }
    //    var tgt = Selection.gameObjects[0];

    //    UIWindow root = tgt.GetComponent<UIWindow>();
    //    if (root == null)
    //    {
    //        EditorUtility.DisplayDialog("提示", "当前选中的对象缺少UIWindow组件", "OK");
    //        return;
    //    }

    //    StringBuilder builder = new StringBuilder();

    //    //UIAdapter[] uilist = tgt.GetComponentsInChildren<UIAdapter>(true);
    //    List<UIAdapter> adapters = root.Entitys.FindAll(c => c != null);
    //    //adapters = adapters.FindAll(c => c.Exportable == true);

    //    foreach (var adapter in adapters)
    //    {
    //        builder.AppendFormat("private GameObject {0}_{1};", adapter.ShortName, GetDesc(adapter));
    //        builder.AppendLine();
    //    }

    //    builder.AppendLine();

    //    //foreach (var adapter in adapters)
    //    //{
    //    //    foreach (var bindinfo in adapter.BindTypes)
    //    //    {
    //    //        builder.AppendFormat("private {0}Binder binder_{1}_{2};", bindinfo.ToString(), GetDesc(adapter), bindinfo.ToString().ToLower());
    //    //        builder.AppendLine();
    //    //    }
    //    //}

    //    builder.Append("protected override void OnInit()");
    //    builder.AppendLine();
    //    builder.Append("{");
    //    builder.AppendLine();

    //    for (int i = 0; i < adapters.Count; ++i)
    //    {
    //        var adapter = adapters[i];
    //        string path = GetPath(adapter.transform, tgt.transform);
    //        builder.AppendFormat("\t{0}_{1} = Find(\"{2}\");", adapter.ShortName, GetDesc(adapter), path);
    //        builder.AppendLine();
    //    }
    //    builder.Append("}");
    //    builder.AppendLine();
    //    builder.AppendLine();

    //    builder.Append("protected override void OnShow()");
    //    builder.AppendLine();
    //    builder.Append("{");
    //    builder.AppendLine();

    //    for (int i = 0; i < adapters.Count; ++i)
    //    {
    //        var adapter = adapters[i];
    //        if (adapter.BindType == EUIBindType.None)
    //        {
    //            continue;
    //        }
    //        var shortname = adapter.ShortName;
    //        var desc = GetDesc(adapter);
    //        builder.AppendFormat("\tEventTriggerListener.Get({0}_{1}).{2} += On{3}{4}{5}Handler;",
    //            shortname, desc, adapter.BindType.ToString(),
    //            GetFirstBigChar(shortname), GetFirstBigChar(desc), GetEventDesc(adapter.BindType));
    //        builder.AppendLine();

    //    }
    //    builder.Append("}");
    //    builder.AppendLine();
    //    builder.AppendLine();


    //    builder.Append("protected override void OnHide()");
    //    builder.AppendLine();
    //    builder.Append("{");
    //    builder.AppendLine();
    //    for (int i = 0; i < adapters.Count; ++i)
    //    {
    //        var adapter = adapters[i];
    //        if (adapter.BindType == EUIBindType.None)
    //        {
    //            continue;
    //        }
    //        var shortname = adapter.ShortName;
    //        var desc = GetDesc(adapter);
    //        builder.AppendFormat("\tEventTriggerListener.Get({0}_{1}).{2} -= On{3}{4}{5}Handler;",
    //            shortname, desc, adapter.BindType.ToString(),
    //            GetFirstBigChar(shortname), GetFirstBigChar(desc), GetEventDesc(adapter.BindType));
    //        builder.AppendLine();
    //    }
    //    builder.Append("}");
    //    builder.AppendLine();

    //    builder.AppendLine();

    //    EditorGUIUtility.systemCopyBuffer = builder.ToString();
    //    Debug.Log("Gene C# Code Complete");

    //}

    //public static void GeneLuaWnd()
    //{
    //    Debug.Log("GeneLuaWnd");
    //    if (Selection.gameObjects.Length == 0)
    //    {
    //        EditorUtility.DisplayDialog("提示", "请选择目标对象", "OK");
    //        return;
    //    }
    //    var tgt = Selection.gameObjects[0];
    //    Debug.Log(tgt);

    //}

    public static void GeneScenePos()
    {

        StringBuilder builder = new StringBuilder();
        var allTransforms = Resources.FindObjectsOfTypeAll(typeof(Transform));
        foreach (var item in allTransforms)
        {
            //if (item.name.StartsWith("c_") == false)
            //{
            //    continue;
            //}
            //Debug.Log(item.name);
            Transform trans = item as Transform;
            if (trans == null)
            {
                continue;
            }
            string shortname = trans.name;
            float x = trans.localPosition.x;
            float y = trans.localPosition.z;
            builder.AppendLine(string.Format(",{0},{1},{2},", shortname, x, y));
        }

        EditorGUIUtility.systemCopyBuffer = builder.ToString();
        Debug.Log("Gene Scene Pos Complete");
    }

}
