﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ToolsEditor
{

    //[MenuItem("ToolsEditor/Gene C# Code &m", false, 101)]
    //public static void GeneCSharpWnd()
    //{
    //    GeneOption.GeneCSharpWnd();
    //}
    //[MenuItem("ToolsEditor/Gene Lua Code &n", false, 102)]
    //public static void GeneLuaWnd()
    //{
    //    GeneOption.GeneLuaWnd();
    //}


    [MenuItem("ToolsEditor/Gene/Scene Pos", false, 101)]
    public static void GeneScenePos()
    {
        GeneOption.GeneScenePos();
    }

    //[MenuItem("ToolsEditor/SaveBinaryFile", false, 201)]
    //public static void SaveBinaryFile()
    //{
    //    BinaryFileOption.SaveBinaryFile();
    //}

    //[MenuItem("ToolsEditor/OpenBinaryFile", false, 202)]
    //public static void OpenBinaryFile()
    //{
    //    BinaryFileOption.OpenBinaryFile();
    //}

    //[MenuItem("ToolsEditor/CompressFile", false, 203)]
    //public static void CompressFile()
    //{
    //    BinaryFileOption.CompressFile();
    //}

    //[MenuItem("ToolsEditor/BuildAssetBundle", false, 301)]
    //public static void BuildAssetBundle()
    //{
    //    BuildOption.BuildAssetBundle();
    //}

    //[MenuItem("ToolsEditor/ClearAssetBundle", false, 302)]
    //public static void ClearAssetBundle()
    //{
    //    BuildOption.ClearAssetBundle();
    //}

    //[MenuItem("ToolsEditor/BuildScene", false, 303)]
    //public static void BuildScene()
    //{
    //    BuildOption.BuildScene();
    //}

    //[MenuItem("ToolsEditor/Resources MoveOut", false, 401)]
    //public static void MoveOutResources()
    //{
    //    BuildOption.MoveOutResources();
    //}

    //[MenuItem("ToolsEditor/Resources MoveBack", false, 402)]
    //public static void MoveBackResources()
    //{
    //    BuildOption.MoveBackResources();
    //}

    [MenuItem("ToolsEditor/Scan/BuildSettings_Proj_Scene", false, 301)]
    public static void ScanProjScene()
    {
        ScanOption.ScanProjScene();
    }

    [MenuItem("ToolsEditor/Scan/BuildSettings_All_Scene", false, 302)]
    public static void ScanAllScene()
    {
        ScanOption.ScanAllScene();
    }

    [MenuItem("ToolsEditor/Scan/Find References", false, 303)]
    public static void FindReferences()
    {
        ScanOption.FindReferences();
    }

    [MenuItem("ToolsEditor/Scan/Missing Scripts", false, 304)]
    public static void FindMissingScripts()
    {
        ScanOption.FindMissingScripts();
    }



    //[MenuItem("ToolsEditor/BuildPlayer", false, 302)]
    //public static void BuildPlayer()
    //{
    //    BuildOption.BuildPlayer();
    //}

    //[MenuItem("ToolsEditor/MarkAB", false, 305)]
    //public static void MarkAB()
    //{
    //    BuildOption.MarkAB();
    //}

    //[MenuItem("ToolsEditor/BuildResource", false, 306)]
    //public static void BuildResource()
    //{
    //    BuildOption.BuildResource();
    //}

    //[MenuItem("ToolsEditor/Scan ABSize", false, 501)]
    //public static void ScanABSize()
    //{
    //    ScanOption.ScanABSize();
    //}

    [MenuItem("ToolsEditor/Copy/RelativePath &c", false, 501)]
    public static void CopyRelativePath()
    {
        CopyOption.CopyRelativePath();
    }

    [MenuItem("ToolsEditor/Copy/WayPoint", false, 502)]
    public static void CopyWayPoint()
    {
        CopyOption.CopyWayPoint();
    }

    [MenuItem("ToolsEditor/Run/Unen", false, 701)]
    public static void RunUnen()
    {
        RunOption.RunUnen();
    }
}
