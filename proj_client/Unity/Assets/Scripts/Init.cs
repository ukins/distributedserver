﻿using System;
using ETT.Client;
using ETT.Editor;
using UnityEngine;

public enum EFacade
{
    Game,
    Editor
}

public class Init : MonoBehaviour
{
    public EFacade Facade;

    private IAbstractFacade m_game_facade;

    // Start is called before the first frame update
    private void Start()
    {
        switch (Facade)
        {
            case EFacade.Game:
                m_game_facade = new GameFacade();
                break;
            case EFacade.Editor:
                m_game_facade = new EditorFacade();
                break;
        }
        m_game_facade?.Init();

        GlobalDefine.OnQuit += OnQuitHandler;
    }

    // Update is called once per frame
    private void Update()
    {
        m_game_facade?.Update();
    }

    private void LateUpdate()
    {
        m_game_facade?.LateUpdate();
    }

    private void OnApplicationQuit()
    {
        m_game_facade?.Destroy();
    }

    #region Event Handler
    private void OnQuitHandler()
    {
#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#else
        Application.Quit();
#endif
    }

    #endregion
}
