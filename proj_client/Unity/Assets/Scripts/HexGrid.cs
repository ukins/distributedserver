﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGrid : MonoBehaviour
{
    public enum EStyle
    {
        //四边形
        Quad,
        //六边形
        Hexagon,
    }
    public float Radius = 2;
    public int level = 5;

    public EStyle Style = EStyle.Quad;

    private Dictionary<Vector2Int, GameObject> m_dict = new Dictionary<Vector2Int, GameObject>();

    // Start is called before the first frame update
    void Start()
    {
        //GameObject cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
        //cube.transform.SetParent(transform);
        //cube.transform.localPosition = Vector3.zero;
        //cube.name = "(0,0)";
        switch (Style)
        {
            case EStyle.Hexagon:
                CreateHex(Vector3.zero, Vector2Int.zero, level);
                break;
            case EStyle.Quad:
                CreateQuad(Vector3.zero, Vector2Int.zero, level);
                break;
        }
    }

    private void CreateHex(Vector3 pos, Vector2Int vec, int lvl)
    {
        var p = CreateCube(pos, vec);

        if (lvl <= 0) return;

        CreateHex(new Vector3(p.x + Radius * 0.5f, 0, p.z + 1.732f * Radius * 0.5f), new Vector2Int(vec.x, vec.y + 1), lvl - 1);
        CreateHex(new Vector3(p.x - Radius * 0.5f, 0, p.z + 1.732f * Radius * 0.5f), new Vector2Int(vec.x - 1, vec.y + 1), lvl - 1);
        CreateHex(new Vector3(p.x - Radius, 0, p.z), new Vector2Int(vec.x - 1, vec.y), lvl - 1);
        CreateHex(new Vector3(p.x - Radius * 0.5f, 0, p.z - 1.732f * Radius * 0.5f), new Vector2Int(vec.x, vec.y - 1), lvl - 1);
        CreateHex(new Vector3(p.x + Radius * 0.5f, 0, p.z - 1.732f * Radius * 0.5f), new Vector2Int(vec.x + 1, vec.y - 1), lvl - 1);
        CreateHex(new Vector3(p.x + Radius, 0, p.z), new Vector2Int(vec.x + 1, vec.y), lvl - 1);
    }

    private void CreateQuad(Vector3 pos, Vector2Int vec, int lvl)
    {
        var p = CreateCube(pos, vec);

        if (lvl <= 0) return;

        CreateQuad(new Vector3(p.x, 0, p.z + Radius), new Vector2Int(vec.x, vec.y + 1), lvl - 1);
        CreateQuad(new Vector3(p.x - Radius, 0, p.z + Radius), new Vector2Int(vec.x - 1, vec.y + 1), lvl - 1);
        CreateQuad(new Vector3(p.x - Radius, 0, p.z), new Vector2Int(vec.x - 1, vec.y), lvl - 1);
        CreateQuad(new Vector3(p.x - Radius, 0, p.z - Radius), new Vector2Int(vec.x - 1, vec.y - 1), lvl - 1);
        CreateQuad(new Vector3(p.x, 0, p.z - Radius), new Vector2Int(vec.x, vec.y - 1), lvl - 1);
        CreateQuad(new Vector3(p.x + Radius, 0, p.z - Radius), new Vector2Int(vec.x + 1, vec.y - 1), lvl - 1);
        CreateQuad(new Vector3(p.x + Radius, 0, p.z), new Vector2Int(vec.x + 1, vec.y), lvl - 1);
        CreateQuad(new Vector3(p.x + Radius, 0, p.z + Radius), new Vector2Int(vec.x + 1, vec.y + 1), lvl - 1);
    }

    private Vector3 CreateCube(Vector3 pos, Vector2Int vec)
    {
        GameObject cube;
        if (m_dict.ContainsKey(vec) == false)
        {
            cube = GameObject.CreatePrimitive(PrimitiveType.Cube);
            cube.transform.SetParent(transform);
            cube.transform.localPosition = pos;
            cube.name = $"{vec.x}|{vec.y}";
            m_dict.Add(vec, cube);
        }
        else
        {
            cube = m_dict[vec];
        }
        return cube.transform.localPosition;
    }

    // Update is called once per frame
    void Update()
    {

    }


}
