﻿using ETT.Model;
using ETT.Widgets;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Client
{
    public class CirculatGrid : MonoBehaviour, ICircularGrid
    {
        [SerializeField]
        int minAmount = 0;//实现无限滚动，需要的最少的child数量。屏幕上能看到的+一行看不到的，比如我在屏幕上能看到 2 行，每一行 2 个。则这个值为 2行*2个 + 1 行* 2个 = 6个。  
        RectTransform rectTransform;
        GridLayoutGroup gridLayoutGroup;
        ContentSizeFitter contentSizeFitter;
        ScrollRect scrollRect;
        List<RectTransform> children = new List<RectTransform>();
        Vector2 startPosition;
        int amount = 0;
        //public delegate void UpdateChildrenCallbackDelegate(int index, Transform trans);

        int realIndex = -1;
        private int toplevelPointer = 0;
        private int bottomlevelPointer = 0;
        //int realIndexUp = -1; //从下往上;  
        bool hasInit = false;
        //Vector2 gridLayoutSize;
        Vector2 gridLayoutPos;
        Dictionary<Transform, Vector2> childsAnchoredPosition = new Dictionary<Transform, Vector2>();
        Dictionary<Transform, int> childsSiblingIndex = new Dictionary<Transform, int>();
        Dictionary<Transform, int> m_child_real_index = new Dictionary<Transform, int>();

        private int constraintCount
        {
            get { return gridLayoutGroup.constraintCount; }
        }

        private Vector2 cellSize
        {
            get { return gridLayoutGroup.cellSize; }
        }

        private Vector2 spacing
        {
            get { return gridLayoutGroup.spacing; }
        }

        private float UnitX
        {
            get { return cellSize.x + spacing.x; }
        }

        private float UnitY
        {
            get { return cellSize.y + spacing.y; }
        }

        private GridLayoutGroup.Constraint constraint
        {
            get { return gridLayoutGroup.constraint; }
        }

        //public event Action<GameObject, int> UpdateChildrenEvent;
        //public event Action<GameObject, int> SelectChildrenEvent;

        private Action<GameObject, int> m_update_func;
        public void SetUpdateFunc(Action<GameObject, int> func)
        {
            m_update_func = func;
        }
        private Action<GameObject, int> m_select_func;
        public void SetSelectFunc(Action<GameObject, int> func)
        {
            m_select_func = func;
        }

        private void Awake()
        {
            Log.Debug("UICircularGridLayout.Awake");
        }

        // Use this for initialization  
        private void Start()
        {
            Log.Debug("UICircularGridLayout.Start");
        }
        IEnumerator InitChildren()
        {
            yield return 0;
            if (!hasInit)
            {
                //获取Grid的宽度;  

                //LogMgr.Instance.Log("UICircularGridLayout.InitChildren1");
                rectTransform = GetComponent<RectTransform>();
                gridLayoutGroup = GetComponent<GridLayoutGroup>();
                gridLayoutGroup.enabled = false;
                contentSizeFitter = GetComponent<ContentSizeFitter>();
                if (contentSizeFitter != null)
                {
                    contentSizeFitter.enabled = false;
                }
                gridLayoutPos = rectTransform.anchoredPosition;
                //gridLayoutSize = rectTransform.sizeDelta;
                //注册ScrollRect滚动回调;  
                scrollRect = transform.parent.GetComponent<ScrollRect>();
                scrollRect.onValueChanged.AddListener((data) => { ScrollCallback(data); });
                //获取所有child anchoredPosition 以及 SiblingIndex;  
                for (int index = 0; index < transform.childCount; index++)
                {
                    Transform child = transform.GetChild(index);
                    RectTransform childRectTrans = child.GetComponent<RectTransform>();
                    childsAnchoredPosition.Add(child, childRectTrans.anchoredPosition);
                    childsSiblingIndex.Add(child, child.GetSiblingIndex());
                    PointerClickListener.Get(child.gameObject).onClick += UICircularGridLayout_onClick;
                }
            }
            else
            {
                //LogMgr.Instance.Log("UICircularGridLayout.InitChildren2");

                rectTransform.anchoredPosition = gridLayoutPos;
                //rectTransform.sizeDelta = gridLayoutSize;
                children.Clear();
                realIndex = -1;
                //realIndexUp = -1;
                //children重新设置上下顺序;  
                foreach (var info in childsSiblingIndex)
                {
                    info.Key.SetSiblingIndex(info.Value);
                }
                //children重新设置anchoredPosition;  
                for (int index = 0; index < transform.childCount; index++)
                {
                    Transform child = transform.GetChild(index);
                    RectTransform childRectTrans = child.GetComponent<RectTransform>();
                    if (childsAnchoredPosition.ContainsKey(child))
                    {
                        childRectTrans.anchoredPosition = childsAnchoredPosition[child];
                    }
                    else
                    {
                        Debug.LogError("childsAnchoredPosition no contain " + child.name);
                    }
                }
            }
            //获取所有child;  
            for (int index = 0; index < transform.childCount; index++)
            {
                Transform trans = transform.GetChild(index);
                children.Add(transform.GetChild(index).GetComponent<RectTransform>());
                //初始化前面几个;  
                if (index < amount)
                {
                    trans.gameObject.SetActive(true);
                    UpdateChildrenCallback(children.Count - 1, transform.GetChild(index));
                }
                else
                {
                    trans.gameObject.SetActive(false);
                }
            }
            startPosition = rectTransform.anchoredPosition;
            realIndex = children.Count - 1;
            toplevelPointer = 0;
            bottomlevelPointer = children.Count - constraintCount;
            //Debug.Log( scrollRect.transform.TransformPoint(Vector3.zero));  
            // Debug.Log(transform.TransformPoint(children[0].localPosition));  
            hasInit = true;
            //如果需要显示的个数小于设定的个数;  
            //LogMgr.Instance.Log("minAmount = " + minAmount);
            for (int index = 0; index < minAmount; index++)
            {
                if (index < children.Count)
                {
                    children[index].gameObject.SetActive(index < amount);
                }
            }
            int mcnt = Mathf.Max(minAmount - constraintCount, amount);
            if (constraint == GridLayoutGroup.Constraint.FixedColumnCount)
            {
                int totalrow = Mathf.CeilToInt(mcnt * 1.0f / constraintCount);
                rectTransform.sizeDelta = new Vector2(UnitX * constraintCount, UnitY * totalrow);
            }
            else
            {
                int totalcolumn = Mathf.CeilToInt(mcnt * 1.0f / constraintCount);
                rectTransform.sizeDelta = new Vector2(UnitX * totalcolumn, UnitY * constraintCount);
            }
        }

        private void UICircularGridLayout_onClick(GameObject go)
        {
            if (m_child_real_index.ContainsKey(go.transform) == false)
            {
                return;
            }
            int index = m_child_real_index[go.transform];
            m_select_func?.Invoke(go, index);
        }

        // Update is called once per frame  
        void Update()
        {
        }

        void ScrollCallback(Vector2 data)
        {
            UpdateChildren();
        }

        private void RefreshScrollUP()
        {
            float scrollRectUp = scrollRect.transform.TransformPoint(Vector3.zero).y;
            var anchoredPosition = children[toplevelPointer].anchoredPosition;
            Vector3 childBottomLeft = new Vector3(anchoredPosition.x, anchoredPosition.y - UnitY, 0f);
            float childBottom = transform.TransformPoint(childBottomLeft).y;
            if (childBottom >= scrollRectUp)
            {
                for (int index = toplevelPointer; index < constraintCount + toplevelPointer; index++)
                {
                    children[index].anchoredPosition = new Vector2(children[index].anchoredPosition.x, children[bottomlevelPointer].anchoredPosition.y - UnitY);
                    realIndex++;
                    if (realIndex > amount - 1)
                    {
                        children[index].gameObject.SetActive(false);
                    }
                    else
                    {
                        UpdateChildrenCallback(realIndex, children[index]);
                    }
                }
                toplevelPointer = (toplevelPointer + constraintCount) % children.Count;
                bottomlevelPointer = (bottomlevelPointer + constraintCount) % children.Count;
            }
        }

        private void RefreshScrollLeft()
        {
            float scrollRectLeft = scrollRect.transform.TransformPoint(Vector3.zero).x;
            var anchoredPosition = children[toplevelPointer].anchoredPosition;
            Vector3 childBottomRight = new Vector3(anchoredPosition.x + UnitX, anchoredPosition.y, 0f);
            float childRight = transform.TransformPoint(childBottomRight).x;
            if (childRight <= scrollRectLeft)
            {
                for (int index = toplevelPointer; index < constraintCount + toplevelPointer; index++)
                {
                    children[index].anchoredPosition = new Vector2(children[bottomlevelPointer].anchoredPosition.x + UnitX, children[index].anchoredPosition.y);
                    realIndex++;
                    if (realIndex > amount - 1)
                    {
                        children[index].gameObject.SetActive(false);
                    }
                    else
                    {
                        UpdateChildrenCallback(realIndex, children[index]);
                    }
                }

                toplevelPointer = (toplevelPointer + constraintCount) % children.Count;
                bottomlevelPointer = (bottomlevelPointer + constraintCount) % children.Count;
            }
        }

        private void RefreshScrollDown()
        {
            RectTransform scrollRectTransform = scrollRect.GetComponent<RectTransform>();
            Vector3 scrollRectAnchorBottom = new Vector3(0, -scrollRectTransform.rect.height, 0f);
            float scrollRectBottom = scrollRect.transform.TransformPoint(scrollRectAnchorBottom).y;
            var anchoredPosition = children[bottomlevelPointer].anchoredPosition;
            Vector3 childUpLeft = new Vector3(anchoredPosition.x, anchoredPosition.y, 0f);
            float childUp = transform.TransformPoint(childUpLeft).y;
            if (childUp < scrollRectBottom)
            {
                for (int index = bottomlevelPointer; index < constraintCount + bottomlevelPointer; index++)
                {
                    children[index].anchoredPosition = new Vector2(children[index].anchoredPosition.x, children[toplevelPointer].anchoredPosition.y + UnitY);
                    realIndex--;
                    children[index].gameObject.SetActive(true);
                    UpdateChildrenCallback(realIndex - children.Count - constraintCount + 2 * (index - bottomlevelPointer + 1), children[index]);
                }

                toplevelPointer = (toplevelPointer - constraintCount + children.Count) % children.Count;
                bottomlevelPointer = (bottomlevelPointer - constraintCount + children.Count) % children.Count;
            }
        }

        private void RefreshScrollRight()
        {
            RectTransform scrollRectTransform = scrollRect.GetComponent<RectTransform>();
            Vector3 scrollRectAnchorRight = new Vector3(scrollRectTransform.rect.width, 0, 0f);
            float scrollRectRight = scrollRect.transform.TransformPoint(scrollRectAnchorRight).x;
            var anchoredPosition = children[bottomlevelPointer].anchoredPosition;
            Vector3 childUpLeft = new Vector3(anchoredPosition.x, anchoredPosition.y, 0f);
            float childLeft = transform.TransformPoint(childUpLeft).x;
            if (childLeft >= scrollRectRight)
            {
                for (int index = bottomlevelPointer; index < constraintCount + bottomlevelPointer; index++)
                {
                    children[index].anchoredPosition = new Vector2(children[toplevelPointer].anchoredPosition.x - UnitX, children[index].anchoredPosition.y);
                    children[index].gameObject.SetActive(true);
                    realIndex--;
                    UpdateChildrenCallback(realIndex - children.Count - constraintCount + 2 * (index - bottomlevelPointer + 1), children[index]);
                }

                toplevelPointer = (toplevelPointer - constraintCount + children.Count) % children.Count;
                bottomlevelPointer = (bottomlevelPointer - constraintCount + children.Count) % children.Count;
            }
        }

        void UpdateChildren()
        {
            if (transform.childCount < minAmount)
            {
                return;
            }
            Vector2 currentPos = rectTransform.anchoredPosition;
            if (constraint == GridLayoutGroup.Constraint.FixedColumnCount)
            {
                float offsetY = currentPos.y - startPosition.y;
                if (offsetY > 0)
                {
                    //向上拉，向下扩展;  
                    if (realIndex >= amount - 1)
                    {
                        startPosition = currentPos;
                        return;
                    }
                    RefreshScrollUP();
                }
                else
                {
                    //Debug.Log("Drag Down");  
                    //向下拉，下面收缩;  
                    if (realIndex + 1 <= children.Count)
                    {
                        startPosition = currentPos;
                        return;
                    }
                    RefreshScrollDown();
                }
            }
            else
            {
                float offsetX = currentPos.x - startPosition.x;
                if (offsetX < 0)
                {
                    //向左拉，向右扩展;  
                    if (realIndex >= amount - 1)
                    {
                        startPosition = currentPos;
                        return;
                    }
                    RefreshScrollLeft();
                }
                else
                {
                    //Debug.Log("Drag Down");  
                    //向右拉，右边收缩;  
                    if (realIndex + 1 <= children.Count)
                    {
                        startPosition = currentPos;
                        return;
                    }
                    RefreshScrollRight();
                }
            }
            startPosition = currentPos;
        }

        void UpdateChildrenCallback(int index, Transform trans)
        {
            if (m_child_real_index.ContainsKey(trans) == false)
            {
                m_child_real_index.Add(trans, index);
            }
            else
            {
                m_child_real_index[trans] = index;
            }
            m_update_func.Invoke(trans.gameObject, index);
        }

        /// <summary>  
        /// 设置总的个数;  
        /// </summary>  
        /// <param name="count"></param>  
        public void SetAmount(int count)
        {
            amount = count;
            StartCoroutine(InitChildren());
        }
    }
}
