﻿using ETT.Client.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.FeiXingQi
{
    /// <summary>
    /// 飞行棋
    /// </summary>
    public class ChessRoot : AbstractChessEntity, IAwake, IChessRoot
    {
        public override ECommonGameKindTable Kind => ECommonGameKindTable.FeiXingQi;

        public void Awake()
        {
            Root = this;
        }

        public override void Enter()
        {
            Log.Debug("Enter FeiXingQi");
        }

        public override void Exit()
        {
            Log.Debug("Exit FeiXingQi");
        }

        public override void GameOver()
        {

        }

    }
}
