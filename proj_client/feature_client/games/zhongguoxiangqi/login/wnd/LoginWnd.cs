﻿using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.server;
using ETT.Widgets;
using System;
using System.Net;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi.Login
{
    [Window((int)EGenericWndTable.Login)]
    public class LoginWnd : AbstractWnd
    {
#pragma warning disable 0649,0169
        [UIField("root/commit")]
        private UIButton btn_commit;
        [UIField("root/cancel")]
        private UIButton btn_cancel;
        [UIField("root/pwd/InputField")]
        private UIInputField ipt_pwd;
        [UIField("root/name/InputField")]
        private UIInputField ipt_name;
        [UIField("root/ip/InputField")]
        private UIInputField ipt_ip;
#pragma warning restore 0649,0169

        private LoginComponent LoginMgr => Root.GetComponent<LoginComponent>();

        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            UIEventListener.Get(btn_commit).onClick += OnBtnCommitClickHandler;
            UIEventListener.Get(btn_cancel).onClick += OnBtnCancelClickHandler;

            ipt_ip.text = PlayerPrefs.GetString("zhongguoxiangqi.login.ip");
            ipt_name.text = PlayerPrefs.GetString("zhongguoxiangqi.login.name");
            ipt_pwd.text = PlayerPrefs.GetString("zhongguoxiangqi.login.pwd");

            if (ipt_ip.text == string.Empty)
            {
                var yaml = Context.Game.GetComponent<YamlComponent>().Global;
                ipt_ip.text = yaml.LoginAddr;
            }
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnHide()
        {
            UIEventListener.Get(btn_commit).onClick -= OnBtnCommitClickHandler;
            UIEventListener.Get(btn_cancel).onClick -= OnBtnCancelClickHandler;
        }

        protected override void OnDestroy()
        {
        }

        #region Event Handler

#if ONLINE_MODE
        private async void OnBtnCommitClickHandler(GameObject obj)
#else
        private void OnBtnCommitClickHandler(GameObject obj)
#endif
        {
            if (string.IsNullOrWhiteSpace(ipt_name.text) || string.IsNullOrWhiteSpace(ipt_pwd.text))
            {
                UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, Localize("用户名或密码不能为空！！"));
                return;
            }

            char[] word = ipt_name.text.ToCharArray();
            bool result = Context.Game.GetComponent<TableComponent>().Match(word);
            if (result == true)
            {
                ipt_name.text = new string(word);
                UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, Localize("存在非法字符！！"));
                return;
            }

            IPEndPoint ip_ent_point;

            try
            {
                ip_ent_point = ServerDefine.ToIPEndPoint(ipt_ip.text);
            }
            catch(Exception e)
            {
                Log.Error(e);
                UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, Localize("IP地址无效！！"));
                return;
            }

            try
            {
                PlayerPrefs.SetString("zhongguoxiangqi.login.ip", ipt_ip.text);
                PlayerPrefs.SetString("zhongguoxiangqi.login.name", ipt_name.text);
                PlayerPrefs.SetString("zhongguoxiangqi.login.pwd", ipt_pwd.text);

                btn_commit.interactable = false;

#if ONLINE_MODE
                var yaml = Context.Game.GetComponent<YamlComponent>().Global;
                Session login_session = NetworkMgr.Create(ip_ent_point, false);
                login_session.OnErrorCallback += Login_session_OnErrorCallback;
                login_session.Start();

                L2C_Res_Login res_login = (L2C_Res_Login)await login_session.Call(new C2L_Req_Login() { name = ipt_name.text, pwd = ipt_pwd.text });
                login_session.Dispose();

                NetworkMgr.GateAddr = ServerDefine.ToIPEndPoint(res_login.addr);

                Session gate_session = NetworkMgr.Create(NetworkMgr.GateAddr);
                LoginMgr.GateSession = gate_session;

                G2C_Res_Login res = (G2C_Res_Login)await gate_session.Call(new C2G_Req_Login()
                {
                    key = res_login.key,
                    AccountName = ipt_name.text
                });

                NetworkMgr.AccountId = res.playerid;

                Log.Debug("登陆Gate成功");
#else
                NetworkMgr.AccountId = 1;
#endif

                UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, Localize("登陆成功"));

                Context.Game.GetComponent<SelectGameComponent>().SwitchModule<LobbyComponent>();
            }
            catch (Exception e)
            {
                Log.Error(e);
            }
            finally
            {
                btn_commit.interactable = true;
            }
        }


        private void OnBtnCancelClickHandler(GameObject obj)
        {
            Log.Debug("OnBtnCancelClickHandler");
            WndMgr.ShowGenericDialog(Localize("是否确定退出游戏？"), Localize("提示"), () =>
            {
                GlobalDefine.OnQuit?.Invoke();
            },
            () => { }
            );
        }

        private void Login_session_OnErrorCallback(Session arg1, int arg2)
        {
            if (arg2 != 0)
            {
                string str = Localize($"登陆服务器连接出错！ ErrorCode = {arg2}");
                Log.Error(str);
                UIEventMgr.Run(GenericTipsWnd.ShowTipsEvent, str);
            }
            btn_commit.interactable = true;
        }
        #endregion
    }
}
