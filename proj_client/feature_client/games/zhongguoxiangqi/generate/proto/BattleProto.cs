using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.ZhongGuoXiangQi
{

	/// <summary>
	/// 请求选中棋子 【Client, World】
	/// </summary>
	[Message((uint)EBattleProto.C2W_Req_Select)]
	public class C2W_Req_Select : AbstractLocationRequest
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 返回选中棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Res_Select)]
	public class W2C_Res_Select : AbstractLocationResponse
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知选中棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Select)]
	public class W2C_Ntf_Select : AbstractLocationMessage
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 请求移动棋子 【Client, World】
	/// </summary>
	[Message((uint)EBattleProto.C2W_Req_Move)]
	public class C2W_Req_Move : AbstractLocationRequest
	{
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chess = new ChessVO();
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 返回移动棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Res_Move)]
	public class W2C_Res_Move : AbstractLocationResponse
	{
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chess = new ChessVO();
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 通知移动棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Move)]
	public class W2C_Ntf_Move : AbstractLocationMessage
	{
		public ChessVO chess {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			chess.Pack(writer);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chess = new ChessVO();
			chess.UnPack(reader);
		}
	}

	/// <summary>
	/// 请求击杀棋子 【Client, World】
	/// </summary>
	[Message((uint)EBattleProto.C2W_Req_Kill)]
	public class C2W_Req_Kill : AbstractLocationRequest
	{
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 返回击杀棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Res_Kill)]
	public class W2C_Res_Kill : AbstractLocationResponse
	{
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知击杀棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Kill)]
	public class W2C_Ntf_Kill : AbstractLocationMessage
	{
		public uint attackerid {get; set;}
		public uint defenderid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(attackerid);
			writer.Write(defenderid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			attackerid = reader.ReadUInt32();
			defenderid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知顺序 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Turn)]
	public class W2C_Ntf_Turn : AbstractLocationMessage
	{
		public bool isOnTurn {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(isOnTurn);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			isOnTurn = reader.ReadBoolean();
		}
	}

	/// <summary>
	/// 游戏结束 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_GameOver)]
	public class W2C_Ntf_GameOver : AbstractLocationMessage
	{
		public int faction {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(faction);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			faction = reader.ReadInt32();
		}
	}
}
