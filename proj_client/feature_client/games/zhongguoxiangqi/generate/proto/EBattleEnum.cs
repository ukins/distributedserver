namespace ETT.Client.ZhongGuoXiangQi
{
	/// <summary>
	/// Battle协议枚举 Range[25000,30000)
	/// <summary>
	public enum EBattleProto
	{
		C2W_Req_Select = 25100,
		W2C_Res_Select = 25101,
		W2C_Ntf_Select = 25102,
		C2W_Req_Move = 25110,
		W2C_Res_Move = 25111,
		W2C_Ntf_Move = 25112,
		C2W_Req_Kill = 25120,
		W2C_Res_Kill = 25121,
		W2C_Ntf_Kill = 25122,
		W2C_Ntf_Turn = 25130,
		W2C_Ntf_GameOver = 25150,
	}

}
