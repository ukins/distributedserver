﻿using ETT.AI.Common;
using ETT.AI.ZhongGuoXiangQi;
using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;

namespace ETT.Client.ZhongGuoXiangQi
{
    public class ChessPieceComponent : Component, IAbstractChessPieceEx,
         IAwake<ChessmanTableConf>
    {
        #region Rule

        public AbstractChessPiece ChessPiece { get; private set; }

        public EChessPieceType Chessmantype => ChessPiece.Chessmantype;


        public List<UVector2Int> FindPoints()
        {
            return ChessPiece.FindPoints();
        }

        public bool CanKill(UVector2Int point)
        {
            return ChessPiece.CanKill(point);
        }

        public void Kill(UVector2Int point)
        {
            ChessPiece.Kill(point);
        }

        public bool CanMove(int x, int y)
        {
            return ChessPiece.CanMove(x, y);
        }

        public bool CanMove(UVector2Int vec)
        {
            return ChessPiece.CanMove(vec);
        }

        public void Move(int x, int y)
        {
            ChessPiece.Move(x, y);
        }

        public void Move(UVector2Int vec)
        {
            ChessPiece.Move(vec);
        }

        //public void BeKilled(IAbstractChessPiece attacker)
        //{
        //    ChessPiece.BeKilled(attacker);
        //    Context.Event.Run(ChessPieceUnit.BeKilled, attacker, ChessPiece);
        //}

        #endregion

        public ChessmanTableConf Conf { get; private set; }

        public void Awake(ChessmanTableConf t)
        {
            Conf = t;

            ChessPiece = Create(t);

            ChessPiece.Move(t.BirthPoint);
        }

        private AbstractChessPiece Create(ChessmanTableConf conf)
        {
            EChessPieceFaction faction = conf.Id <= 16 ? EChessPieceFaction.RED : EChessPieceFaction.BLACK;
            AbstractChessPiece rule = null;
            var kind = (EChessPieceType)Enum.Parse(typeof(EChessPieceType), conf.Type);
            switch (kind)
            {
                case EChessPieceType.Soldier:
                    rule = Root.Factory.CreateWithId<Soldier, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.Cannon:
                    rule = Root.Factory.CreateWithId<Cannon, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.Chariot:
                    rule = Root.Factory.CreateWithId<Chariot, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.Knight:
                    rule = Root.Factory.CreateWithId<Knight, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.Minister:
                    rule = Root.Factory.CreateWithId<Minister, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.Scholar:
                    rule = Root.Factory.CreateWithId<Scholar, EChessPieceFaction>(faction);
                    break;
                case EChessPieceType.General:
                    rule = Root.Factory.CreateWithId<General, EChessPieceFaction>(faction);
                    break;
            }
            if (rule != null)
            {
                rule.ChessPiece = GetParent<ChessPieceEntity>();
                rule.ChessPiece.Faction = (int)faction;
            }
            return rule;
        }
    }
}
