﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Model;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi
{
    public class ChessBoardComponent : Model.Component, IAwake
    {
        public void Awake()
        {
            InitChessman().Coroutine();
        }

        private async ETVoid InitChessman()
        {
            SceneUnitComponent unitMgr = Context.Game.GetComponent<SceneUnitComponent>();
            ChessmanTable table = Context.Table.GetComponent<ChessmanTable>();

            var chess_board = GetParent<ChessBoardEntity>();

            var lst = table.FindAllVO();
            foreach (var conf in lst)
            {
                var chess_piece = Root.Factory.CreateWithId<ChessPieceEntity>();
                chess_board.Add(conf.Id, chess_piece);

                chess_piece.AddComponent<ChessPieceComponent, ChessmanTableConf>(conf);
                chess_piece.Board = chess_board;
                chess_piece.IsAvailable = true;

                var unit = chess_piece.AddComponent<ChessSceneUnit, ChessmanTableConf>(conf);
                unit.SelectEvt = Define.Select;
                await unit.LoadModel();
                unitMgr.Add(unit);

                chess_piece.AddComponent<AnimateComponent, Transform>(unit.ModelGO.transform.GetChild(0));
                chess_piece.AddComponent<MovableComponent>();
            }

            Context.Game.GetComponent<WndComponent>().Hide((int)ECommonWndTable.SceneLoading);
        }

    }
}
