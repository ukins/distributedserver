﻿using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.ZhongGuoXiangQi
{

    [Window((int)EGenericWndTable.Battle)]
    [UIEventLabel]
    public class BattleWnd : AbstractWnd
    {
        public const string OnTurnEvent = "chinesechess_onturnevent";

#pragma warning disable 0649
        [UIField("root/countdown")]
        private UILabel lbl_cd;
        [UIField("root/master/Text")]
        private UILabel lbl_master;
        [UIField("root/challenger/Text")]
        private UILabel lbl_challenger;
#pragma warning restore 0649

        private string m_str_cd;
        private long m_start_time;

        //private Action m_start_pointer;

        protected override void OnShow()
        {
            m_str_cd = Localize("倒计时：{0}秒");

#if ONLINE_MODE
            m_start_time = Define.StartTime;
            lbl_master.text = Define.VO.Players[0].PlayerName;
            lbl_challenger.text = Define.VO.Players[1].PlayerName;

            StartCDAsync().Coroutine();
#endif
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnHide()
        {
        }

        private async ETVoid StartCDAsync()
        {
            long curr_time = TimeUtils.ClientNow();
            if (curr_time > m_start_time)
            {
                lbl_cd.text = "";
                lbl_cd.text = "";
                OnTurn();
                return;
            }

            int value = (int)(m_start_time - curr_time);

            while (value > 0)
            {
                lbl_cd.text = string.Format(m_str_cd, value / 1000);
                await Context.Game.GetComponent<TimeComponent>().WaitAsync(500);
                value -= 500;
            }

            OnTurn();
        }

        #region Evnet Handler

        [UIEventHandler(OnTurnEvent)]
        private void OnTurn()
        {
            if (WndState != EWndState.Show)
            {
                return;
            }
            if (Define.isOnTurn == true)
            {
                lbl_cd.text = Localize("己方回合");
            }
            else
            {
                lbl_cd.text = Localize("敌方回合");
            }
        }

        #endregion
    }
}
