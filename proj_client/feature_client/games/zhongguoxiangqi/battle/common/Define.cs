﻿using ETT.AI.Common;
using ETT.AI.ZhongGuoXiangQi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.ZhongGuoXiangQi
{
    public static class Define
    {
        public const string Move = "zhongguoxiangqi_move";
        public const string Kill = "zhongguoxiangqi_kill";
        public const string Select = "zhongguoxiangqi_select";
        public const string MayBeKilled = "zhongguoxiangqi_maybekilled";

        public const string Exe_Move = "zhongguoxiangqi_exe_move";
        public const string Exe_Kill = "zhongguoxiangqi_exe_kill";
        public const string Exe_GameOver = "zhongguoxiangqi_exe_gameover";

        public static RoomVO VO;
        public static long StartTime;
        public static bool isOnTurn = false;
        public static EChessPieceFaction MineFaction = EChessPieceFaction.BLACK;

        public static bool isGameOver = true;
        public static EChessPieceFaction WinFaction = EChessPieceFaction.INVALID;

        public static ChessBoardEntity BoardET;
    }
}
