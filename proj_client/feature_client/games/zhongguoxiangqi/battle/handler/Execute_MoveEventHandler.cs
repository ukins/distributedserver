﻿using ETT.AI.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Exe_Move)]
    public class Execute_MoveEventHandler : IEvent<ChessSceneUnit, UVector2Int>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        public void Run(ChessSceneUnit unit, UVector2Int pos)
        {
            RunAsync(unit, pos).Coroutine();
        }

        private Vector3 TranslateVec(UVector2Int pos)
        {
            var footholdtable = Context.Table.GetComponent<FootholdTable>();
            var conf = footholdtable[pos];
            return new Vector3(conf.Axis.X, 0, conf.Axis.Y);
        }

        private async ETVoid RunAsync(ChessSceneUnit unit, UVector2Int pos)
        {
            var chess_piece = unit.GetParent<ChessPieceEntity>();

            var agentMgr = chess_piece.GetComponent<ChessPieceComponent>();

            //播放动画
            var animMgr = chess_piece.GetComponent<AnimateComponent>();
            animMgr.Move();

            //位移
            var moveMgr = chess_piece.GetComponent<MovableComponent>();
            await moveMgr.MoveAsync(TranslateVec(pos));

            animMgr.Stop();

            //动画播放完成之后设置数据
            agentMgr.Move(pos);

            //调整朝向
            //unit.transform.DOLocalMove(new Vector3(foothold.Axis.X, 0, foothold.Axis.Y), 1);

        }
    }
}
