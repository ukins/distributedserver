﻿using ETT.AI.Common;
using ETT.Client.Common;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_KillHandler : AbstractMsgHandler<W2C_Ntf_Kill>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_Kill msg)
        {
            var select_comp = Context.Game.GetComponent<SelectGameComponent>();
            var board = select_comp.CurrChess.GetComponent<ChessBoardEntity>();

            var chess_atk = board[msg.attackerid];
            if (chess_atk == null || chess_atk.IsAvailable == false)
            {
                throw new Exception($"Error！ Unit_Atk Is Not Exist, id={msg.attackerid}");
            }

            var chess_def = board[msg.defenderid];
            if (chess_def == null || chess_def.IsAvailable == false)
            {
                throw new Exception($"Error！ Unit_Def Is Not Exist, id={msg.defenderid}");
            }

            var unit_atk = chess_atk.GetComponent<ChessSceneUnit>();
            var unit_def = chess_def.GetComponent<ChessSceneUnit>();

            Context.Event.Run(Define.Exe_Kill, unit_atk, unit_def);

            await ETTask.CompletedTask;
        }
    }
}
