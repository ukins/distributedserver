﻿using ETT.AI.Common;
using ETT.Client.Table;
using ETT.Model;

namespace ETT.Client.ZhongGuoXiangQi.Battle
{
    [Event(Define.Select)]
    public class SelectEventHandler : IEvent<SceneUnit, bool>
    {
        private ChessSceneUnit CurrUnit { get; set; }

        public void Run(SceneUnit t, bool state)
        {
            if (t == null || t.Root == null)
            {
                return;
            }

            if (state == true)
            {
                Select(t);
            }
            else
            {
                Deselect(t);
            }
        }

        private void Switch(SceneUnit unit)
        {
            CurrUnit?.ConfirmSelect(false);
            if (unit is ChessSceneUnit)
            {
                unit.ConfirmSelect(true);
                CurrUnit = unit as ChessSceneUnit;
            }

            ShowPathPoint();
        }

        private void Select(SceneUnit unit)
        {
            if (unit == null)
            {
                return;
            }
            if (CurrUnit == null)
            {
                Switch(unit);
            }
            else
            {
                //已经选择了棋子
                var curr_piece = CurrUnit.GetParent<ChessPieceEntity>();
                var tgt_piece = unit.GetParent<ChessPieceEntity>();
                var curr_agent = CurrUnit.Entitas.GetComponent<ChessPieceComponent>();
                //var tgt_agent = unit.Entitas.GetComponent<ChessPieceComponent>();
                if (curr_piece.Faction != (int)Define.MineFaction)
                {
                    Switch(unit);
                }
                else
                {
                    if (tgt_piece == null)
                    {
                        Context.Event.Run(Define.Move, CurrUnit, unit.Pos);
                        HidePathPoint();
                        return;
                    }
                    if (tgt_piece.Faction == (int)Define.MineFaction)
                    {
                        Switch(unit);
                    }
                    else
                    {
                        bool result = curr_agent.CanKill(tgt_piece.CurrPos);
                        if (result == true)
                        {
                            Context.Event.Run(Define.Kill, CurrUnit, unit as ChessSceneUnit);
                            HidePathPoint();
                        }
                        else
                        {
                            Switch(unit);
                        }
                    }
                }
            }

        }

        private void Deselect(SceneUnit unit)
        {
            unit.ConfirmSelect(false);

            if (CurrUnit == unit)
            {
                CurrUnit = null;
            }
            HidePathPoint();
        }

        private void ShowPathPoint()
        {
            var path_point_mgr = Define.BoardET.GetComponent<PathPointManager>();
            path_point_mgr.ShowPathPoint(CurrUnit);
        }

        private void HidePathPoint()
        {
            var path_point_mgr = Define.BoardET.GetComponent<PathPointManager>();
            path_point_mgr.HidePathPoint();
        }
        
    }
}
