﻿using ETT.Client.Common;
using ETT.Client.Scene;
using ETT.Client.Table;
using ETT.Model;
using ETT.Model.network;

namespace ETT.Client.ZhongGuoXiangQi
{
    public class LobbyComponent : LogicComponent, IAwake, IModuleComponent
    {
        public void Awake()
        {
            SceneMgr.OnSwitchFinish += OnSceneLoadCompleteHnadler;

            Log.Debug("LobbyComponent Awake");
        }

        public void Enter()
        {
            uint scene_id = (uint)EGenericSceneTable.scene_lobby;
            uint model_id = (uint)EGenericModelTable.Elven;
            SceneMgr.Switch(scene_id, model_id);
        }

        public void Exit()
        {

        }

#if ONLINE_MODE
        private async void OnSceneLoadCompleteHnadler(GenericScene src, GenericScene tgt)
        {
            if (tgt != null && tgt.ConfId == (uint)EGenericSceneTable.scene_lobby)
            {
                Session session = NetworkMgr.Get(NetworkMgr.GateAddr);
                G2C_Res_EnterMap res = (G2C_Res_EnterMap)await session.Call(new C2G_Req_EnterMap() { });
                if (res.Error == (int)ECommonErrCodeTable.Success)
                {
                    NetworkMgr.ActorId = res.UnitId;
                    WndMgr.Show((int)EGenericWndTable.Lobby);
                }
                else
                {
                    ShowErrorCode(res.Error);
                }
            }
        }
#else
        private void OnSceneLoadCompleteHnadler(GenericScene src, GenericScene tgt)
        {
            if (tgt != null && tgt.ConfId == (uint)EGenericSceneTable.scene_lobby)
            {
                WndMgr.Show((int)EGenericWndTable.Lobby);
            }
        }
#endif

    }
}
