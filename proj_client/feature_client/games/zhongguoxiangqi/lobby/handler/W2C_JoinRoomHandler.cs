﻿using ETT.Client.Common;
using ETT.Client.ZhongGuoXiangQi.Lobby;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;

namespace ETT.Client.ZhongGuoXiangQi
{

    [MessageHandler(EPeerType.Client)]
    public class W2C_JoinRoomHandler : AbstractMsgHandler<W2C_Ntf_JoinRoom>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_JoinRoom msg)
        {
            UIEventUtils.Run(MatchWnd.ShowApplierEvt, msg.Applier.PlayerId, msg.Applier.PlayerName);

            await ETTask.CompletedTask;
        }
    }
}
