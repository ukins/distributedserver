using ETT.Model;
using ETT.Model.proto;
using System.IO;
using System.Collections.Generic;

namespace ETT.Client.WuZiQi
{

	/// <summary>
	/// 放置棋子 【Client, World】
	/// </summary>
	[Message((uint)EBattleProto.C2W_Req_Place)]
	public class C2W_Req_Place : AbstractLocationRequest
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 放置棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Res_Place)]
	public class W2C_Res_Place : AbstractLocationResponse
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 放置棋子 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Place)]
	public class W2C_Ntf_Place : AbstractLocationMessage
	{
		public uint chessid {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(chessid);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			chessid = reader.ReadUInt32();
		}
	}

	/// <summary>
	/// 通知顺序 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_Turn)]
	public class W2C_Ntf_Turn : AbstractLocationMessage
	{
		public bool isOnTurn {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(isOnTurn);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			isOnTurn = reader.ReadBoolean();
		}
	}

	/// <summary>
	/// 游戏结束 【World, Client】
	/// </summary>
	[Message((uint)EBattleProto.W2C_Ntf_GameOver)]
	public class W2C_Ntf_GameOver : AbstractLocationMessage
	{
		public int faction {get; set;}

		public override void Pack(BinaryWriter writer)
		{
			base.Pack(writer);
			writer.Write(faction);
		}

		public override void UnPack(BinaryReader reader)
		{
			base.UnPack(reader);
			faction = reader.ReadInt32();
		}
	}
}
