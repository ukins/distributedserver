using ETT.Client.Table;
using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Client.WuZiQi
{
	public class ChessmanTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Type { get; private set; }

		public UVector2Int BirthPoint { get; private set; }

		public uint Model { get; private set; }

		private ModelTableConf m_model_conf;
		public ModelTableConf ModelConf
		{
			get
			{
				if (m_model_conf == null)
				{
					m_model_conf = Context.Table.GetComponent<ModelTable>()[Model];
				}
				return m_model_conf;
			}
		}

		public UVector3 Collider { get; private set; }

		public UVector3 ColliderOffset { get; private set; }

		public UVector3Int Forward { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Type = br.ReadString();

				BirthPoint = UVector2Int.Parse(br.ReadString());

				Model = br.ReadUInt32();
				m_model_conf = null;

				Collider = UVector3.Parse(br.ReadString());

				ColliderOffset = UVector3.Parse(br.ReadString());

				Forward = UVector3Int.Parse(br.ReadString());
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class ChessmanTable : SingleKeyTable<ChessmanTableConf,uint>
	{
		public override string Name { get { return "ChessmanTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"wuziqi/battle/table/chessman",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='ChessmanTable' isseparate='true' >
						<field name='Id' type='uint' primarykey='true' desc='棋子索引' />
						<field name='Name' type='string' desc='棋子名称' />
						<field name='Type' type='string' desc='棋子类型' />
						<field name='BirthPoint' type='vector2int' desc='棋子类型' />
						<field name='Model' type='uint' foreigntable='ModelTable' desc='模型' />
						<field name='Collider' type='vector3' desc='包围盒' />
						<field name='ColliderOffset' type='vector3' desc='包围盒偏移' />
						<field name='Forward' type='vector3int' desc='默认正向朝向' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
