﻿using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.WuZiQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_DealJoinHandler : AbstractMsgHandler<W2C_Ntf_DealJoin>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_DealJoin msg)
        {
            string str = string.Empty;
            if (msg.IsAccept == true)
            {
                str = UIUtils.Localize("{0}接受了您的请求");
            }
            else
            {
                str = UIUtils.Localize("{0}拒绝了您的请求");
            }
            str = string.Format(str, msg.Accepter.PlayerName);
            UIEventUtils.Run(GenericTipsWnd.ShowTipsEvent, str);

            await ETTask.CompletedTask;
        }
    }
}
