﻿using ETT.AI.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.WuZiQi.Battle
{
    [Event(Define.Exe_Place)]
    public class Execute_PlaceEventHandler : IEvent<UVector2Int>
    {
        protected NetworkOuterExComponent NetworkMgr => Context.Game.GetComponent<NetworkOuterExComponent>();

        public void Run(UVector2Int pos)
        {
            RunAsync(pos).Coroutine();
        }

        private Vector3 TranslateVec(UVector2Int pos)
        {
            var footholdtable = Context.Table.GetComponent<FootholdTable>();
            var conf = footholdtable[pos];
            return new Vector3(conf.Axis.X, 0, conf.Axis.Y);
        }

        private async ETVoid RunAsync(UVector2Int pos)
        {
            //var chess_piece = unit.GetParent<ChessPieceEntity>();

            //var agentMgr = chess_piece.GetComponent<ChessPieceComponent>();

            ////播放动画
            //var animMgr = chess_piece.GetComponent<AnimateComponent>();
            //animMgr.Move();

            ////位移
            //var moveMgr = chess_piece.GetComponent<MovableComponent>();
            //await moveMgr.MoveAsync(TranslateVec(pos));

            //animMgr.Stop();

            ////动画播放完成之后设置数据
            //agentMgr.Move(pos);

            ////调整朝向
            ////unit.transform.DOLocalMove(new Vector3(foothold.Axis.X, 0, foothold.Axis.Y),
            ///1);
            ///

            SceneUnitComponent unitMgr = Context.Game.GetComponent<SceneUnitComponent>();
            var boardET = Define.BoardET;
            var chessET = boardET.Root.Factory.CreateWithId<ChessPieceEntity>();
            var table = Context.Table.GetComponent<ChessmanTable>();
            var faction = Define.MineFaction;
            var conf = table.GenericFindVO((uint)faction);
            boardET.Add(Define.GetId(pos), chessET);

            var chess_piece = chessET.AddComponent<ChessPieceComponent, ChessmanTableConf>(conf);
            chessET.Board = boardET;
            chessET.IsAvailable = true;
            chess_piece.Move(pos);

            var mgr = boardET.GetComponent<PathPointManager>();
            mgr.RemovePathPoint(pos);

            var unit = chessET.AddComponent<ChessSceneUnit, ChessmanTableConf>(conf);
            unit.SelectEvt = Define.Select;
            await unit.LoadModel();
            unitMgr.Add(unit);
            unit.transform.localPosition = TranslateVec(pos);

            chessET.AddComponent<AnimateComponent, Transform>(unit.ModelGO.transform.GetChild(0));
        }
    }
}
