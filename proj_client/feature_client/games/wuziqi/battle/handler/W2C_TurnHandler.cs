﻿using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.WuZiQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_TurnHandler : AbstractMsgHandler<W2C_Ntf_Turn>
    {
        protected override async ETTask Run(Session session, W2C_Ntf_Turn msg)
        {
            Define.isOnTurn = msg.isOnTurn;

            UIEventUtils.Run(BattleWnd.OnTurnEvent);

            var mgr = Define.BoardET.GetComponent<PathPointManager>();
            if (Define.isOnTurn == true)
            {
                mgr.ShowPathPoint();
            }
            else
            {
                mgr.HidePathPoint();
            }

            await ETTask.CompletedTask;
        }
    }
}
