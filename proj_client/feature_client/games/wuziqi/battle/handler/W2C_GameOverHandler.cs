﻿using ETT.AI.WuZiQi;
using ETT.Client.Common;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Model.network;
using ETT.Model.proto;
using ETT.Model.server;

namespace ETT.Client.WuZiQi.Battle
{
    [MessageHandler(EPeerType.Client)]
    public class W2C_GameOverHandler : AbstractMsgHandler<W2C_Ntf_GameOver>
    {
        private TimeComponent TimeMgr => Context.Game.GetComponent<TimeComponent>();
        protected SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();


        protected override async ETTask Run(Session session, W2C_Ntf_GameOver msg)
        {
            Define.isGameOver = true;
            Define.WinFaction = (EChessPieceFaction)msg.faction;

            var str = Define.WinFaction == Define.MineFaction ? UIUtils.Localize("胜利") : UIUtils.Localize("失败");
            UIEventUtils.Run(GenericTipsWnd.ShowTipsEvent, str);

            await Context.Game.GetComponent<TimeComponent>().WaitAsync(5 * 1000);

            Define.VO = null;
            Define.StartTime = 0;
            Define.isOnTurn = false;
            Define.MineFaction = EChessPieceFaction.INVALID;

            var select = Context.Game.GetComponent<SelectGameComponent>();
            var model = Context.Game.GetComponent<RenderModelComponent>();
            var model_id = (uint)EGenericModelTable.Elven;

            select.GameOver();

            model.SetActive(model_id, true);

            select.SwitchModule<LobbyComponent>();

            await ETTask.CompletedTask;
        }
    }
}
