﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.WuZiQi
{
    public class BattleComponent : LogicComponent, IAwake, IModuleComponent
    {
        public void Awake()
        {
        }

        public void Enter()
        {
            uint scene_id = (uint)EGenericSceneTable.scene_battle;
            uint model_id = (uint)EGenericModelTable.Elven;
            SceneMgr.Switch(scene_id, model_id);
        }

        public void Exit()
        {

        }
    }
}
