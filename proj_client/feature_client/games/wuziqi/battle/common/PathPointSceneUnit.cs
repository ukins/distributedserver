﻿using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.WuZiQi
{
    public class PathPointSceneUnit : SceneUnit, IStart
    {
        private GameObject m_container;
        private GameObject m_foot;
        private GameObject m_head;

        private static int m_cnt = 0;

        public void Start()
        {
            m_container = GameObject.Find("root/guide_container");
            if (m_container == null)
            {
                throw new Exception("Error! 路径点位的挂点不存在");
            }
        }

        protected override void OnLoadComplete()
        {
            base.OnLoadComplete();
            gameObject.name = $"path_point_{++m_cnt}";
            UIUtils.SetLayer(gameObject, m_container.layer);
            UIUtils.SetParent(gameObject, m_container);
            m_foot = UIUtils.FindChild(ModelGO, "foot");
            m_head = UIUtils.FindChild(ModelGO, "head");
            UIUtils.SetActive(m_head, false);
        }

        public void SetInfo(UVector2Int vec, Vector3 pos)
        {
            Pos = vec;

            transform.localPosition = pos;
            transform.localRotation = Quaternion.Euler(Vector3.zero);

            UIUtils.SetActive(m_foot, true);
            UIUtils.SetActive(m_head, false);

            var collider = gameObject.GetComponentInChildren<BoxCollider>();
            collider.enabled = true;
        }
    }
}
