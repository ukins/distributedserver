﻿using ETT.Client.Common;
using ETT.Client.Scene;
using ETT.Model;
using ETT.Model.network;
using System;

namespace ETT.Client.WuZiQi
{
    public class LoginComponent : LogicComponent, IAwake, IModuleComponent
    {
        public Session GateSession { get; set; }

        public void Awake()
        {
            SceneMgr.OnSwitchFinish += OnSceneLoadCompleteHnadler;

            Log.Debug("LoginComponent Awake");
        }

        public void Enter()
        {
            uint scene_id = (uint)EGenericSceneTable.scene_login;
            uint model_id = (uint)EGenericModelTable.Elven;
            var render = Context.Game.GetComponent<RenderModelComponent>();
            render.LoadModel(model_id);
            SceneMgr.Switch(scene_id, model_id);
        }

        public void Exit()
        {

        }

        private void OnSceneLoadCompleteHnadler(GenericScene src, GenericScene tgt)
        {
            if (tgt != null && tgt.ConfId == (uint)EGenericSceneTable.scene_login)
            {
                WndMgr.Show((int)EGenericWndTable.Login);
            }
        }
    }
}
