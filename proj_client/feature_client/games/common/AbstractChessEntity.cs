﻿using ETT.Client.Common;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public abstract class AbstractChessEntity : Entitas, IRootComponent, IChessRoot
    {
        private static int m_instance_id = 0;
        protected SceneComponent SceneMgr => Context.Game.GetComponent<SceneComponent>();
        protected WndComponent WndMgr => Context.Game.GetComponent<WndComponent>();

        public int RootId { get; set; }

        public FactoryEntity Factory { get; }

        protected IModuleComponent CurrModule;

        public AbstractChessEntity()
        {
            RootId = ++m_instance_id;

            Factory = new FactoryEntity(this);
        }

        public abstract ECommonGameKindTable Kind { get; }

        public abstract void Enter();

        public abstract void Exit();

        public void SwitchModule<T>()
            where T : Model.Component, IModuleComponent
        {
            CurrModule?.Exit();

            CurrModule = GetComponent<T>() as IModuleComponent;

            CurrModule?.Enter();
        }

        public abstract void GameOver();
    }
}
