namespace ETT.Client.Common
{
	public enum ECommonGameKindTable
	{
		//中国象棋
		ZhongGuoXiangQi = 1,
		//斗兽棋
		DouShouQi = 2,
		//五子棋
		WuZiQi = 3,
		//飞行棋
		FeiXingQi = 4,
		//跳棋
		TiaoQi = 5,
		//军棋
		JunQi = 6,
		//国际象棋
		GuoJiXiangQi = 7,
		//围棋
		WeiQi = 8,
	}

}
