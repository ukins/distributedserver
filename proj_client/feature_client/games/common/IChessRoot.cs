﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Common
{
    public interface IChessRoot
    {
        ECommonGameKindTable Kind { get; }

        void Enter();

        void Exit();

        void SwitchModule<T>()
            where T : Component, IModuleComponent;

        void GameOver();
    }
}
