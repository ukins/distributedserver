﻿using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Common
{
    public class SelectGameComponent : LogicComponent, IAwake
    {
        private Dictionary<ECommonGameKindTable, IChessRoot> m_dict = new Dictionary<ECommonGameKindTable, IChessRoot>();

        private IChessRoot curr_game;

        public Entitas CurrChess => curr_game as Entitas;

        private void AddRoot<T>()
            where T : Component, IChessRoot
        {
            IChessRoot root = Root.Factory.CreateWithParent<T>(this);
            m_dict.Add(root.Kind, root);
        }

        public void Awake()
        {
            AddRoot<ZhongGuoXiangQi.ChessRoot>();
            AddRoot<DouShouQi.ChessRoot>();
            AddRoot<FeiXingQi.ChessRoot>();
            AddRoot<WuZiQi.ChessRoot>();
            AddRoot<TiaoQi.ChessRoot>();

            WndMgr.Show((int)ECommonWndTable.Tips);
            WndMgr.Show((int)ECommonWndTable.SelectGame);

        }

        public void Select(ECommonGameKindTable kind)
        {
            if (curr_game != null)
            {
                curr_game.Exit();
            }
            if (m_dict.ContainsKey(kind) == false)
            {
                UIEventUtils.Run(GenericTipsWnd.ShowTipsEvent, Localize("当前游戏尚未开放"));
                return;
            }
            curr_game = m_dict[kind];

            WndMgr.SwitchWndRoot(curr_game as IRootComponent);

            curr_game.Enter();

            WndMgr.Hide((int)ECommonWndTable.SelectGame);
        }
        public void SwitchModule<T>()
            where T : Model.Component, IModuleComponent
        {
            curr_game?.SwitchModule<T>();
        }

        public void GameOver()
        {
            curr_game?.GameOver();
        }
    }
}
