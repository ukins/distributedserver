﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public interface IModuleComponent
    {
        void Enter();

        void Exit();
    }
}
