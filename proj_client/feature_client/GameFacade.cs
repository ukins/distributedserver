﻿using ETT.Client.Common;
using ETT.Model;
using ETT.Model.server;
using System;
using System.Threading;

namespace ETT.Client
{
    public class GameFacade : Entitas, IRootComponent, IAbstractFacade
    {
        public int RootId { get; set; }

        public FactoryEntity Factory { get; }

        public GameFacade()
        {
            Root = this;
            Factory = new FactoryEntity(this);
        }

        public void Init()
        {
            StartAsync();
        }

        private void StartAsync()
        {
            try
            {
                Log.Init(new ClientLogAdapter());

                SynchronizationContext.SetSynchronizationContext(ThreadSync.Instance);

                Context.Event.Add(DllType.Model, typeof(Context).Assembly);
                Context.Event.Add(DllType.Client, typeof(GameFacade).Assembly);

                Context.Game.AddComponent<CommandComponent>();
                Context.Game.AddComponent<ResourcesComponent>();
                Context.Game.AddComponent<UIEventComponent>();
                Context.Game.AddComponent<ProtoComponent, bool>(false);
                Context.Game.AddComponent<TimeComponent>();
                Context.Game.AddComponent<NetworkOuterExComponent>();
                Context.Game.AddComponent<MessageDispatchComponent>();
                Context.Game.AddComponent<TableComponent>();
                Context.Game.AddComponent<YamlComponent>();
                Context.Game.AddComponent<SceneUnitComponent>();
                Context.Game.AddComponent<RenderModelComponent>();

                Context.Game.AddComponent<SceneComponent>();
                Context.Game.AddComponent<WndComponent>();

                Context.Game.AddComponent<SelectGameComponent>();

            }
            catch (Exception e)
            {
                Log.Error(e);
            }
        }

        public void Update()
        {
            Context.Event.Update();
            ThreadSync.Instance.Update();
        }

        public void LateUpdate()
        {
            Context.Event.LateUpdate();
        }

        public void Destroy()
        {
            Context.Dispose();
        }
    }
}
