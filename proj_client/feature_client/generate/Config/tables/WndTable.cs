using ETT.Model;
using ETT.Table;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;

namespace ETT.Client.Table
{
	public class WndTableConf : SingleKeyItem<uint>
	{
		public override uint PrimaryKey => Id;

		public uint Id { get; private set; }

		public string Name { get; private set; }

		public string Desc { get; private set; }

		public string LuaFile { get; private set; }

		public string ResPath { get; private set; }

		public string[] Preload { get; private set; }

		public uint[] DependScene { get; private set; }

		public ESortingLayer Layer { get; private set; }

		public ECacheType CacheType { get; private set; }

		public uint RelyWnd { get; private set; }

		public bool IsPopup { get; private set; }

		public override bool Deserialize(BinaryReader br)
		{
			try
			{
				Id = br.ReadUInt32();

				Name = br.ReadString();

				Desc = br.ReadString();

				LuaFile = br.ReadString();

				ResPath = br.ReadString();

				int m_preload_cnt = br.ReadInt32();
				Preload = new string[m_preload_cnt];
				for (int i = 0; i < m_preload_cnt; ++i)
				{
					Preload[i] = br.ReadString();
				}

				int m_dependscene_cnt = br.ReadInt32();
				DependScene = new uint[m_dependscene_cnt];
				for (int i = 0; i < m_dependscene_cnt; ++i)
				{
					DependScene[i] = br.ReadUInt32();
				}

				Layer = (ESortingLayer)Enum.Parse(typeof(ESortingLayer), br.ReadString());

				CacheType = (ECacheType)Enum.Parse(typeof(ECacheType), br.ReadString());

				RelyWnd = br.ReadUInt32();

				IsPopup = br.ReadBoolean();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

	}

	public class WndTable : SingleKeyTable<WndTableConf,uint>
	{
		public override string Name { get { return "WndTable"; } }

		private readonly List<string> m_path = new List<string>()
		{
			@"common/common/table/wnd",
			@"zhongguoxiangqi/generic/table/wnd",
			@"wuziqi/generic/table/wnd",
		};
		public override List<string> Path { get { return m_path; } }

		public override string XmlContent
		{
			get
			{
				return @"<table name='WndTable' >
						<field name='Id' type='uint' primarykey='true' desc='窗口Id' />
						<field name='Name' type='string' desc='窗口名称' />
						<field name='Desc' type='string' desc='描述' />
						<field name='LuaFile' type='string' desc='lua文件名称' />
						<field name='ResPath' type='string' desc='资源路径' />
						<field name='Preload' type='array(string)' desc='预加载资源数组' />
						<field name='DependScene' type='array(uint)' foreigntable='SceneTable' desc='依赖场景' />
						<field name='Layer' type='ESortingLayer' desc='层级' />
						<field name='CacheType' type='ECacheType' desc='缓存类型' />
						<field name='RelyWnd' type='uint' desc='依赖窗口' />
						<field name='IsPopup' type='bool' desc='是否弹出窗口' />
					</table>";
			}
		}

		public override AbstractItem FindVO(string key)
		{
			try
			{
				uint primarykey = key == string.Empty ? 0 : UInt32.Parse(key);
				return GenericFindVO(primarykey);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}

		public override IEnumerable Range(string min, string max)
		{
			try
			{
				uint range1 = min == string.Empty ? 0 : UInt32.Parse(min);
				uint range2 = max == string.Empty ? 0 : UInt32.Parse(max);
				return GenericRange(range1, range2);
			}
			catch (Exception e)
			{
				Log.Error(e);
				return null;
			}
		}
	}
}
