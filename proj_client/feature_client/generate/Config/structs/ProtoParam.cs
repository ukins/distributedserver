using ETT.Model;
using ETT.Table;
using System;
using System.IO;

namespace ETT.Client.Table
{
	public class ProtoParam : AbstractTableStruct
	{
		public string Name { get; private set; }

		public string Type { get; private set; }

		public string Desc { get; private set; }

		public bool Deserialize(BinaryReader br)
		{
			try
			{
				Name = br.ReadString();
				Type = br.ReadString();
				Desc = br.ReadString();
			}
			catch (Exception e)
			{
				Log.Error(e);
				return false;
			}
			return true;
		}

		public static ProtoParam CreateFromBinary(BinaryReader br)
		{
			var obj = new ProtoParam();
			obj.Deserialize(br);
			return obj;
		}
	}
}
