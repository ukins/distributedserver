﻿using ETT.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Widgets
{
    public class UICircularGrid : UIWidget, ICircularGrid
    {
        protected ICircularGrid grid;

        public Action<GameObject, int> update;
        public Action<GameObject, int> select;

        public override string Prefix => "grid";

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            grid = gameObject.GetComponent<ICircularGrid>();
            if (grid == null)
            {
                throw new Exception("UICircularGrid找不到CirculatGrid组件");
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            grid = null;
        }

        public void SetAmount(int cnt)
        {
            grid?.SetAmount(cnt);
        }

        public void SetUpdateFunc(Action<GameObject, int> func)
        {
            grid?.SetUpdateFunc(func);
        }

        public void SetSelectFunc(Action<GameObject, int> func)
        {
            grid?.SetSelectFunc(func);
        }
    }
}
