﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    /// <summary>
    /// 不规则按钮
    /// </summary>
    public class UIIrregulaButton : UIButton, IAwake<GameObject, float>
    {
        public override string Prefix => "ibtn";

        public override void Awake(GameObject t)
        {
            base.Awake(t);

            alphaThreshold = 0.5f;
        }

        public void Awake(GameObject t, float threshold)
        {
            base.Awake(t);

            alphaThreshold = threshold;
        }

        public float alphaThreshold
        {
            get { return button.image.alphaHitTestMinimumThreshold; }
            set { button.image.alphaHitTestMinimumThreshold = value; }
        }
    }
}
