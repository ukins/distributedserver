﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    public class UIButton : UIWidget
    {
        protected Button button;

        protected UILabel label;

        public override string Prefix => "btn";

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            button = gameObject.GetComponent<Button>();
            if (button == null)
            {
                throw new Exception("UIButton找不到Button组件");
            }

            var txt = Find("Text");
            if (txt != null)
            {
                label = Context.Game.Factory.CreateWithParent<UILabel, GameObject>(this, txt);
            }
        }

        public override void Dispose()
        {
            base.Dispose();

            button = null;
            label?.Dispose();
        }

        public string text
        {
            get { return label != null ? label.text : string.Empty; }
            set { if (label != null) { label.text = value; } }
        }

        public bool interactable
        {
            get { return button.interactable; }
            set { button.interactable = value; }
        }
    }
}
