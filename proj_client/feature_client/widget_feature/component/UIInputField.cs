﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    public class UIInputField : UIWidget
    {
        protected InputField inputfield;

        public override string Prefix => "ipt";

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            inputfield = gameObject.GetComponent<InputField>();
            if (inputfield == null)
            {
                throw new Exception("UIInputField找不到InputField组件");
            }
        }
        public override void Dispose()
        {
            base.Dispose();
            inputfield = null;
        }
        public string text
        {
            get { return inputfield.text; }
            set { inputfield.text = value; }
        }

        public void ActivateInputField()
        {
            inputfield.ActivateInputField();
        }
    }
}
