﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    public class UILabel : UIWidget
    {
        protected Text label;

        public override string Prefix => "lbl";

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            label = gameObject.GetComponent<Text>();
            if (label == null)
            {
                throw new Exception("UILable找不到Text组件");
            }
            raycastTarget = false;
        }

        public override void Dispose()
        {
            base.Dispose();
            label = null;
        }

        public string text
        {
            get { return label.text; }
            set { label.text = value; }
        }

        public Color color
        {
            get { return label.color; }
            set { label.color = value; }
        }

        public bool raycastTarget
        {
            get { return label.raycastTarget; }
            set { label.raycastTarget = value; }
        }
    }
}
