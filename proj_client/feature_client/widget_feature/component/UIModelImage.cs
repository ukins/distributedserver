﻿using ETT.Client;
using ETT.Client.Common;
using ETT.Client.Table;
using ETT.Client.ZhongGuoXiangQi;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Widgets
{
    public class UIModelImage : UIWidget
    {
        protected RawImage image;

        public override string Prefix => "mod";

        private RenderModelComponent RenderMgr => Context.Game.GetComponent<RenderModelComponent>();

        private RenderTexture m_texture;

        private GenericRender m_render;

        public override void Awake(GameObject t)
        {
            base.Awake(t);
            image = gameObject.GetComponent<RawImage>();
            if (image == null)
            {
                throw new Exception($"UIImage找不到Image组件");
            }

            raycastTarget = false;

            m_texture = new RenderTexture(Screen.width, Screen.height, 1);
            m_texture.antiAliasing = 8;
        }

        public override void Dispose()
        {
            base.Dispose();
            image.texture = null;
            m_render.SetRenderTexture(null);
            m_texture = null;

            SetActive(false);
        }

        public bool raycastTarget
        {
            get { return image.raycastTarget; }
            set { image.raycastTarget = value; }
        }

        public Color color
        {
            get { return image.color; }
            set { image.color = value; }
        }

        public void SetModel(uint id)
        {
            m_render = RenderMgr.Get(id);
            if (m_render == null)
            {
                return;
            }
            m_render.SetActive(true);
            m_render.SetRenderTexture(m_texture);

            image.texture = m_texture;
        }

        public void ClearModel()
        {
            m_render?.SetRenderTexture(null);
        }

    }
}
