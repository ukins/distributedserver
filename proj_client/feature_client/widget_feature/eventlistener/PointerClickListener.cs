﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;

namespace ETT.Widgets
{
    public class PointerClickListener : MonoBehaviour, IPointerClickHandler
    {
        public Action<GameObject> onClick;

        public static PointerClickListener Get(GameObject go)
        {
            PointerClickListener listener = go.GetComponent<PointerClickListener>();
            if (listener == null) listener = go.AddComponent<PointerClickListener>();
            return listener;
        }

        public static PointerClickListener Get(UIWidget widget)
        {
            return Get(widget.gameObject);
        }

        public void OnPointerClick(PointerEventData eventData)
        {
            onClick?.Invoke(gameObject);
        }
    }
}
