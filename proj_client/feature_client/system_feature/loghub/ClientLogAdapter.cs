﻿using ETT.Model.log;

namespace ETT.Client
{
    public class ClientLogAdapter : ILog
    {
        //private readonly Logger logger = LogManager.GetLogger("Client_Logger");
        public void Debug(string message)
        {
            //logger.Debug(message);
            UnityEngine.Debug.Log(message);
        }

        public void Debug(string message, params object[] args)
        {
            //logger.Debug(message, args);
            UnityEngine.Debug.LogFormat(message, args);
        }

        public void Error(string message)
        {
            //logger.Error(message);
            UnityEngine.Debug.LogError(message);
        }

        public void Error(string message, params object[] args)
        {
            //logger.Error(message, args);
            UnityEngine.Debug.LogErrorFormat(message, args);
        }

        public void Fatal(string message)
        {
            //logger.Fatal(message);
            UnityEngine.Debug.LogError(message);
        }

        public void Fatal(string message, params object[] args)
        {
            //logger.Fatal(message, args);
            UnityEngine.Debug.LogErrorFormat(message, args);
        }

        public void Info(string message)
        {
            //logger.Info(message);
            UnityEngine.Debug.Log(message);
        }

        public void Info(string message, params object[] args)
        {
            //logger.Info(message, args);
            UnityEngine.Debug.LogFormat(message, args);
        }

        public void Trace(string message)
        {
            //logger.Trace(message);
            UnityEngine.Debug.Log(message);
        }

        public void Trace(string message, params object[] args)
        {
            //logger.Trace(message, args);
            UnityEngine.Debug.LogFormat(message, args);
        }

        public void Warning(string message)
        {
            //logger.Warn(message);
            UnityEngine.Debug.LogWarning(message);
        }

        public void Warning(string message, params object[] args)
        {
            //logger.Warn(message, args);
            UnityEngine.Debug.LogWarningFormat(message, args);
        }
    }
}
