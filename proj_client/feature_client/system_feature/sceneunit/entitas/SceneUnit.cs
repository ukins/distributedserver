﻿using ETT.Client.Table;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public abstract class SceneUnit : Model.Component, ISceneUnit, IAwake<ModelTableConf>, IAwake<uint>
    {
        protected ResourcesComponent ResMgr => Context.Game.GetComponent<ResourcesComponent>();

        public GameObject gameObject { get; protected set; }
        public Transform transform { get; protected set; }

        public GameObject ModelGO { get; protected set; }
        public ModelTableConf ModelConf { get; protected set; }

        public virtual UVector2Int Pos { get; protected set; }

        public string SelectEvt = string.Empty;

        public void Awake(ModelTableConf conf)
        {
            ModelConf = conf ?? throw new Exception("Error! Model Conf Is Null");

            CreateShell();
        }

        public void Awake(uint model_id)
        {
            ModelTableConf conf = Context.Table.GetComponent<ModelTable>()[model_id];
            Awake(conf);
        }

        protected virtual void CreateShell()
        {
            if (gameObject == null)
            {
                gameObject = new GameObject();
            }

            transform = gameObject.transform;
        }


        public async ETTask LoadModel()
        {
            ModelGO = await ResMgr.GetInstantiate(ModelConf.Path);

            ModelGO.GetComponent<ISceneUnitSelect>().Unit = this;

            CreateMgr();

            OnLoadComplete();
        }

        protected virtual void OnLoadComplete()
        {
            UIUtils.SetLayer(ModelGO, gameObject.layer);
            UIUtils.SetParent(ModelGO, gameObject);
            UIUtils.SetLocalScale(ModelGO, ModelConf.Scale);
        }

        protected virtual void CreateMgr()
        {
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }
            base.Dispose();
            ResMgr.Recycle(ModelGO);

            GameObject.Destroy(gameObject);
        }

        public void SetActive(bool active)
        {
            if (gameObject.activeSelf == active)
            {
                return;
            }
            gameObject.SetActive(active);
        }

        public virtual void ConfirmSelect(bool state)
        {
            ModelGO.GetComponent<ISceneUnitSelect>().ConfirmSelect(state);
        }
    }
}
