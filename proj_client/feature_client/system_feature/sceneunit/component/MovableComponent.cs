﻿using ETT.AI.Common;
using ETT.Model;
using System.Collections.Generic;
using UnityEngine;

namespace ETT.Client
{
    public class MovableComponent : Model.Component, IAwake, IUpdate
    {
        private struct Node
        {
            public long Id { get; set; }
            public Transform Trans { get; set; }
            public Vector3 Pos { get; set; }
            public ETTaskCompletionSource<object> tcs { get; set; }
        }

        private static long m_generator_id = 0;

        private SortedDictionary<long, Node> m_dict = new SortedDictionary<long, Node>();
        private Queue<Node> m_queue = new Queue<Node>();

        public void Awake()
        {
        }

        public void Update()
        {
            if (m_dict.Count == 0)
            {
                return;
            }

            foreach (var node in m_dict.Values)
            {
                var dis = node.Trans.localPosition - node.Pos;
                dis.y = 0;
                if (dis.magnitude < 0.1f)
                {
                    m_queue.Enqueue(node);
                }
                else
                {
                    node.Trans.localPosition -= dis.normalized * Time.deltaTime * 1.5f;
                }
            }

            while (m_queue.Count > 0)
            {
                var node = m_queue.Dequeue();
                m_dict.Remove(node.Id);
                node.tcs.SetResult(null);
            }
        }

        public ETTask MoveAsync(Vector3 vec)
        {
            var chess_piece = GetParent<ChessPieceEntity>();
            var unit = chess_piece.GetComponentInHerit<SceneUnit>();
            vec.y = 0;

            ETTaskCompletionSource<object> tcs = new ETTaskCompletionSource<object>();
            Node node = new Node { Id = ++m_generator_id, Trans = unit.transform, Pos = vec, tcs = tcs };
            m_dict[node.Id] = node;

            return tcs.Task;
        }

        public ETTask NearAsync(Vector3 vec)
        {
            var chess_piece = GetParent<ChessPieceEntity>();
            var unit = chess_piece.GetComponentInHerit<SceneUnit>();

            Vector3 cur = unit.transform.localPosition;

            Vector3 dir = vec - cur;
            dir.y = 0;

            Vector3 tgt = cur + dir.normalized * (dir.magnitude - 2);

            ETTaskCompletionSource<object> tcs = new ETTaskCompletionSource<object>();
            Node node = new Node { Id = ++m_generator_id, Trans = unit.transform, Pos = tgt, tcs = tcs };
            m_dict[node.Id] = node;

            return tcs.Task;
        }
    }
}
