﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public interface ISceneUnitSelect
    {
        SceneUnit Unit { get; set; }

        void ConfirmSelect(bool state);
    }
}
