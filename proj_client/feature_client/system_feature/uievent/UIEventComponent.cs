﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Reflection;

namespace ETT.Client
{
    using UIEventDict = Dictionary<string, Dictionary<long, UIEventComponent.MethodWrap>>;

    public class UIEventComponent : Component, IAwake
    {
        public class MethodWrap
        {
            public MethodInfo Method { get; private set; }
            public object Owner { get; private set; }

            public MethodWrap(MethodInfo methodInfo, object obj)
            {
                Method = methodInfo;
                Owner = obj;
            }
        }
        private UIEventDict m_method_dict = new UIEventDict();

        public void Awake()
        {
            Context.Event.OnAddCom += OnAddComponentHandler;
            Context.Event.OnDelCom += OnDelComponentHandler;
        }

        private void OnAddComponentHandler(Component obj)
        {
            Type type = obj.GetType();
            var attr = type.GetCustomAttribute<UIEventLabelAttribute>();
            if (attr == null)
            {
                return;
            }

            var methodinfos = type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (var m in methodinfos)
            {
                var attrs = m.GetCustomAttributes(false);
                if (attrs.Length == 0)
                {
                    continue;
                }

                for (int i = 0; i < attrs.Length; ++i)
                {
                    if (attrs[i] is UIEventHandlerAttribute attribute)
                    {
                        if (m_method_dict.ContainsKey(attribute.Type) == false)
                        {
                            m_method_dict.Add(attribute.Type, new Dictionary<long, MethodWrap>());
                        }
                        var dict = m_method_dict[attribute.Type];
                        MethodWrap wrap = new MethodWrap(m, obj);
                        if (dict.ContainsKey(obj.InstanceId) == true)
                        {
                            Log.Error($"同一个类中不能定义两个相同事件类型的事件处理方法 class={type.Name}; Event = {attribute.Type}");
                        }
                        else
                        {
                            dict.Add(obj.InstanceId, wrap);
                        }
                    }
                }
            }
        }

        private void OnDelComponentHandler(Component obj)
        {
            Type type = obj.GetType();
            var attr = type.GetCustomAttribute<UIEventLabelAttribute>();
            if (attr == null)
            {
                return;
            }

            var methodinfos = type.GetMethods(BindingFlags.DeclaredOnly | BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
            foreach (var m in methodinfos)
            {
                var attrs = m.GetCustomAttributes(false);
                if (attrs.Length == 0)
                {
                    continue;
                }

                if (attrs[0] is UIEventHandlerAttribute attribute)
                {
                    if (m_method_dict.ContainsKey(attribute.Type) == false)
                    {
                        continue;
                    }
                    var dict = m_method_dict[attribute.Type];
                    dict.Remove(obj.InstanceId);
                }
            }
        }

        private void RunWithParam(string event_type, object[] paramArr)
        {
            if (m_method_dict.ContainsKey(event_type) == false)
            {
                Log.Error($"事件【{event_type}】不存在对应的处理Method");
                return;
            }

            var dict = m_method_dict[event_type];
            foreach (var pair in dict)
            {
                var m = pair.Value;
                m.Method?.Invoke(m.Owner, paramArr);
            }
        }

        public void Run(string event_type)
        {
            RunWithParam(event_type, null);
        }

        public void Run<T1>(string event_type, T1 param1)
        {
            object[] paramArr = new object[1];
            paramArr[0] = param1;

            RunWithParam(event_type, paramArr);
        }

        public void Run<T1, T2>(string event_type, T1 param1, T2 param2)
        {
            object[] paramArr = new object[2];
            paramArr[0] = param1;
            paramArr[1] = param2;

            RunWithParam(event_type, paramArr);
        }

        public void Run<T1, T2, T3>(string event_type, T1 param1, T2 param2, T3 param3)
        {
            object[] paramArr = new object[3];
            paramArr[0] = param1;
            paramArr[1] = param2;
            paramArr[2] = param3;

            RunWithParam(event_type, paramArr);
        }
    }
}
