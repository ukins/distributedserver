﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client
{
    public static class UIEventUtils
    {
        private static UIEventComponent UIEventMgr => Context.Game.GetComponent<UIEventComponent>();

        public static void Run(string event_type)
        {
            UIEventMgr.Run(event_type);
        }

        public static void Run<T1>(string event_type, T1 param1)
        {
            UIEventMgr.Run(event_type, param1);
        }

        public static void Run<T1, T2>(string event_type, T1 param1, T2 param2)
        {
            UIEventMgr.Run(event_type, param1, param2);
        }

        public static void Run<T1, T2, T3>(string event_type, T1 param1, T2 param2, T3 param3)
        {
            UIEventMgr.Run(event_type, param1, param2, param3);
        }
    }
}
