﻿using ETT.Client.Common;
using ETT.Client.Res;
using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace ETT.Client.Scene
{
    public class GenericScene : Entitas, IUpdate
    {
        private class AsyncLoadedComplete : ILoadAsync
        {
            public bool isDone => true;
            public float progress => 1;
        }
        private static AsyncLoadedComplete Complete = new AsyncLoadedComplete();

        private uint m_sceneid = 0;
        public virtual uint ConfId => m_sceneid;

        private WndComponent WndMgr => Context.Game.GetComponent<WndComponent>();
        private ResourcesComponent ResMgr => Context.Game.GetComponent<ResourcesComponent>();
        //事件
        public Action<GenericScene> OnEnterBegin;
        public Action<GenericScene> OnEnterFinish;
        public Action<GenericScene> OnExitBegin;
        public Action<GenericScene> OnExitFinish;

        public SceneTableConf Conf { get; private set; }

        private ILoadAsync m_async = null;

        private float m_curr_value;
        private float m_target_value;

        public float ProgressValue => m_curr_value;

        public bool IsOpen { get; protected set; }

        private Action m_CheckFuncPointer;

        private GameObject gameObject;


        public GenericScene(uint sceneid)
        {
            if (ConfId == 0)
            {
                m_sceneid = sceneid;
            }

            Conf = Context.Table.GetComponent<SceneTable>()[ConfId];
            if (Conf == null)
            {
                throw new Exception($"场景配置无效，Confid = {ConfId}");
            }
        }

        public void Enter()
        {
            OnEnterBegin?.Invoke(this);
            IsOpen = true;
            m_curr_value = 0;
            m_target_value = 0;
            SceneManager.LoadScene(Conf.Name);

            if (string.IsNullOrWhiteSpace(Conf.ResPath) == false)
            {
                LoadScenePrefab().Coroutine();
                m_async = ResMgr.GetLoadAsync(Conf.ResPath);
            }
            else
            {
                m_async = Complete;
            }

            m_CheckFuncPointer = CheckSceneIsComplete;
        }

        private async ETVoid LoadScenePrefab()
        {
            gameObject = await ResMgr.GetInstantiate(Conf.ResPath);
        }

        public void Update()
        {
            m_CheckFuncPointer?.Invoke();
        }

        public void Exit()
        {
            OnExitBegin?.Invoke(this);

            if (gameObject != null)
            {
                GameObject.Destroy(gameObject);
            }

            OnExitFinish?.Invoke(this);
            IsOpen = false;
            OnSceneUnloadComplete();
        }

        private void CheckSceneIsComplete()
        {
            if (m_async == null)
            {
                return;
            }
            if (m_async.isDone == true)
            {
                m_target_value = 1;
            }
            else
            {
                m_target_value = m_async.progress;
            }

            if (m_curr_value < m_target_value)
            {
                if (Conf.ManualCloseLoadingWnd == false)
                {
                    m_curr_value += Time.deltaTime;
                }
                else
                {
                    m_curr_value += Time.deltaTime * 0.5f;
                }
            }
            if (m_curr_value >= 1)
            {
                if (Conf.ManualCloseLoadingWnd == false)
                {
                    WndMgr.Hide((int)ECommonWndTable.SceneLoading);
                }
                OnSceneLoadComplete();
                OnEnterFinish?.Invoke(this);
                m_CheckFuncPointer = null;
                m_async = null;
            }
        }

        protected virtual void OnSceneLoadComplete()
        {

        }

        protected virtual void OnSceneUnloadComplete()
        {

        }
    }
}
