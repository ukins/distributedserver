﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Res
{
    public interface ILoadAsync
    {
        bool isDone { get; }

        float progress { get; }
    }
}
