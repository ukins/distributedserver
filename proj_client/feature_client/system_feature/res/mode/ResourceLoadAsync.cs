﻿using ETT.Model;
using UnityEngine;

namespace ETT.Client.Res
{
    public class ResourceLoadAsync : Model.Component, IUpdate, ILoadAsync
    {
        private ResourceRequest request;

        private ETTaskCompletionSource<Object> tcs;

        private string path;

        private int cnt = 0;

        public bool isDone { get; private set; }

        public float progress { get; private set; }

        public void Update()
        {
            progress = request.progress;

            if (request.isDone == false)
            {
                return;
            }

            if (request.asset != null)
            {
                isDone = true;
                ETTaskCompletionSource<Object> t = tcs;
                t.SetResult(request.asset);
            }
            else
            {
                if (cnt <= 0)
                {
                    Log.Error($"资源加载失败 {path}");
                    ETTaskCompletionSource<Object> t = tcs;
                    t.SetResult(null);
                }
                else
                {
                    cnt--;
                    request = Resources.LoadAsync(path);
                }
            }
        }

        public ETTask<Object> LoadAsync(string path)
        {
            isDone = false;
            progress = 0;

            cnt = 3;
            this.path = path;
            tcs = new ETTaskCompletionSource<Object>();
            request = Resources.LoadAsync(path);
            return tcs.Task;
        }

    }
}
