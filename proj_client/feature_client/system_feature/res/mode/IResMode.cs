﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Res
{
    public interface IResMode
    {
        AssetVO GetAsset(string path);

        ETTask<AssetVO> LoadAync(string path);

        ILoadAsync GetLoadAsync(string path);
    }
}
