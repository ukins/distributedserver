﻿using ETT.Client.Res;
using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class ResourcesComponent : Model.Component, IAwake
    {
        private IResMode m_resmode;

        public void Awake()
        {
            if (GlobalDefine.ResourcesMode == true)
            {
                m_resmode = Context.Game.Factory.Create<ResourcesMode>();
                //m_resmode = GameObject.Find("UIRoot").GetComponent<IResMode>();
            }
            else
            {

            }

            Log.Debug("ResourcesComponent Awake");
        }

        public async ETTask<AssetVO> LoadAsync(string path)
        {
            if (string.IsNullOrWhiteSpace(path) == true)
            {
                return null;
            }
            var asset = await m_resmode.LoadAync(path);
            return asset;
        }

        public async ETTask LoadAsync(List<string> path_list)
        {
            if (path_list.Count == 0)
            {
                return;
            }
            for (int i = 0; i < path_list.Count; ++i)
            {
                await m_resmode.LoadAync(path_list[i]);
            }
        }

        private AssetVO GetAsset(string path)
        {
            return m_resmode.GetAsset(path);
        }

        /// <summary>
        /// 用于处理GameObject的引用计数
        /// </summary>
        private Dictionary<int, AssetVO> m_go_dict = new Dictionary<int, AssetVO>();


        public async ETTask<GameObject> GetInstantiate(string path)
        {
            var asset = await m_resmode.LoadAync(path);
            if (asset == null)
            {
                return null;
            }

            var go = GameObject.Instantiate(asset.Asset) as GameObject;

            asset.ReferenceCnt++;
            asset.LastUseTime = TimeUtils.ClientNow();
            m_go_dict.Add(go.GetHashCode(), asset);
            return go;
        }


        public GameObject GetSyncInstantiate(string path)
        {
            var asset = GetAsset(path);
            if (asset == null) { return null; }

            var go = GameObject.Instantiate(asset.Asset) as GameObject;

            asset.ReferenceCnt++;
            asset.LastUseTime = TimeUtils.ClientNow();
            m_go_dict.Add(go.GetHashCode(), asset);

            return go;
        }

        /// <summary>
        /// 调用次数= GetInstantiate()+GetSyncInstantiate()调用次数的总和
        /// </summary>
        /// <param name="go"></param>
        public void Recycle(GameObject go)
        {
            if (go == null) { return; }

            var hashcode = go.GetHashCode();
            m_go_dict.TryGetValue(hashcode, out AssetVO vo);
            if (vo != null)
            {
                vo.ReferenceCnt--;
                m_go_dict.Remove(hashcode);
            }
            GameObject.Destroy(go);
        }

        public async ETTask<Texture2D> GetTexture(string path)
        {
            var asset = await m_resmode.LoadAync(path);
            if (asset == null)
            {
                return null;
            }
            return asset.Asset as Texture2D;
        }

        public ILoadAsync GetLoadAsync(string path)
        {
            if (string.IsNullOrWhiteSpace(path) == true)
            {
                return null;
            }
            return m_resmode.GetLoadAsync(path);
        }

    }
}
