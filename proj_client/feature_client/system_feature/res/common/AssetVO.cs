﻿using ETT.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client
{
    public class AssetVO
    {
        public UnityEngine.Object Asset { get; set; }

        public string Path { get; protected set; }

        public string BundleName { get; protected set; }

        public string AssetName { get; protected set; }

        public int ReferenceCnt { get; set; }

        public float LastUseTime { get; set; }

        public AssetVO(string path)
        {
            Path = path;
            int pos = path.LastIndexOf('/');
            if (pos < 0)
            {
                Log.Error($"Error, AssetName Invalid; path={path}");
            }
            BundleName = path.Substring(0, pos) + ".ab";
            AssetName = path.Substring(pos + 1, path.Length - pos - 1);
        }

        public void Dispose()
        {
            Path = string.Empty;
            BundleName = string.Empty;
            AssetName = string.Empty;
            Resources.UnloadAsset(Asset);
            Asset = null;
        }
    }
}
