﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;

namespace ETT.Client.Res
{
    public enum ELoadAssetState
    {
        //未开始加载
        None,
        //加载中
        Loading,
        //加载完成
        Complete
    }

    public class LoadVO
    {
        public AssetVO Ent { get; set; }

        public ELoadAssetState State { get; set; }

    }
}
