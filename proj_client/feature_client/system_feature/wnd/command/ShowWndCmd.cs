﻿using CommandLine;
using ETT.Model;
using ETT.Model.Command;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Wnd
{
    public class ShowWndOption
    {
        [Option("id", Required = true)]
        public uint WndId { get; set; }
    }

    [CommandOption("showwnd")]
    public class ShowWndCmd : AbstractCmd
    {
        public ShowWndOption Option { get; private set; }

        public override bool Check(string[] args)
        {
            bool result = false;

            Parser.Default.ParseArguments<ShowWndOption>(args)
                .WithNotParsed(error => { result = false; })
                .WithParsed(options => { Option = options; result = true; });

            return result;
        }

        public override void Execute()
        {
            Context.Game.GetComponent<WndComponent>().Show(Option.WndId);
        }
    }
}
