﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ETT.Client.Wnd
{
    [AttributeUsage(AttributeTargets.Field | AttributeTargets.Property)]
    public class UIFieldAttribute : Attribute
    {
        public string Path { get; }

        public UIFieldAttribute(string path)
        {
            Path = path;
        }
    }
}
