﻿using ETT.Client.Common;
using ETT.Widgets;
using System;
using UnityEngine;

namespace ETT.Client.Wnd
{

    /// <summary>
    /// 通用对话框界面
    /// 参数
    /// Title 标题文字
    /// Content 对话框内容文字
    /// OnConfirmFunc 确认按钮点击回调
    /// OnCancelFunc 取消按钮点击回调
    /// ConfirmText 确认按钮文字
    /// CancelText 取消按钮文字
    /// </summary>
    [Window((int)ECommonWndTable.Dialog)]
    public class GenericDialogWnd : AbstractWnd
    {
        public struct WindowParam : IWindowParam
        {
            public string Title;
            public string Content;
            public Action OnConfirmFunc;
            public Action OnCancelFunc;
            public string ConfirmText;
            public string CancelText;
        }

        private WindowParam Param => (WindowParam)WndParam;

#pragma warning disable 0649
        [UIField("Background/MessageBox/Title")]
        private UILabel lbl_title;
        [UIField("Background/MessageBox/Content/Text")]
        private UILabel lbl_content;
        [UIField("Background/MessageBox/ButtonGroup/Sure")]
        private UIButton btn_sure;
        [UIField("Background/MessageBox/ButtonGroup/Cancel")]
        private UIButton btn_cancel;
#pragma warning restore 0649


        protected override void OnInit()
        {
        }

        protected override void OnShow()
        {
            UIEventListener.Get(btn_sure).onClick += OnBtnSureClickHandler;
            UIEventListener.Get(btn_cancel).onClick += OnBtnCancelClickHandler;

            RefreshMain();
        }

        protected override void OnUpdate()
        {
        }

        protected override void OnHide()
        {
            UIEventListener.Get(btn_sure).onClick -= OnBtnSureClickHandler;
            UIEventListener.Get(btn_cancel).onClick -= OnBtnCancelClickHandler;
        }

        protected override void OnDestroy()
        {
        }

        private void RefreshMain()
        {
            lbl_title.text = Param.Title;
            lbl_content.text = Param.Content;

            btn_sure.text = Param.ConfirmText == string.Empty ? Localize("确定") : Param.ConfirmText;
            btn_cancel.text = Param.CancelText == string.Empty ? Localize("取消") : Param.CancelText;

            btn_cancel.SetActive(Param.OnCancelFunc != null);
        }

        #region Event Handler

        private void OnBtnSureClickHandler(GameObject obj)
        {
            Close();
            Param.OnConfirmFunc?.Invoke();
        }

        private void OnBtnCancelClickHandler(GameObject obj)
        {
            Close();
            Param.OnCancelFunc?.Invoke();
        }
        #endregion

    }
}
