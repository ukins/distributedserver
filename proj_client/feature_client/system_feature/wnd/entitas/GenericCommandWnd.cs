﻿using ETT.Client.Common;
using ETT.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;


#region Version Summary
/*----------------------------------------------------------------
// Copyright (C) 2018
// 版权所有。 
//
// 文件名：GenericCommandWnd
// 文件功能描述：
//
// 
// 创建者：名字 (Yanghuixiong)
// 时间：2019/9/22 21:45:05
//
// 修改人：
// 时间：
// 修改说明：
//
// 版本：V1.0.0
//----------------------------------------------------------------*/
#endregion

namespace ETT.Client.Wnd
{
    [Window((int)ECommonWndTable.Command)]
    public class GenericCommandWnd : AbstractWnd
    {
#pragma warning disable 0649
        [UIField("root/InputField")]
        private UIInputField ipt_command;
#pragma warning restore 0649


        protected override void OnShow()
        {

        }

        protected override void OnUpdate()
        {
            if (Input.GetKeyDown(KeyCode.KeypadEnter) == true
                || Input.GetKeyDown(KeyCode.Return) == true)
            {
                ipt_command.text = "";
                ipt_command.ActivateInputField();
            }
        }

        protected override void OnHide()
        {

        }

    }
}
