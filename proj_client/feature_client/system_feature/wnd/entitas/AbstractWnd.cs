﻿using ETT.Client.Table;
using ETT.Model;
using ETT.Widgets;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

namespace ETT.Client.Wnd
{
    public abstract class AbstractWnd : LogicComponent, IAwake<uint>, IUIFieldParser
    {
        public enum EWndState
        {
            Invalid,
            Loading,
            Show,
            Hide,
        }

        public uint ConfId { get; private set; }

        public WndTableConf WndConf { get; protected set; }

        protected GameObject gameObject = null;

        public int sortingorder = 0;

        private Action m_funcpointer = null;

        protected virtual void OnInit() { }

        protected abstract void OnShow();

        protected virtual void OnUpdate() { }

        protected abstract void OnHide();

        protected virtual void OnDestroy() { }

        public void Awake(uint id)
        {
            ConfId = id;
            WndConf = Context.Table.GetComponent<WndTable>()[ConfId];
            if (WndConf == null)
            {
                Log.Error($"WndConf Is Not Exist! id={ConfId}");
            }

        }

        public IWindowParam WndParam { get; set; }

        public EWndState WndState { get; protected set; }

        public void Show()
        {
            if (WndState == EWndState.Show || WndState == EWndState.Loading)
            {
                return;
            }
            if (WndState == EWndState.Hide)
            {
                InnerShow();
                return;
            }
            //LoadComplete = null;
            InnerLoading().Coroutine();
        }
        //public void Focus()
        //{
        //}
        public void Update()
        {
            m_funcpointer?.Invoke();
            if (WndState == EWndState.Show)
            {
                OnUpdate();
            }
        }
        //public void LoseFocus()
        //{
        //}
        public void Hide()
        {
            Log.Debug(string.Format("Hide {0}", WndConf.Name));
            if (WndState == EWndState.Hide)
            {
                return;
            }
            if (WndState == EWndState.Loading)
            {
                WndState = EWndState.Hide;
                return;
            }
            if (WndState == EWndState.Show)
            {
                InnerHide();
                return;
            }
        }

        public override void Dispose()
        {
            if (IsDisposed == true)
            {
                return;
            }

            foreach (var pair in m_widgets)
            {
                pair.Value.Dispose();
            }
            m_widgets.Clear();

            base.Dispose();

            Log.Debug(string.Format("Destroy {0}", WndConf.Name));
            OnDestroy();
            WndState = EWndState.Invalid;
            WndParam = null;

            ResMgr.Recycle(gameObject);
        }

        private async ETVoid InnerLoading()
        {
            WndState = EWndState.Loading;

            if (WndConf.Preload.Length > 0)
            {
                var pre_list = Enumerable.ToList(WndConf.Preload).FindAll(c => c.Trim() != string.Empty);
                if (pre_list.Count > 0)
                {
                    await ResMgr.LoadAsync(pre_list);
                }
                var go = await ResMgr.GetInstantiate(WndConf.ResPath);
                OnLoadResComplete(go);
            }
        }

        private void OnLoadResComplete(GameObject go)
        {
            if (go != null)
            {
                gameObject = go;

                try
                {
                    m_widgets.Clear();
                    UIUtils.ParseUIFields(gameObject, this);
                }
                catch (Exception e)
                {
                    Log.Error(e);
                }

                if (WndState == EWndState.Loading)
                {
                    OnInit();
                    InnerShow();
                }
                else if (WndState == EWndState.Hide)
                {
                    OnInit();
                    InnerShow();
                    InnerHide();
                }
            }
            else
            {
                WndState = EWndState.Invalid;
            }
        }

        private void InnerShow()
        {
            WndState = EWndState.Show;
            GameObject layer = WndMgr.GetLayer(WndConf.Layer);
            gameObject.transform.SetParent(layer.transform);
            UIUtils.SetLayer(gameObject, layer.layer);

            gameObject.SetActive(true);

            Canvas canvas = gameObject.GetComponent<Canvas>();
            if (canvas == null)
            {
                Log.Error("Wnd Without Canvas Comp");
            }

            canvas.renderMode = RenderMode.ScreenSpaceCamera;
            canvas.worldCamera = WndMgr.UICamera;

            canvas.sortingLayerName = WndConf.Layer.ToString();
            canvas.sortingOrder = sortingorder;

            CanvasScaler scaler = gameObject.GetComponent<CanvasScaler>();
            if (scaler != null)
            {
                scaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
                scaler.referenceResolution = new Vector2(800, 600);
            }

            m_funcpointer = Ready2Show;
        }

        private void InnerHide()
        {
            WndState = EWndState.Hide;

            gameObject.SetActive(false);

            OnHide();
        }

        private void Ready2Show()
        {
            m_funcpointer = null;
            OnShow();
        }

        protected virtual void Close()
        {
            WndMgr.Hide(ConfId);
        }

        private Dictionary<string, UIWidget> m_widgets = new Dictionary<string, UIWidget>();
        public T AddWidget<T>(string path, GameObject go) where T : UIWidget
        {
            T component = Context.Game.Factory.CreateWithParent<T, GameObject>(this, go, IsFromPool);

            m_widgets.Add(path, component);

            return component;
        }
    }
}
