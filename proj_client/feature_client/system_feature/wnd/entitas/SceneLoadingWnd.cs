﻿using ETT.Client.Common;
using ETT.Client.ZhongGuoXiangQi;
using ETT.Widgets;

namespace ETT.Client.Wnd
{
    [Window((int)ECommonWndTable.SceneLoading)]
    public class SceneLoadingWnd : AbstractWnd
    {
        public struct WindowParam : IWindowParam
        {
            public uint BGModelId;
        }

        private WindowParam Param => (WindowParam)WndParam;

#pragma warning disable 0649,0169
        [UIField("BackGround")]
        private UIModelImage mod_bg;
        [UIField("Loading/Image")]
        private UIImage img_loading;
        [UIField("Loading/Image/Text")]
        private UILabel lbl_progress;
#pragma warning restore 0649,0169

        protected override void OnInit()
        {
            lbl_progress.text = string.Empty;
        }

        protected override void OnShow()
        {
            if (Param.BGModelId != 0)
            {
                mod_bg.SetModel(Param.BGModelId);
            }
        }

        protected override void OnUpdate()
        {
            img_loading.fillAmount = SceneMgr.ProgressValue;
            int value = (int)(SceneMgr.ProgressValue * 100);
            if (value > 100)
            {
                value = 100;
            }
            lbl_progress.text = $"{value}%";
        }

        protected override void OnHide()
        {
            mod_bg.ClearModel();
        }

        protected override void OnDestroy()
        {
        }


    }
}
