﻿using ETT.Client.Table;
using ETT.Client.Wnd;
using ETT.Model;
using ETT.Table;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace ETT.Client
{
    public partial class TableComponent : Model.Component, IAwake
    {
        //private readonly Dictionary<string, IAbstractTable> m_tabledict = new Dictionary<string, IAbstractTable>();
        private readonly List<IAbstractTable> m_list = new List<IAbstractTable>();

        public List<IAbstractTable> FindAll()
        {
            return m_list;
        }

        private AbstractTableParser m_parser;

        private ShieldWordTree m_shield;

        public void Awake()
        {

            InnerInit();

            m_parser = new TextParser();
            m_parser.Mgr = this;

            //Log.Debug($"Table Start Parse {Time.realtimeSinceStartup}");
            m_parser.Parse();
            //Log.Debug($"Shield Start Parse {Time.realtimeSinceStartup}");
            m_shield = new ShieldWordTree(Context.Table.GetComponent<ShieldwordTable>());
            //Log.Debug($"Parse Finish {Time.realtimeSinceStartup}");

            Log.Debug("TableComponent Awake");
        }

        public bool Match(char[] word)
        {
            return m_shield.Match(word);
        }
    }
}
